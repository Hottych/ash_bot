/*
 *  Poi.cpp
 *  HottychBot
 *
 *  Created by Alexey Prosin on 12/17/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#include "Poi.h"
#include "State.h"

using namespace std;


Poi::Poi(ControlPoint *centerPoint, int radius, float from, float to)
{
    center = centerPoint;
    valueFrom = from;
    valueTo = to;
    poiSteps = radius;
    GeneratePoiWave();
    ApplyField();
}

Poi::~Poi()
{
    DeapplyField();
}

void Poi::RecheckField()
{
    DeapplyField();
    GeneratePoiWave();
    ApplyField();
}

void Poi::ChangeField(float from, float to)
{
    DeapplyField();
    valueFrom = from;
    valueTo = to;
    GeneratePoiWave();
    ApplyField();
}

void Poi::ChangeField(int radius, float from, float to)
{
    DeapplyField();
    valueFrom = from;
    valueTo = to;
    poiSteps = radius;
    GeneratePoiWave();
    ApplyField();
}

void Poi::ReplaceField(ControlPoint *newCenter)
{
    DeapplyField();
    center = newCenter;
    GeneratePoiWave();
    ApplyField();
}

void Poi::ApplyField()
{
//    LOG("Apply field", 0);
    float vl = (valueTo - valueFrom) / (float)(poiSteps-1);
//    LOG("Interest step = %.4f", vl);
    int sz = pointsList.size();
    for (int i = 0; i < sz; i++) 
    {
        pointsList[i]->interest *= valueFrom + vl * pointsStep[i];
//        LOG("Point %d interest now is %.4f", pointsList[i]->curId, pointsList[i]->interest);
    }
}

void Poi::DeapplyField()
{
//    LOG("Deapply field", 0);
    float vl = (valueTo - valueFrom) / (float)(poiSteps-1);
//    LOG("Interest step = %.4f", vl);
    int sz = pointsList.size();
    for (int i = 0; i < sz; i++) 
    {
        pointsList[i]->interest /= valueFrom + vl * pointsStep[i];
//        LOG("Point %d interest now is %.4f", pointsList[i]->curId, pointsList[i]->interest);
    }
}


void Poi::GeneratePoiWave()
{
    State *state = center->state;

    int sz = state->controlPoints.size();
    for (int i = 0; i < sz; i++) 
    {
        state->controlPoints[i]->poiTemp = false;
    }
    pointsList.clear();
    pointsStep.clear();
    
    vector<ControlPoint*> points1;//todo: optimize this
    vector<ControlPoint*> points2;
    points1.reserve(500);
    points2.reserve(500);
    
    vector<ControlPoint*> *inPoints = &points2;
    vector<ControlPoint*> *pointsToScan = &points1;
    
    inPoints->push_back(center);
    pointsList.push_back(center);
    pointsStep.push_back(0);
    center->poiTemp = true;

    
    LOG("Generating po field in point %d", center->curId);
    int step = 1;
    while (true) 
    {
        int sz = inPoints->size();
        LOG("Points to check %d", sz);
        for (int i = 0; i < sz; i++) 
        {
            int csz = (*inPoints)[i]->connectedPoints.size();
            for (int n = 1; n < csz; n++) 
            {
                if (!(*inPoints)[i]->connectedPoints[n]->poiTemp)
                {
                    (*inPoints)[i]->connectedPoints[n]->poiTemp = true;
                    pointsToScan->push_back((*inPoints)[i]->connectedPoints[n]);
                    pointsList.push_back((*inPoints)[i]->connectedPoints[n]);
                    pointsStep.push_back(step);
                }
            }
        }
        if (pointsToScan->empty()) 
        {
            break;
        }
        
        step++;
        if (step == poiSteps) 
        {
            LOG("Poi field generated on %d step", step);
            return;
        }
        if (inPoints == &points2) 
        {
            inPoints = &points1;
            points2.clear();
            pointsToScan = &points2;
        }
        else 
        {
            inPoints = &points2;
            points1.clear();
            pointsToScan = &points1;
        }
        
    }
    LOG("Poi field has no more moves on %d step", step);
}

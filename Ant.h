/*
 *  Ant.h
 *  HottychBot
 *
 *  Created by Alexey Prosin on 11/1/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef _ANT_H_
#define _ANT_H_

#include "PositionedObject.h"
#include "vector"

class State;
class Path;
class Food;
class ControlPoint;
class Hive;
class Enemy;
class WarriorTeam;

class Ant : public PositionedObject
{
public:
    enum eAntType 
    {
        ANT_PROTECTOR = 0,//защищают мураваейник
        ANT_FOOD_HUNTER,//скауты ищут еду, разведывают территорию
        ANT_TERMINATOR, //любители уничтожать вражеские муравейники
//        ANT_FREE, // муравей оставшийся без работы, скорее всег бывший воин
        
        ANT_TYPES_COUNT
    };
    
    enum eAntStatus 
    {
        STATUS_ON_DUTY = 0, //выполняет текущее задание
        STATUS_ON_CLEAR_THE_WAY,
        STATUS_RETURNING_TO_THE_PATH
    };
    
    enum eMoveDirection
    {
        MOVE_UP = 0,
        MOVE_RIGHT,
        MOVE_DOWN,
        MOVE_LEFT,
        MOVE_NONE,
        MOVES_COUNT
    };
    
    
    Ant()
    : PositionedObject()
    {
        type = ANT_PROTECTOR;
    };
    
    Ant(State *statePtr, int _x, int _y, int _index);
    ~Ant();
    
    void Refresh(int currentTurn)
    {
        lastActiveTurn = currentTurn;
        isMoved = false;
        isProcessed = false;
    }

    void OnFoodIsGone(Food *food);
    void OnTargetHiveIsGone(Hive *hive);
    void OnAntIsDead();
    void OnTypeChanged(int newType);
    void OnWaterOnThePath();
    void OnPathIsFinished();
    void OnClearingTheWay();

    bool SpaceRequest(Ant *forAnt, int withDir);//запрос возможности пройти если есть возможность рецепиент должен отойти и вернуть true

    
    void ProcessTargeting();
    void ProcessProtectorTargeting();
    void ProcessFoodHunterTargeting();
    void ProcessTerminatorTargeting();

    void ProcessMovement();
    void ProcessProtectorMovement();
    void ProcessFoodHunterMovement();
    void ProcessTerminatorMovement();
    
    bool MoveAntToDirection(int dir, bool forceHive = false);
    
    void FollowPath();
    void CheckingPathForWater();
    bool CheckingPathForEnemies();

    void TargetToNearestFood(ControlPoint *cp, bool checkNearPoints);
    void CheckFoodInPlaceField();
    bool CheckForNearHives();//возвращает true если поблизости обнаружен вражий муравейник и наш мураш переквалифицирован в терминатора
    
    bool ShouldGetReadyToFightWith(Enemy *aEnemy);
    bool CheckForAttackableEnemies();
    void JoinWarriorsAgainstEnemy(Enemy *aEnemy);

    void ReturningToThePath();
    void CancelCurrentTarget();
    
    void SetNewWorkingPlace(ControlPoint *newPoint);
    
    bool CheckFroEnemyAtackers(int forX, int forY); //возвращает true если вокруг есть те кто может потенциально атаковать

    ControlPoint *FindBestPoint();


    Path *currentPath;
    Path* pathInMind;

    Food *targetFood;
    int checkFoodCounter;
    
    Hive *myHive;
    Hive *targetHive;
    
    State *worldState;

    ControlPoint *currentPlace;
    int currentPlaceVectorIndex;

    ControlPoint *currentTarget;
    int currentTargetVectorIndex;


    int type;
    bool isScout;
    int status;
    int stepsToTargetCouner;
    
    WarriorTeam *warriorTeam;
    std::vector<Enemy *>attackableEnemies;
    int noEnemiesForTurnsCount;
    
    int indexInWarriorTeamMembersVector;

    int lastActiveTurn;
    int storeVectorIndex;
    
    bool isMoved;
    bool isProcessed;
    bool isInRequest;
    bool isInRedeployment;
    
    int antId;
    
    std::vector<ControlPoint *> lastBadPoints;
};

const int MOVE_DIRECTIONS[Ant::MOVES_COUNT][2] = { {0, -1}, {1, 0}, {0, 1}, {-1, 0}, {0, 0} };      //{U, R, D, L, NONE}

#endif
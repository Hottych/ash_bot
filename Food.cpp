/*
 *  Food.cpp
 *  HottychBot
 *
 *  Created by Alexey Prosin on 11/7/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#include "Food.h"
#include "WaveField.h"
#include "State.h"

using namespace std;

Food::Food(int _x, int _y, int _index, State *state)
: PositionedObject(_x, _y, _index)
{
    harvester = NULL;
    lastActiveTurn = 0;
    storeVectorIndex = -1;
    placedPoint = NULL;
    field = new WaveField(state, state->viewRadius * 2 + 1, state->viewRadius * 2 + 1);
    field->mapOffsetX = x - field->centerX;
    field->mapOffsetY = y - field->centerY;
    field->GenerateField();
}

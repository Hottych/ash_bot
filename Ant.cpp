/*
 *  Ant.cpp
 *  HottychBot
 *
 *  Created by Alexey Prosin on 11/1/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#include "Ant.h"
#include "RefCounter.h"
#include "Path.h"
#include "Food.h"
#include "list"
#include "ControlPoint.h"
#include "State.h"
#include "Bot.h"
#include "Bug.h"
#include "Enemy.h"
#include "WarriorTeam.h"

using namespace std;

static int nextId = 1;
static const int stepsToAreaSwitch = 3;
static const int checkFoodSteps = 2;



Ant::Ant(State *statePtr, int _x, int _y, int _index)
: PositionedObject(_x, _y, _index)
{
    worldState = statePtr;
    type = ANT_PROTECTOR;
    currentPath = NULL;
    pathInMind = NULL;
    targetFood = NULL;
    targetHive = NULL;
    lastActiveTurn = 0;
    storeVectorIndex = -1;
    currentPlaceVectorIndex = -1;
    currentTargetVectorIndex = -1;
    status = STATUS_ON_DUTY;
    
    currentPlace = NULL;
    currentTarget = NULL;
    
    isMoved = false;
    isProcessed = false;
    isInRequest = false;
    isInRedeployment = false;

    isScout = false;
    
    warriorTeam = NULL;
    noEnemiesForTurnsCount = 0;
    indexInWarriorTeamMembersVector = -1;

    antId = nextId;
    nextId++;
    LOG("ANT_%d  CONSTRUCTOR", antId);
    
    checkFoodCounter = 0;

}

Ant::~Ant()
{
    SafeRelease(currentPath);
    SafeRelease(pathInMind);
}


void Ant::OnFoodIsGone(Food *food)
{//кто-то сожрал еду за которой мы шли
    switch (type) 
    {
        case ANT_FOOD_HUNTER:
        {
            status = STATUS_ON_DUTY;
            LOG("ANT_%d Food is gone", antId);
            CancelCurrentTarget();
        }
            break;
    }
}

void Ant::OnTargetHiveIsGone(Hive *hive)
{//кто-то унечтожил муравейник который мы пытались уничтожить
    if (targetHive)
    {
        LOG("ANT_%d Releasing hive", antId);
        targetHive->UnregisterTerminators(this);
        targetHive = NULL;
        LOG("ANT_%d hive released", antId);
    }
    OnTypeChanged(ANT_FOOD_HUNTER);
}

void Ant::OnAntIsDead()
{//мураш сдох
    LOG("ANT_%d %d, %d is dead", antId, x, y);
    CancelCurrentTarget();
    worldState->bot->OnAntIsDead(this);
    if (targetHive)
    {
        LOG("ANT_%d Releasing hive", antId);
        targetHive->UnregisterTerminators(this);
        targetHive = NULL;
        LOG("ANT_%d hive released", antId);
        OnTypeChanged(ANT_FOOD_HUNTER);
    }

    if (currentPlace) 
    {
        currentPlace->UnregisterPlacedAnt(this);
        currentPlace = NULL;
    }
    
    if (warriorTeam)
    {
        warriorTeam->OnAntIsDead(this);
        warriorTeam = NULL;
        indexInWarriorTeamMembersVector = -1;
    }
    
    if (type == ANT_PROTECTOR) 
    {
        myHive->UnregisterAnt(this);
    }
}

void Ant::OnTypeChanged(int newType)
{//меняем тип мураша на другой
    LOG("ANT_%d changing type to %d ", antId, newType);
    worldState->bot->OnAntTypeChanged(this, newType);
    currentPlace->OnPlacedAntChangeType(this, newType);
    if (type == ANT_PROTECTOR) 
    {
        myHive->UnregisterAnt(this);
    }
    type = newType;
    CancelCurrentTarget();
    status = STATUS_ON_DUTY;
}

void Ant::OnWaterOnThePath()
{//если впереди на пути появилась вода. Не перед ебалом, а именно вдали на кривой пути
    LOG("ANT_%d Water on the path", antId);
    switch (type) 
    {
        case ANT_PROTECTOR:
        case ANT_FOOD_HUNTER:
        {
            status = STATUS_ON_DUTY;
            CancelCurrentTarget();
        }
            break;
        case ANT_TERMINATOR:
        {
            if (targetHive)
            {
                LOG("ANT_%d Releasing hive", antId);
                targetHive->UnregisterTerminators(this);
                targetHive = NULL;
                LOG("ANT_%d hive released", antId);
                OnTypeChanged(ANT_FOOD_HUNTER);
            }
            OnTypeChanged(ANT_FOOD_HUNTER);
        }
            break;
    }
}

void Ant::OnPathIsFinished()
{//если пришел в конечную точку пути
    switch (type) 
    {
        case ANT_PROTECTOR:
        case ANT_FOOD_HUNTER:
        {
            LOG("ANT_%d Path is finished", antId);
            if (status == STATUS_ON_DUTY) 
            {
                LOG("For status STATUS_ON_DUTY", 0);
                CancelCurrentTarget();
            }
            else if(status == STATUS_RETURNING_TO_THE_PATH || status == STATUS_ON_CLEAR_THE_WAY)
            {
                status = STATUS_ON_DUTY;
                LOG("For status STATUS_RETURNING_TO_THE_PATH", 0);
                SafeRelease(currentPath);
                    //            LOG("ANT_%d releasing currentPath line %d", antId, __LINE__);
                currentPath = pathInMind;
                pathInMind = NULL;
                    //            LOG("ANT_%d releasing path in mind ", antId, __LINE__);
                currentPath->StepIsDone();
                LOG("Returns to the path", 0);
            }
        }
            break;
        case ANT_TERMINATOR:
        {
            if (targetHive)
            {
                LOG("ANT_%d Releasing hive", antId);
                targetHive->UnregisterTerminators(this);
                targetHive = NULL;
                LOG("ANT_%d hive released", antId);
                OnTypeChanged(ANT_FOOD_HUNTER);
            }
            OnTypeChanged(ANT_FOOD_HUNTER);
        }
            break;
    }
}

void Ant::OnClearingTheWay()
{//муравей уступил кому-то дорогу
    switch (type) 
    {
        case ANT_PROTECTOR:
        case ANT_FOOD_HUNTER:
        {
            LOG("ANT_%d  %d, %d  is clearing the way", antId, x, y);
            if (targetFood) 
            {
                if (status == STATUS_ON_DUTY && targetFood) 
                {
                    pathInMind = currentPath;
                        //            LOG("ANT_%d releasing pathInMind line %d", antId, __LINE__);
                }
                status = STATUS_ON_CLEAR_THE_WAY;
                currentPath = NULL;
                    //        LOG("ANT_%d releasing current path line %d", antId, __LINE__);
            }
            else 
            {
                CancelCurrentTarget();
                status = STATUS_ON_DUTY;
            }
        }
            break;
    }
}

void Ant::CancelCurrentTarget()
{//отменяем текущую цель и все что на нее завязано
    LOG("ANT_%d Canceling current target", antId);
    stepsToTargetCouner = 0;
    if (currentTarget)
    {
        LOG("Current target 0x%X    %d", currentTarget, currentTarget->curId);
        LOG("ANT_%d Unregistring current target", antId);
        currentTarget->UnregisterTargetedAnt(this);
        currentTarget = NULL;
    }
    
    SafeRelease(currentPath);
    SafeRelease(pathInMind);
    LOG("ANT_%d Paths released", antId);

    if (targetFood) 
    {
        LOG("ANT_%d Releasing food", antId);
        targetFood->harvester = NULL;
        targetFood = NULL;
        LOG("ANT_%d Food released", antId);
    }
    if (targetHive)
    {
        LOG("ANT_%d Releasing hive", antId);
        targetHive->UnregisterTerminators(this);
        targetHive = NULL;
        if (type == ANT_TERMINATOR) 
        {
            OnTypeChanged(ANT_FOOD_HUNTER);
        }
        LOG("ANT_%d hive released", antId);
    }
    
}


bool Ant::SpaceRequest(Ant *forAnt, int withDir)
{//запрос на освобождение дороги
//    LOG("Space requested for ANT_%d  %d, %d  by ANT_%d  %d, %d", antId, x, y, forAnt->antId, forAnt->x, forAnt->y);

    if (isMoved
        || (type == ANT_TERMINATOR && forAnt->type != ANT_TERMINATOR) //теперь терминаторы дорогу уступают только терминаторам TODO: просчитать для них куда выгоднее отступить
       || (NULL != warriorTeam)
        )
    {//todo: запоминать что кто-то просил пройти и реагировать на это если собирается стоять на месте
//        LOG("ANT_%d  allready moved", antId);
        return false;
    }
    if (isInRequest) 
    {
//        LOG("ANT_%d  allready in request", antId);
        return false;
    }
    isInRequest = true;

    
    int dir = (withDir + 1) & 0x3;
    if (MoveAntToDirection(dir)) 
    {
        OnClearingTheWay();
        isInRequest = false;
        return true;
    }
    dir = (withDir + 2) & 0x3;
    if (MoveAntToDirection(dir)) 
    {
        OnClearingTheWay();
        isInRequest = false;
        return true;
    }

    if (MoveAntToDirection(withDir))
    {
        OnClearingTheWay();
        isInRequest = false;
        return true;
    }
    
    isInRequest = false;
    return false;
}


bool Ant::MoveAntToDirection(int dir, bool forceHive/* = false*/)
{//собственно перемещение с одной клетки на другую
    if (isMoved) 
    {
        LOG("ANT_%d is already moved", antId);
        return false;
    }
    ANT_ASSERT(dir >= 0 && dir < 4);
    int nx = (x + MOVE_DIRECTIONS[dir][0] + State::width) % State::width;
    int ny = (y + MOVE_DIRECTIONS[dir][1] + State::height) % State::height;
    int ind = nx + ny * State::width;
    VECTOR_BOUNDS(worldState->myAntsMap, ind);
    if (worldState->myAntsMap[ind]) 
    {
        isInRequest = true;
        if (!worldState->myAntsMap[ind]->SpaceRequest(this, dir)) 
        {
            isInRequest = false;
            LOG("ANT_%d cant move because of other ant", antId);
            return false;
        }
        isInRequest = false;
    }
    VECTOR_BOUNDS(worldState->terrainMap, ind);
    if (worldState->terrainMap[ind] == State::WATER)
    {
        LOG("ANT_%d cant move because of water", antId);
        return false;
    }
    if (worldState->terrainMap[ind] == State::FOOD)
    {
        LOG("ANT_%d cant move because of food", antId);
        return false;
    }
    if (!forceHive && worldState->terrainMap[ind] == State::HIVE)
    {
        LOG("ANT_%d cant move because of our hive", antId);
        return false;
    }
    
    LOG("ANT_%d making move", antId);
    worldState->MakeMove(this, dir);

    //removing
    VECTOR_BOUNDS(worldState->terrainMap, mapIndex);
    if (worldState->terrainMap[mapIndex] == State::ANT)
    {
        worldState->terrainMap[mapIndex] = State::GROUND;
    }
    
    VECTOR_BOUNDS(worldState->myAntsMap, mapIndex);
    if (this == worldState->myAntsMap[mapIndex])
    {
        worldState->myAntsMap[mapIndex] = NULL;

        VECTOR_BOUNDS(worldState->myAntsMap, ind);
        worldState->myAntsMap[ind] = this;
    }
    
    x = nx;
    y = ny;
    mapIndex = ind;
    
    VECTOR_BOUNDS(worldState->terrainMap, mapIndex);
    if (State::GROUND == worldState->terrainMap[mapIndex])
    {
        worldState->terrainMap[mapIndex] = State::ANT;
    }

    VECTOR_BOUNDS(worldState->controlAreasMap, mapIndex);
    ANT_ASSERT(worldState->controlAreasMap[mapIndex]);
    ANT_ASSERT(currentPlace);
    if (worldState->controlAreasMap[mapIndex] != currentPlace) 
    {//смена контрольной точки
        currentPlace->UnregisterPlacedAnt(this);
        VECTOR_BOUNDS(worldState->controlAreasMap, mapIndex);
        currentPlace = worldState->controlAreasMap[mapIndex];
        currentPlace->RegisterPlacedAnt(this);
        LOG("ANT_%d current area switched to the %d", antId, currentPlace->curId);
        
        switch (type) 
        {
            case ANT_FOOD_HUNTER:
            {
                if (stepsToTargetCouner > stepsToAreaSwitch)
                {
                    if (!targetFood && status != STATUS_ON_CLEAR_THE_WAY && status != STATUS_RETURNING_TO_THE_PATH) 
                    {
                        CancelCurrentTarget();
                    }
                }
            }
                break;
        }
    }

    LOG("ANT_%d New pos %d,%d    index: %d", antId, x, y, mapIndex);
    
    stepsToTargetCouner++;
    
    isMoved = true;
    return true;
}


void Ant::FollowPath()
{//следование по пути
    if (currentPath && currentPath->IsFinished()) 
    {
        OnPathIsFinished();
    }
    if (currentPath) 
    {
        LOG("ANT_%d follow path direction %d", antId, currentPath->GetCurrentStepDirection());
        
        if(MoveAntToDirection(currentPath->GetCurrentStepDirection()))
        {
            if (currentPath) 
            {
                currentPath->StepIsDone();
                if (currentPath->IsFinished()) 
                {
                    OnPathIsFinished();
                }
            }
        }
        
    }
}

void Ant::CheckingPathForWater()
{//проверка видимой части пути на наличие воды.
    ANT_ASSERT(currentPath);
    if (currentPath->GetStepsLeft() > (worldState->viewRadius - 1))
    {
        int st = currentPath->GetStepsLeft() - (int)(worldState->viewRadius - 1);
        int px = currentPath->GetStepX(st);
        int py = currentPath->GetStepY(st);
        VECTOR_BOUNDS(worldState->terrainMap, px + py * State::width);
        if (worldState->terrainMap[px + py * State::width] == State::WATER)
        {
            LOG("ANT_%d water on the path %d, %d", antId, px, py);
            OnWaterOnThePath();
        }
    }
}

//bool Ant::CheckingPathForEnemies()
//{//возвращает true если путь свободен от врагов
//    ANT_ASSERT(currentPath);
//    if (!currentPath->IsFinished()) 
//    {
//        return !CheckFroEnemyAtackers(currentPath->GetCurrentStepX(), currentPath->GetCurrentStepY());
//    }
//    return true;
//}

void Ant::CheckFoodInPlaceField()
{//исчем еду в текущей арии и во всех с ней соедененных
    if (worldState->activeHivesCount <= 0) 
    {
        return;
    }
    LOG("ANT_%d Checking for food", antId);
    checkFoodCounter = 0;

    int foodDist = 999999;
    Food *newFood = NULL;
    if (targetFood) 
    {
        LOG("ANT_%d Getting current food dist", antId);
        int fx = x;
        int fy = y;
        if (fx - targetFood->x > State::width/2)
        {
            fx -= State::width;
        }
        else if (fx - targetFood->x < -State::width/2)
        {
            fx += State::width;
        }
        
        if (fy - targetFood->y > State::height/2)
        {
            fy -= State::height;
        }
        else if (fy - targetFood->y < -State::height/2)
        {
            fy += State::height;
        }
//        LOG("Food pos %d, %d", fx, fy);
        
        fx = fx - targetFood->field->mapOffsetX;
        fy = fy - targetFood->field->mapOffsetY;
        
//        LOG("Field pos %d, %d", fx, fy);
        if (fx >= 0 && fy >= 0 && fx < targetFood->field->mapWidth && fy < targetFood->field->mapHeight) 
        {
            foodDist = targetFood->field->pathMap[fx + fy * targetFood->field->mapWidth]/*-1*/;
            LOG("Current targeted food dist is %d", foodDist);
        }
    }
//    LOG("Current place id %d", currentPlace->curId);
//    currentPlace->waveField->Log();
    Ant *newHarvester = NULL;
    for (vector<ControlPoint*>::iterator it = currentPlace->connectedPoints.begin(); it != currentPlace->connectedPoints.end(); it++)
    {
        LOG("ANT_%d Getting food dist in point %d", antId, (*it)->curId);
        for (int i = 0; i < (*it)->food.size(); i++) 
        {
            VECTOR_BOUNDS((*it)->food, i);
            ANT_ASSERT((*it)->food[i]);
            Food *food = (*it)->food[i];
            if (food != targetFood)
            {
//                LOG("Food pos %d, %d", (*it)->food[i]->x, (*it)->food[i]->y);
                int harvDist = 9999999;
                Ant *h = food->harvester;
                if (h) 
                {
                    LOG("Food allready with harvester ANT_%d", h->antId);
                    int fx = h->x;
                    int fy = h->y;
                    if (fx - food->x > State::width/2)
                    {
                        fx -= State::width;
                    }
                    else if (fx - food->x < -State::width/2)
                    {
                        fx += State::width;
                    }
                    
                    if (fy - food->y > State::height/2)
                    {
                        fy -= State::height;
                    }
                    else if (fy - food->y < -State::height/2)
                    {
                        fy += State::height;
                    }
                        //        LOG("Food pos %d, %d", fx, fy);
                    
                    fx = fx - food->field->mapOffsetX;
                    fy = fy - food->field->mapOffsetY;
                    
                        //        LOG("Field pos %d, %d", fx, fy);
                    if (fx >= 0 && fy >= 0 && fx < food->field->mapWidth && fy < food->field->mapHeight) 
                    {
                        VECTOR_BOUNDS(food->field->pathMap, fx + fy * food->field->mapWidth);
                        harvDist = food->field->pathMap[fx + fy * food->field->mapWidth];
                        LOG("Old harvester dist %d", harvDist);
                    }
                }
                int fx = x;
                int fy = y;
                if (fx - food->x > State::width/2)
                {
                    fx -= State::width;
                }
                else if (fx - food->x < -State::width/2)
                {
                    fx += State::width;
                }
                
                if (fy - food->y > State::height/2)
                {
                    fy -= State::height;
                }
                else if (fy - food->y < -State::height/2)
                {
                    fy += State::height;
                }
                    //        LOG("Food pos %d, %d", fx, fy);
                
                fx = fx - food->field->mapOffsetX;
                fy = fy - food->field->mapOffsetY;
                
                    //        LOG("Field pos %d, %d", fx, fy);
                if (fx >= 0 && fy >= 0 && fx < food->field->mapWidth && fy < food->field->mapHeight) 
                {
                    VECTOR_BOUNDS(food->field->pathMap, fx + fy * food->field->mapWidth);
                    int d = food->field->pathMap[fx + fy * food->field->mapWidth];
                    LOG("Food dist is %d", d);
                        //                    LOG("Checking field dist %d", d);
                    if (d < harvDist) 
                    {
                        if (d < foodDist)
                        {
                            LOG("New dist to food %d", d);
                            foodDist = d;
                            newFood = food;
                            newHarvester = h;
                            if (newHarvester) 
                            {
                                LOG("Old harvester ANT_%d", newHarvester->antId);
                            }
                            else 
                            {
                                LOG("Old harvester is absent", 0);
                            }

                        }
                    }
                        //            LOG("Current targeted food dist is %d", foodDist);
                }
                
//                LOG("Field pos %d, %d", fx, fy);
                    //                fx = (fx + currentPlace->waveField->mapWidth) % currentPlace->waveField->mapWidth;
                    //                fy = (fy + currentPlace->waveField->mapHeight) % currentPlace->waveField->mapHeight;
                    //                LOG("Field pos %d, %d", fx, fy);
            }
        }
    }
    if (newFood) 
    {
        if (newHarvester) 
        {
            newHarvester->CancelCurrentTarget();
            worldState->bot->OnAntIsNeedNewTarget(newHarvester);
        }
        CancelCurrentTarget();
        targetFood = newFood;
        targetFood->harvester = this;
        status = STATUS_ON_DUTY;
        LOG("New target food %d, %d", targetFood->x, targetFood->y);
    }
}

void Ant::TargetToNearestFood(ControlPoint *cp, bool checkNearPoints)
{
    if (worldState->activeHivesCount <= 0) 
    {
        return;
    }
    targetFood = NULL;
    ANT_ASSERT(cp);
    CheckFoodInPlaceField();

        //если ничего не нашли относительно текущей арии, ищем чуть другим способом
        //теперь ограничено чуть больше радиуса обзора конкретного муравья - дабы не ходили без дела через пол карты

    if (!targetFood) 
    {
        LOG("Checking for food by distance", 0);
        int foodDist = worldState->viewRadiusSq + 4*4;
        for (vector<ControlPoint*>::iterator it = currentPlace->connectedPoints.begin(); it != currentPlace->connectedPoints.end(); it++)
        {
            for (int i = 0; i < (*it)->food.size(); i++) 
            {
                VECTOR_BOUNDS((*it)->food, i);
                ANT_ASSERT((*it)->food[i]);
                if (!(*it)->food[i]->harvester)
                {
                    int d = DistanceSq((*it)->food[i]);
                    if (d < foodDist)
                    {
                        foodDist = d;
                        targetFood = (*it)->food[i];
                    }
                }
            }
        }

        if (targetFood) 
        {
            targetFood->harvester = this;
            status = STATUS_ON_DUTY;
        }
    }
}

bool Ant::CheckForNearHives()
{
    int hiveDist = 999999;
    Hive *newHive = NULL;
    for (vector<ControlPoint*>::iterator it = currentPlace->connectedPoints.begin(); it != currentPlace->connectedPoints.end(); it++)
    {
        for (int i = 0; i < (*it)->hives.size(); i++) 
        {
            VECTOR_BOUNDS((*it)->hives, i);
            ANT_ASSERT((*it)->hives[i]);
            int d = DistanceSq((*it)->hives[i]);
            if (d < hiveDist)
            {
                hiveDist = d;
                newHive = (*it)->hives[i];
            }
        }
    }
    if (newHive) 
    {
        OnTypeChanged(ANT_TERMINATOR);
        LOG("Target hive %d, %d", newHive->x, newHive->y);
        targetHive = newHive;
        targetHive->RegisterTerminators(this);
        return true;
    }
    return false;
}

bool Ant::ShouldGetReadyToFightWith(Enemy *aEnemy)
{
    int steps = worldState->bot->pathFinder->GetPath(x, y, aEnemy->x, aEnemy->y)->steps.size();

    return ((steps < worldState->movedAttackRadius * 2 + 1)
            && (DistanceSq(aEnemy) <= worldState->movedAttackRadiusSq));
    // FILTODO: discuss
}

bool Ant::CheckForAttackableEnemies()
{
//    LOG("ANT_%d Checking for enemies in %d, %d", antId, this->x, this->y);
    attackableEnemies.clear();
    
    VECTOR_BOUNDS(worldState->controlAreasMap, mapIndex);

    if ((type == Ant::ANT_PROTECTOR) && (DistanceSq(myHive) > 6*6)) 
    {
        if (NULL != warriorTeam)
        {
            warriorTeam->RemoveAnt(this);
        }
        return false;
    }
    vector<ControlPoint *> controlPointsToCheck;
    controlPointsToCheck.reserve(ControlPoint::GetTotalControlPointsCount());
    
    vector<bool> cpIsVisited = vector<bool>(ControlPoint::GetTotalControlPointsCount());

    ControlPoint *cp0 = worldState->controlAreasMap[mapIndex];
    controlPointsToCheck.push_back(cp0);
    
    for (vector<ControlPoint*>::iterator it0 = cp0->connectedPoints.begin(); it0 != cp0->connectedPoints.end(); it0++)
    {
        controlPointsToCheck.push_back(*it0);

        for (vector<ControlPoint*>::iterator it1 = (*it0)->connectedPoints.begin(); it1 != (*it0)->connectedPoints.end(); it1++)
        {
            controlPointsToCheck.push_back((*it1));
        }
    }

    WaveField *field = worldState->GetWaveFieldMapCell(x, y).field;

    for (vector<ControlPoint*>::iterator it = controlPointsToCheck.begin(); it != controlPointsToCheck.end(); it++)
    {
        ControlPoint *cp = (*it);
        ANT_ASSERT(cp->curId < cpIsVisited.size());
                
        if (!cpIsVisited[cp->curId]) 
        {
            for (vector<Enemy *>::iterator enemy_it = cp->enemies.begin(); enemy_it != cp->enemies.end(); enemy_it++)
            {
                Enemy *enemy = (*enemy_it);
                int enemyDist = field->DistanceToPoint(enemy->x, enemy->y);                            
                
//                LOG("ANT_%d distance to enemy at (%d, %d) is %d", antId, enemy->x, enemy->y, enemyDist);
                
                if ((enemyDist <= worldState->movedAttackRadiusSq)
                    && (worldState->DistanceSq(x, y, enemy->x, enemy->y) <= worldState->movedAttackRadiusSq))
                {
//                    LOG("ANT_%d getting ready to fight", antId);
//                    if (type == Ant::ANT_PROTECTOR)
                    
                    attackableEnemies.push_back(enemy);
                    JoinWarriorsAgainstEnemy(enemy);
                }
            }
            
            cpIsVisited[cp->curId] = true;
        }
    }
    
    if (attackableEnemies.size() > 0)
    {
        noEnemiesForTurnsCount = 0;
//        LOG("noEnemiesForTurnsCount: %d", noEnemiesForTurnsCount);
    }
    else 
    {
        noEnemiesForTurnsCount += 1;
//        LOG("noEnemiesForTurnsCount: %d", noEnemiesForTurnsCount);
    }
            
    return (NULL != this->warriorTeam);
}

void Ant::JoinWarriorsAgainstEnemy(Enemy *aEnemy)
{
//    LOG("ANT_%d joins team against enemy at (%d, %d)", this->antId, aEnemy->x, aEnemy->y);

    WarriorTeam *destWarriorTeam = NULL;

    if ((NULL == aEnemy->warriorTeam) && (NULL == this->warriorTeam))
    {
        destWarriorTeam = new WarriorTeam(worldState);
        worldState->AddWarriorTeam(destWarriorTeam);
    } 
    else if ((NULL != aEnemy->warriorTeam) && (NULL != this->warriorTeam) && (aEnemy->warriorTeam != this->warriorTeam))
    {
        destWarriorTeam = new WarriorTeam(worldState, this->warriorTeam, aEnemy->warriorTeam);
        
        worldState->AddWarriorTeam(destWarriorTeam);
    } 
    else 
    {
        destWarriorTeam = (NULL == this->warriorTeam) ? aEnemy->warriorTeam : this->warriorTeam;
    }
    
    if (NULL == this->warriorTeam)
    {
//        LOG("JOINING EXISTING TEAM: WARTEAM_%d", destWarriorTeam->teamID);
        destWarriorTeam->AddAnt(this);
    }

    if (NULL == aEnemy->warriorTeam)
    {
        destWarriorTeam->AddEnemy(aEnemy);
    }
}

void Ant::ReturningToThePath()
{
    LOG("ANT_%d returning to the path", antId);
    if (pathInMind) 
    {
        currentPath = worldState->bot->pathFinder->GetPath(x, y, pathInMind->GetCurrentStepX(), pathInMind->GetCurrentStepY());
        if (currentPath && currentPath->IsFinished()) 
        {
            OnPathIsFinished();
        }
            //        LOG("ANT_%d releasing current path line %d", antId, __LINE__);
        status = STATUS_RETURNING_TO_THE_PATH;
    }
    else 
    {
       status = STATUS_ON_DUTY; 
    }

}

//bool Ant::CheckFroEnemyAtackers(int forX, int forY)//координаты должны быть нормализованы
//{ //возвращает true если вокруг есть те кто может потенциально атаковать
//    LOG("ANT_%d Checking for enemies in %d, %d", antId, forX, forY);
//    ControlPoint *cp = worldState->controlAreasMap[forX + forY * State::width];
//    VECTOR_BOUNDS(worldState->controlAreasMap, forX + forY * State::width);
//    for (vector<ControlPoint*>::iterator it = cp->connectedPoints.begin(); it != cp->connectedPoints.end(); it++)
//    {
//        int sz = (*it)->enemies.size();
//        LOG("ANT_%d  %d Enemies in area", antId, sz);
//        for (int i = 0; i < sz; i++) 
//        {
//            VECTOR_BOUNDS((*it)->enemies, i);
//            LOG("Distance is %d of %d", DistanceSq((*it)->enemies[i]), worldState->movedAttackRadiusSq);
//            if (DistanceSq((*it)->enemies[i]) <= worldState->movedAttackRadiusSq)
//            {
//                LOG("ANT_%d Enemies in attack range", antId);
//                return true;
//            }
//        }
//    }
//    return false;
//}


void Ant::ProcessTargeting()
{
    LOG("process targeting ANT_%d    %d, %d", antId, x, y);
    
    switch (type) 
    {
        case ANT_PROTECTOR:
            ProcessProtectorTargeting();
            break;
        case ANT_FOOD_HUNTER:
            ProcessFoodHunterTargeting();
            break;
        case ANT_TERMINATOR:
            ProcessTerminatorTargeting();
            break;
    }
}

void Ant::ProcessMovement()
{
    LOG("process movement ANT_%d    %d, %d", antId, x, y);
    
    if (isMoved) 
    { // FILTODO: discuss
        return;
    }
    
    switch (type) 
    {
        case ANT_PROTECTOR:
            ProcessProtectorMovement();
            break;
        case ANT_FOOD_HUNTER:
            ProcessFoodHunterMovement();
            break;
        case ANT_TERMINATOR:
            ProcessTerminatorMovement();
            break;
    }
    isProcessed = true;
}

#define MIN_PROTECTION_RADIUS                           4
#define MAX_PROTECTION_RADIUS                           7
#define MAX_ATTEMPTS_TO_FIND_NEXT_PROTECTOR_POSITION    20

void Ant::ProcessProtectorTargeting()
{
//    checkFoodCounter++;
//    if (checkFoodCounter > checkFoodSteps) 
//    {
//        CheckFoodInPlaceField();
//        if (targetFood) 
//        {
//            currentPath = worldState->bot->pathFinder->GetPath(x, y, targetFood->x, targetFood->y);
//            OnTypeChanged(ANT_FOOD_HUNTER);
//            return;
//        }
//    }
    if (currentPath)
    {
        CheckingPathForWater();
    }
    if (STATUS_ON_CLEAR_THE_WAY == status) 
    {
        ReturningToThePath();
    }
    if (!currentPath) 
    {
        ANT_ASSERT(myHive);
        
        int radius = min(MAX_PROTECTION_RADIUS, max(MIN_PROTECTION_RADIUS, myHive->GetProtectorsCount()));
        
        // даем несколько попыток на поиск подходящего пути
        for (int i = 0; i < MAX_ATTEMPTS_TO_FIND_NEXT_PROTECTOR_POSITION; ++i)
        {
            int dx = myHive->x + rand() % (2 * radius) - radius;
            int dy = myHive->y + rand() % (2 * radius) - radius;
            if (dx != x || dy != y && (dx != myHive->x || dy != myHive->y))
            {
                currentPath = worldState->bot->pathFinder->GetPath(x, y, dx, dy);
                if (currentPath && currentPath->IsFinished()) 
                {
                    OnPathIsFinished();
                }
            }
            if (currentPath) break;
            
            if ((MAX_ATTEMPTS_TO_FIND_NEXT_PROTECTOR_POSITION - 1) == i)
            {
                LOG("ANT_%d OnTypeChanged from PROTECTOR to FOOD_HUNTER", antId);
                OnTypeChanged(ANT_FOOD_HUNTER);
                return;
            }
        }
    }
}

void Ant::ProcessProtectorMovement()
{// в целом похож на фуд-хантера, только шарится около своего муровейника в определенном радиусе
    // если рядом еда, переквалифицируем в еданоса, т.к. по результату всеравно родиться защитниг, как только еда будет подобрана
    
    if (currentPath) 
    {
        FollowPath();
    }
}

void Ant::ProcessFoodHunterTargeting()
{
    LOG("ANT_%d targeting as hunter", antId);
    if (worldState->activeHivesCount <= 0) 
    {
        if (targetFood)
        {
            targetFood->harvester = NULL;
            targetFood = NULL;
        }
    }
    if(CheckForNearHives()) // FILTODO: kill enemy 
    {
        ProcessTerminatorTargeting();
        return;
    }

    checkFoodCounter++;
    if (checkFoodCounter >= checkFoodSteps) 
    {
        CheckFoodInPlaceField();
    }
    if (currentPath) 
    {
        CheckingPathForWater();
    }
    
    
    switch (status) 
    {
        case STATUS_ON_DUTY:
        {
            if (!currentPath) 
            {
                    //исчем путь к еде
                if (!targetFood) 
                {
                    LOG("ANT_%d searching nearest food", antId);
                    VECTOR_BOUNDS(worldState->controlAreasMap, mapIndex);
                    TargetToNearestFood(currentPlace, true);
                }
                if (targetFood) 
                {
                    LOG("ANT_%d searching path to the current food %d, %d", antId, targetFood->x, targetFood->y);
                    currentPath = worldState->bot->pathFinder->GetPath(x, y, targetFood->x, targetFood->y);
                        //                    LOG("ANT_%d releasing current line %d", antId, __LINE__);
                    if (currentPath && currentPath->IsFinished()) 
                    {
                        OnPathIsFinished();
                    }
                    if (!currentPath) 
                    {
                        LOG("ANT_%d can't find path", antId);
                        CancelCurrentTarget();
                    }
                }
                else 
                { //кусок фэйковой логики, чисто на пока
                    
                    if (!currentTarget) 
                    {
                        LOG("ANT_%d trying to change placePoint  current place is %d", antId, currentPlace->curId);
                        ControlPoint *newPlace = FindBestPoint();
                        if (newPlace && currentPlace != newPlace) 
                        {
                            LOG("ANT_%d searching path to the new place %d, %d", antId, newPlace->x, newPlace->y);
                            currentPath = worldState->bot->pathFinder->GetPath(x, y, newPlace->x, newPlace->y);
                                //                        LOG("ANT_%d releasing current path line %d", antId, __LINE__);
                            if (currentPath && currentPath->IsFinished()) 
                            {
                                OnPathIsFinished();
                            }
                            if (!currentPath) 
                            {
                                LOG("ANT_%d can't find path to the newPlace", antId);
                                lastBadPoints.push_back(newPlace);
                                ControlPoint::OnCantFindPathToPoint(newPlace);
                            }
                            else 
                            {
                                lastBadPoints.clear();
                                currentTarget = newPlace;
                                currentTarget->RegisterTargetedAnt(this);
                            }                            
                        }
                    }
                    else 
                    {
                        LOG("ANT_%d searching path to the current target point", antId);
                        currentPath = worldState->bot->pathFinder->GetPath(x, y, currentTarget->x, currentTarget->y);
                        if (currentPath && currentPath->IsFinished()) 
                        {
                            OnPathIsFinished();
                        }
                        if (!currentPath) 
                        {
                            lastBadPoints.push_back(currentTarget);
                            ControlPoint::OnCantFindPathToPoint(currentTarget);
                            LOG("ANT_%d can't find path", antId);
                            CancelCurrentTarget();
                        }
                        else 
                        {
                            lastBadPoints.clear();
                        }

                    }
                    
                    
                }
                
            }
        }
            break;
        case STATUS_ON_CLEAR_THE_WAY:
        {
            ReturningToThePath();
        }
            break;
        case STATUS_RETURNING_TO_THE_PATH:
        {
            LOG("ANT_%d is on returning to the path", antId);
        }
            break;
    }
    
    
    
    if (!currentPath) 
    {
        LOG("ANT_%d has no path", antId);
    }
    
}

ControlPoint *Ant::FindBestPoint()
{
    ControlPoint *bestPoint = NULL;
    list<ControlPoint *> totalPts;
    int sz = currentPlace->connectedPoints.size();
    for (int i = 1; i < sz; i++) 
    {
        int csz = currentPlace->connectedPoints[i]->connectedPoints.size();
        for(int n = 1; n < csz; n++)
        {
            if (currentPlace->connectedPoints[i]->connectedPoints[n]->type != ControlPoint::HIVE)
            {
                totalPts.push_back(currentPlace->connectedPoints[i]->connectedPoints[n]);
            }
        }
    }
    totalPts.sort();
    totalPts.unique();
    float minInf = 999999.f;
    static const float infDelta = 0.01;
    list<ControlPoint *> likePts;

    int lbsz = lastBadPoints.size();
    bool isBad = false;
    for(list<ControlPoint *>::iterator it = totalPts.begin(); it != totalPts.end(); it++)
    {
        for (int i = 0; i < lbsz; i++) 
        {
            if (lastBadPoints[i] == (*it))
            {
                isBad = true;
                break;
            }
        }
        if (isBad) 
        {
            continue;
        }
        int sz = 0;
        float inf = (float)((*it)->influence) / (float)((*it)->connectedPoints.size());
        if ((*it)->type == ControlPoint::HIVE)
        {
            inf *= 10.f;
        }
        LOG("ANT_%d influence on point %d is %f", antId, (*it)->curId, inf);
        inf *= (*it)->interest;
        LOG("ANT_%d influence on point %d is %f", antId, (*it)->curId, inf);
        if (inf < minInf - infDelta)
        {//тодо: попробовать не искать точку с минимальным влиянием, а рассчитывать в какой точке муравей максимально может увеличить свое влияние на точки
            minInf = inf;
            likePts.clear();
            likePts.push_back((*it));
        }
        else if (inf < minInf + infDelta)
        {
            minInf = inf;
            likePts.push_back((*it));
        }
    }

    LOG("ANT_%d find %d points like to move", antId, likePts.size());

/*    
    totalPts = likePts;
    likePts.clear();
    stepsToTargetCouner = 0;
    int minVal = 999999;
    int valCnt = 0;
    for(list<ControlPoint *>::iterator it = totalPts.begin(); it != totalPts.end(); it++)
    {
        LOG("Total visits of %d is %d", (*it)->curId, (*it)->totalVisits);
        if ((*it)->totalVisits < minVal)
        {
            LOG("New min %d", (*it)->curId);
            minVal = (*it)->totalVisits;
            likePts.clear();
            likePts.push_back((*it));
            valCnt = 0;
        }
        else if ((*it)->totalVisits == minVal)
        {
            likePts.push_back((*it));
            valCnt++;
        }
    }
 */

    totalPts = likePts;
    likePts.clear();
    stepsToTargetCouner = 0;
    float minVal = 99999.f;
    int valCnt = 0;
    for(list<ControlPoint *>::iterator it = totalPts.begin(); it != totalPts.end(); it++)
    {
        float tv = (float)((*it)->totalVisits) / (float)((*it)->connectedPoints.size());
        if ((*it)->type == ControlPoint::HIVE)
        {
            tv *= 10.f;
        }
        tv *= (*it)->interest;
        LOG("Total visits of %d is %f", (*it)->curId, tv);
        if (tv < minVal - 0.001f)
        {
            LOG("New min %d", (*it)->curId);
            minVal = tv;
            likePts.clear();
            likePts.push_back((*it));
            valCnt = 0;
        }
        else if (tv < minVal + 0.001f)
        {
            likePts.push_back((*it));
            valCnt++;
        }
    }
    

    
    if (!isScout && worldState->activeHivesCount > 0)
    {
        int stepsToHive = 99999;
        Hive *hv = worldState->bot->GetNearestHive(currentPlace);
        for(list<ControlPoint *>::iterator it = likePts.begin(); it != likePts.end(); it++)
        {
            int steps = (*it)->stepsToHive;
            if (steps < stepsToHive)
            {
                stepsToHive = steps;
                bestPoint = (*it);
            }
        }
    }
    else 
    {
        int stepsToHive = 0;
        Hive *hv = worldState->bot->GetNearestHive(currentPlace);
        for(list<ControlPoint *>::iterator it = likePts.begin(); it != likePts.end(); it++)
        {
            int steps = (*it)->stepsToHive;
            if (steps < ControlPoint::maxPointsDist) 
            {
                if (steps > stepsToHive)
                {
                    stepsToHive = steps;
                    bestPoint = (*it);
                }
            }
        }
    }

    return bestPoint;
}    

/*
ControlPoint *Ant::FindBestPoint()
{//лучший вариант
    ControlPoint *bestPoint = NULL;
    list<ControlPoint *> totalPts;
    int sz = currentPlace->connectedPoints.size();
    for (int i = 1; i < sz; i++) 
    {
        int csz = currentPlace->connectedPoints[i]->connectedPoints.size();
        for(int n = 1; n < csz; n++)
        {
            totalPts.push_back(currentPlace->connectedPoints[i]->connectedPoints[n]);
        }
    }
    totalPts.unique();
    float minInf = 10000.f;
    static const float infDelta = 0.01;
    list<ControlPoint *> likePts;
    for(list<ControlPoint *>::iterator it = totalPts.begin(); it != totalPts.end(); it++)
    {//исчем точки в которых сейчас наименьшее влияние наших мурашей
        float inf = (float)((*it)->influence) / (float)((*it)->connectedPoints.size());
        if (inf < minInf - infDelta)
        {            minInf = inf;
            likePts.clear();
            likePts.push_back((*it));
        }
        else if (inf < minInf + infDelta)
        {
            minInf = inf;
            likePts.push_back((*it));
        }
    }
    
    LOG("ANT_%d find %d points like to move", antId, likePts.size());
    
    totalPts = likePts;
    likePts.clear();
    stepsToTargetCouner = 0;
    float minVal = 99999.f;
    int valCnt = 0;
    for(list<ControlPoint *>::iterator it = totalPts.begin(); it != totalPts.end(); it++)
    {//исем точки в которых количество визитов в точку наименьшее по отношению к количеству связей точки
        float tv = (float)((*it)->totalVisits) / (float)((*it)->connectedPoints.size());
        LOG("Total visits of %d is %f", (*it)->curId, tv);
        if (tv < minVal - 0.001f)
        {
            LOG("New min %d", (*it)->curId);
            minVal = tv;
            likePts.clear();
            likePts.push_back((*it));
            valCnt = 0;
        }
        else if (tv < minVal + 0.001f)
        {
            likePts.push_back((*it));
            valCnt++;
        }
    }
    
    
    int stepsToHive = 99999;
    Hive *hv = worldState->bot->GetNearestHive(currentPlace);
    for(list<ControlPoint *>::iterator it = likePts.begin(); it != likePts.end(); it++)
    {//исчем ту точку что поближе к муравейнику, чтобы сильно далеко его не покидать
        int steps = (*it)->wayToPoints[hv->connectedPoint->curId].pointsLeft;
        if (steps < stepsToHive)
        {
            stepsToHive = steps;
            bestPoint = (*it);
        }
    }
    return bestPoint;
}
 */

/*
ControlPoint *Ant::FindBestPoint()
{
    ControlPoint *bestPoint = NULL;
    stepsToTargetCouner = 0;
    int minVal = 999999;
    int valCnt = 0;
    int sz = currentPlace->connectedPoints.size();
    for (int i = 1; i < sz; i++) 
    {
        VECTOR_BOUNDS(currentPlace->connectedPoints, i);
        ANT_ASSERT(currentPlace->connectedPoints[i]);
        ANT_ASSERT(currentPlace);
        if (currentPlace->connectedPoints[i]->type != ControlPoint::HIVE)
        {
            LOG("Total visits of %d is %d", currentPlace->connectedPoints[i]->curId, currentPlace->connectedPoints[i]->totalVisits);
            if (currentPlace->connectedPoints[i]->totalVisits < minVal)
            {
                LOG("New min %d", currentPlace->connectedPoints[i]->curId);
                minVal = currentPlace->connectedPoints[i]->totalVisits;
                bestPoint = currentPlace->connectedPoints[i];
                valCnt = 0;
            }
            else if (currentPlace->connectedPoints[i]->totalVisits == minVal)
            {
                valCnt++;
            }
        }
    }
    if (valCnt > 1) 
    {//делаем более равномерный разброс для выбора направления движения
        int cnt = currentPlace->directionCounter % valCnt;
        LOG("cnt = %d", cnt);
        currentPlace->directionCounter++;
        for (int i = 0; i < sz; i++) 
        {
            VECTOR_BOUNDS(currentPlace->connectedPoints, i);
            ANT_ASSERT(currentPlace->connectedPoints[i]);
            ANT_ASSERT(currentPlace);
            if (currentPlace->connectedPoints[i]->type != ControlPoint::HIVE)
            {
                if (currentPlace->connectedPoints[i]->totalVisits == minVal)
                {
                    if (cnt == 0) 
                    {
                        LOG("New Selected %d", currentPlace->connectedPoints[i]->curId);
                        bestPoint = currentPlace->connectedPoints[i];
                        return bestPoint;
                    }
                    cnt--;
                }
            }
        }
    }
    return bestPoint;
}
*/

void Ant::ProcessFoodHunterMovement()
{
    LOG("ANT_%d move as hunter", antId);
    if (currentPath) 
    {
        FollowPath();
    }
}

void Ant::ProcessTerminatorTargeting()
{
    LOG("ANT_%d targeting as terminator", antId);
    if (currentPath) 
    {
        CheckingPathForWater();
    }
    if (!targetHive) 
    {
        OnTypeChanged(ANT_FOOD_HUNTER);
        return;
    }
    if (!currentPath) 
    {
        currentPath = worldState->bot->pathFinder->GetPath(x, y, targetHive->x, targetHive->y);
        if (currentPath && currentPath->IsFinished()) 
        {
            OnPathIsFinished();
        }
        if (!currentPath) 
        {
            if (targetHive)
            {
                LOG("ANT_%d Releasing hive", antId);
                targetHive->UnregisterTerminators(this);
                targetHive = NULL;
                LOG("ANT_%d hive released", antId);
                OnTypeChanged(ANT_FOOD_HUNTER);
            }
            OnTypeChanged(ANT_FOOD_HUNTER);//TODO: некоторое время не проверять на муравейники
        }
    }
    
}

void Ant::ProcessTerminatorMovement()
{
    LOG("ANT_%d move as terminator", antId);
    if (currentPath) 
    {
        if (true)
        {
            FollowPath();
        }
/*        else 
        {//отходим на безопасное расстояние если это необходимо и ждем
            LOG("ANT_%d Searching for retreat way", antId);
            int dx = 0;
            int dy = 0;
            for (vector<ControlPoint*>::iterator it = currentPlace->connectedPoints.begin(); it != currentPlace->connectedPoints.end(); it++)
            {
                int sz = (*it)->enemies.size();
                LOG("ANT_%d  %d Enemies in area", antId, sz);
                for (int i = 0; i < sz; i++) 
                {
                    VECTOR_BOUNDS((*it)->enemies, i);
                    if (DistanceSq((*it)->enemies[i]) <= worldState->movedAttackRadiusSq)
                    {
                        LOG("ANT_%d Enemies in attack range %d, %d", antId, (*it)->enemies[i]->x, (*it)->enemies[i]->y);
                        
                        int fx = (*it)->enemies[i]->x;
                        int fy = (*it)->enemies[i]->y;
                        if (fx - x > State::width/2)
                        {
                            fx -= State::width;
                        }
                        else if (fx - x < -State::width/2)
                        {
                            fx += State::width;
                        }
                        
                        if (fy - y > State::height/2)
                        {
                            fy -= State::height;
                        }
                        else if (fy - y < -State::height/2)
                        {
                            fy += State::height;
                        }
                        
                        dx += x - fx;
                        dy += y - fy;
                        LOG("ANT_%d new delta %d, %d", antId, dx, dy);
                    }
                }
            }
            if (dx != 0 || dy != 0) 
            {
                SafeRelease(currentPath);
                if (abs(dx) >= abs(dy)) 
                {
                    if (dx < 0) 
                    {
                        LOG("ANT_%d retreat left", antId);
                        MoveAntToDirection(MOVE_LEFT, false);
                    }
                    else 
                    {
                        LOG("ANT_%d retreat right", antId);
                        MoveAntToDirection(MOVE_RIGHT, false);
                    }
                }
                else 
                {
                    if (dy < 0) 
                    {
                        LOG("ANT_%d retreat up", antId);
                        MoveAntToDirection(MOVE_UP, false);
                    }
                    else 
                    {
                        LOG("ANT_%d retreat down", antId);
                        MoveAntToDirection(MOVE_DOWN, false);
                    }
                }
                
            }
            
        }
   */     
    }
 
}










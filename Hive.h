/*
 *  Hive.h
 *  HottychBot
 *
 *  Created by Alexey Prosin on 10/30/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef _HIVE_H_
#define _HIVE_H_

#include "PositionedObject.h"
#include "vector"

class Ant;
class ControlPoint;
class Poi;

class Hive : public PositionedObject
{
public:
    
    enum eHiveState
    {
        HIVE_ACTIVE = 0,
        HIVE_UNKNOWN,
        HIVE_DEAD
    };
    
    Hive()
    : PositionedObject()
    {
        owner = 0;
        protectorsRequired = 0;
        protCounter = 0;
    };
    
    Hive(int _x, int _y, int _index, int _owner);
    ~Hive();
    void RegisterAnt(Ant *ant);
    void UnregisterAnt(Ant *ant);
    
    int GetProtectorsCount()
    {
        return protectors.size();
    }

    Ant *GetProtector(int num);
    
    
    void RegisterTerminators(Ant *ant);
    void UnregisterTerminators(Ant *ant);

    
    int owner;
    int state;
    
    int protectorsRequired;
    int protCounter;
    ControlPoint *connectedPoint;
    Poi *poi;

    std::vector<Ant *> terminators;
    
protected:
    
    std::vector<Ant *> protectors;
};

#endif //_HIVE_H_

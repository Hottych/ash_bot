//
//  HBUnitTests.m
//  HBUnitTests
//
//  Created by Denis Fileev on 11/26/11.
//  Copyright (c) 2011 DAVA Consulting LLC. All rights reserved.
//

#import "WarriorTeam.h"
#import "State.h"
#import "Enemy.h"
#import "Ant.h"
#import "WaveField.h"
#import "WavePathFinder.h"

#import "WarriorTeamTestCase.h"

@interface WarriorTeamTestCase ()

- (void)checkForMapSize:(int[2])mapSize
                mapData:(int *)map
         expectedResult:(int *)result;

- (void)checkForMapSize:(int[2])mapSize
               antCount:(int)antCount 
           antPositions:(int[][2])antPositions 
           enemiesCount:(int)enemiesCount 
         enemyPositions:(int[][2])enemyPositions 
         expectedResult:(int *)result;

@end

@implementation WarriorTeamTestCase

struct teststruct {
    int i;
    int *p;
};

- (void)test1_0
{
//    [self checkForMapSize:(int[2]){20, 30} 
//                 antCount:2 antPositions:(int [][2]){
//                     {9, 13},
//                     {9, 14}
//                 }
//             enemiesCount:2 enemyPositions:(int[][2]){
//                 {13, 12},
//                 {6, 14}
//             } 
//           expectedResult:NULL];
    
        
    [self checkForMapSize:(int[2]){17, 14} 
             mapData:(int []){
                 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                 0, 0, 3, 2, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0,
                 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0,
                 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
           }
     expectedResult:NULL];
}

- (void)checkForMapSize:(int[2])mapSize
                mapData:(int *)map
         expectedResult:(int *)result {
    
    WarriorTeam *team;
    State *state;
    std::vector<Enemy *> enemies;
    
    state = new State();
    state->terrainMap = std::vector<int>(mapSize[0] * mapSize[1]);
    state->SetAttackRadiusSq(5);
    State::width = mapSize[0];
    State::height = mapSize[1];
    
    team = new WarriorTeam(state);
    
    enemies.clear();
    state->myAntsMap = std::vector<Ant *>(State::height * State::width);
    
//    for (int i = 0; i < mapSize[0]; i++) {
//        for (int j = 0; j < mapSize[1] ; j++) {
    for (int i = mapSize[0]; i >= 0; i--) {
        for (int j = mapSize[1]; j >= 0; j--) {
            int index = state->GetNormalizedMapIndex(i, j);
            if (3 == map[index]) { // water
                state->terrainMap[index] = State::WATER;
            } else if (1 == map[index]) { // member
                Ant *ant = new Ant(state, i, j, index);
                ant->SetPosition(i, j);
                state->myAntsMap[index] = ant;
                team->AddAnt(ant);
                state->terrainMap[index] = State::ANT;
            } else if (2 == map[index]) { // enemy
                Enemy *enemy = new Enemy(i, j, index, 1);
                enemy->SetPosition(i, j);
                team->AddEnemy(enemy);
                enemies.push_back(enemy);
            }
        }        
    }    
  
//    team->CalculateEnemyAttackArea();
//    std::vector<int> value0;
//    value0.assign(team->calculatedEnemyAttackArea.begin(), team->calculatedEnemyAttackArea.end());
//
//    team->CalculateEnemyAttackAreaV2();
//    std::vector<int> value1;
//    value1.assign(team->calculatedEnemyAttackArea.begin(), team->calculatedEnemyAttackArea.end());
    
//    for (int i = 0; i < value0.size(); i++) {
//        STAssertEquals(value0[i], value1[i], @"fockey!");
//    }

    team->attackStrategy = 0;
    
    team->ProcessTurn();
    
//    BOOL isEqualWithResult = YES;
//    for (int j = 0; j < mapSize[1]; j++) {
//        for (int i = 0; i < mapSize[0]; i++) {
//            int index = i + j * State::width;
//            printf("%d ", team->calculatedEnemyAttackArea[index]);
//            if (team->calculatedEnemyAttackArea[index] != result[index]) {
//                isEqualWithResult = NO;
//            }
//        }
//        printf("\n");
//    }
    
//    STAssertTrue(isEqualWithResult, @"Fockey!");
    
    for (int i = 0; i < enemies.size(); i++) {
        delete enemies[i];
    }

    for (int i = 0; i < state->myAntsMap.size(); i++) {
        delete state->myAntsMap[i];
    }
}

- (void)checkForMapSize:(int[2])mapSize
           antCount:(int)antCount 
         antPositions:(int[][2])antPositions 
           enemiesCount:(int)enemiesCount 
              enemyPositions:(int[][2])enemyPositions 
              expectedResult:(int *)result 
{
    int *map = (int *)malloc(mapSize[0] * mapSize[1] * sizeof(int));
    memset(map, 0, mapSize[0] * mapSize[1] * sizeof(int));
    
    for (int i = 0; i < antCount; i++) {
        int index = mapSize[0] * antPositions[i][1] + antPositions[i][0];
        map[index] = 1;
    }    

    for (int i = 0; i < enemiesCount; i++) {
        int index = mapSize[0] * enemyPositions[i][1] + enemyPositions[i][0];
        map[index] = 2;
    }    
    
    [self checkForMapSize:mapSize mapData:map expectedResult:result];
    
    free(map);
}

@end

/*
 *  Poi.h
 *  HottychBot
 *
 *  Created by Alexey Prosin on 12/17/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef _POI_H_
#define _POI_H_

#include "vector"

class ControlPoint;

class Poi
{
public:
    
    Poi(ControlPoint *centerPoint, int radius, float from, float to);//radius - радиус в контрол поинтах(не квадрат радиуса),
                                                                     //from - значение интенсивности интереса в центре пои,
                                                                     //to - значение интереса на краю пои
                                                                     //значение от 0.001 и до 1.0 - положительный интерес, чем меньше значение тем больше интерес
                                                                     //значение от 1.0 до 100 - отрицательный интерес, чем больше значение тем меньше интерес
    ~Poi();
    
    void ApplyField();
    void DeapplyField();

    void RecheckField();
    
    void ChangeField(float from, float to);
    void ChangeField(int radius, float from, float to);
    void ReplaceField(ControlPoint *newCenter);
    
    ControlPoint *center;
    float valueFrom;
    float valueTo;
    int poiSteps;
    std::vector<ControlPoint *> pointsList;
    std::vector<float> pointsStep;
protected:
    void GeneratePoiWave();
};

#endif //_POI_H_

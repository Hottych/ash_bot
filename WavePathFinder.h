/*
 *  WavePathFinder.h
 *  HottychBot
 *
 *  Created by Alexey Prosin on 11/5/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef _WAVE_PATH_FINDER_H_
#define _WAVE_PATH_FINDER_H_

#include "vector"
#include "PathFinder.h"
#include "WaveField.h"

class Path;
class State;
class WavePathFinder : public PathFinder
{
public:

    WavePathFinder(State *statePtr);
    
    Path *GetPath(int fromX, int fromY, int toX, int toY);

    
    WaveField *field;
};

#endif
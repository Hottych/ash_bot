#ifndef LOCATION_H_
#define LOCATION_H_

/*
    struct for representing locations in the grid.
*/
struct Location
{
    int x;
    int y;

    Location()
    {
        x = y = 0;
    };

    Location(int _x, int _y)
    {
        x = _x;
        y = _y;
    };
};

#endif //LOCATION_H_

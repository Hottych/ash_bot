/*
 *  PathFinder.h
 *  HottychBot
 *
 *  Created by Alexey Prosin on 11/1/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef _PATH_FINDER_H_
#define _PATH_FINDER_H_


class State;
class PathFinder
{
public:
    
    PathFinder(State *statePtr);
    
    State *state;
};

#endif
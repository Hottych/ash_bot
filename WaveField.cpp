/*
 *  WaveField.cpp
 *  HottychBot
 *
 *  Created by Alexey Prosin on 11/5/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#include "WaveField.h"
#include "State.h"
#include "Bot.h"

#define WAVE_FIELD_LOG_VERBOSE

using namespace std;

const int WaveField::maxStepValue = 1000*1000;

WaveField::WaveField(State *statePtr, int width, int height)
{
#ifdef WAVE_FIELD_LOG_VERBOSE
    LOG("Wave field init", 0);
#endif
    mapWidth = width;
    mapHeight = height;
    
    
    pathMap = vector<int>(mapWidth * mapHeight);
    unitX = mapWidth / 2;
    unitY = mapHeight / 2;
    stepsLimit = 100;
    
    centerX = unitX;
    centerY = unitY;

    targetX = targetY = -1;
    
    state = statePtr;
#ifdef WAVE_FIELD_LOG_VERBOSE
    LOG("Wave field init DONE", 0);
#endif
}

WaveField::WaveField(const WaveField &field)
{
    mapWidth = field.mapWidth;
    mapHeight = field.mapHeight;
    
    
    pathMap = field.pathMap;
    unitX = field.unitX;
    unitY = field.unitY;
    stepsLimit = field.stepsLimit;
    
    centerX = field.centerX;
    centerY = field.centerY;
    
    
    state = field.state;
    
    mapOffsetX = field.mapOffsetX;
    mapOffsetY = field.mapOffsetY;

    targetX = field.targetX;
    targetY = field.targetY;
}

bool WaveField::GenerateFieldWithFullClearing()
{
//    state->bug.Log("Startign field generation with full clearing");
//    state->bug.Log("From: %d, %d", unitX, unitY);
//    state->bug.Log("To: %d, %d", targetX, targetY);
    int sz = pathMap.size();
    for (int i = 0; i < sz; i++)
    {
        VECTOR_BOUNDS(pathMap, i);
        pathMap[i] = maxStepValue;
    }
    VECTOR_BOUNDS(pathMap, unitX + unitY * mapWidth);
    pathMap[unitX + unitY * mapWidth] = 0;
    targetInd = targetX + targetY * mapWidth;
    step = 1;
    noSteps = false;
    while (true) 
    {
        fieldFromX = max(unitX - step, 0);
        fieldFromY = max(unitY - step, 0);
        fieldToX = min(unitX + step, mapWidth - 1);
        fieldToY = min(unitY + step, mapHeight - 1);
        
        
        if (ProcessFieldStep(step - 1)) 
        {
            return true;
        }
        
        step++;
        if (step >= stepsLimit || noSteps)
        {
            if (noSteps) 
            {
//                state->bug.Log("NO Steps");
            }
            else 
            {
//                state->bug.Log("Steps limit reached");
            }
            
            return false;
        }
    }
}


bool WaveField::GenerateField()
{
#ifdef WAVE_FIELD_LOG_VERBOSE
    LOG("Startign field generation", 0);
    LOG("From: %d, %d", unitX, unitY);
    LOG("To: %d, %d", targetX, targetY);
#endif
    VECTOR_BOUNDS(pathMap, unitX + unitY * mapWidth);
    pathMap[unitX + unitY * mapWidth] = 0;
    targetInd = targetX + targetY * mapWidth;
    step = 1;
    noSteps = false;
    while (true) 
    {
        int ffX = unitX - step;
        int ffY = unitY - step;
        int ftX = unitX + step;
        int ftY = unitY + step;
        fieldFromX = max(ffX, 0);
        fieldFromY = max(ffY, 0);
        fieldToX = min(ftX, mapWidth - 1);
        fieldToY = min(ftY, mapHeight - 1);
        
        if (ffY >= 0)
        {
            int pos1 = fieldFromY * mapWidth;
            for (int x = fieldFromX; x <= fieldToX; x++) 
            {
                VECTOR_BOUNDS(pathMap, x + pos1);
                pathMap[x + pos1] = maxStepValue;
            }
        }
        if (ftY < mapHeight) 
        {
            int pos2 = fieldToY * mapWidth;
            for (int x = fieldFromX; x <= fieldToX; x++) 
            {
                VECTOR_BOUNDS(pathMap, x + pos2);
                pathMap[x + pos2] = maxStepValue;
            }
        }
        if (ffX >= 0) 
        {
            for (int y = fieldFromY; y <= fieldToY; y++)
            {
                VECTOR_BOUNDS(pathMap, fieldFromX + y * mapWidth);
                pathMap[fieldFromX + y * mapWidth] = maxStepValue;
            }
        }
        if (ftX < mapWidth) 
        {
            for (int y = fieldFromY; y <= fieldToY; y++)
            {
                VECTOR_BOUNDS(pathMap, fieldToX + y * mapWidth);
                pathMap[fieldToX + y * mapWidth] = maxStepValue;
            }
        }
        
        if (ProcessFieldStep(step - 1)) 
        {
            return true;
        }
        
        step++;
        if (step >= stepsLimit || noSteps)
        {
            if (noSteps) 
            {
                LOG("NO Steps", 0);
            }
            else 
            {
                LOG("Steps limit reached", 0);
            }

            return false;
        }
    }
}

bool WaveField::ProcessFieldStep(int stepNum)
{
//    LOG("Processing step: %d", stepNum);
//    LOG("From X: %d  to X: %d", fieldFromX, fieldToX);
//    LOG("From Y: %d  to Y: %d", fieldFromY, fieldToY);
 
    noSteps = true;
    for (int y = fieldFromY; y <= fieldToY; y++)
    {
        int indY = y * mapWidth;
        for (int x = fieldFromX; x <= fieldToX; x++) 
        {
            int ind = indY + x;
            VECTOR_BOUNDS(pathMap, ind);
            if (pathMap[ind] == stepNum)
            {
                noSteps = false;
                if (targetInd == ind) 
                {
                    return true;
                }
                int chX = x - 1;
                int chY = y;
                if (chX >= 0 && pathMap[ind - 1] == maxStepValue)
                {
                    VECTOR_BOUNDS(pathMap, ind - 1);

                    int tind = state->GetNormalizedMapIndex(chX + mapOffsetX, chY + mapOffsetY);
                    VECTOR_BOUNDS(state->terrainMap, tind);
                    if (state->terrainMap[tind] != State::WATER && state->terrainMap[tind] != State::HIVE)
                    {
                        pathMap[ind - 1] = stepNum + 1;
                    }
                    else 
                    {
                        pathMap[ind - 1] = maxStepValue + 1;
                    }

                }
                chX = x + 1;
                if (chX < mapWidth && pathMap[ind + 1] == maxStepValue)
                {
                    VECTOR_BOUNDS(pathMap, ind + 1);

                    int tind = state->GetNormalizedMapIndex(chX + mapOffsetX, chY + mapOffsetY);
                    VECTOR_BOUNDS(state->terrainMap, tind);
                    if (state->terrainMap[tind] != State::WATER && state->terrainMap[tind] != State::HIVE)
                    {
                        pathMap[ind + 1] = stepNum + 1;
                    }
                    else 
                    {
                        pathMap[ind + 1] = maxStepValue + 1;
                    }
                }
                chX = x;
                chY = y - 1;
                if (chY >= 0 && pathMap[ind - mapWidth] == maxStepValue)
                {
                    VECTOR_BOUNDS(pathMap, ind - mapWidth);

                    int tind = state->GetNormalizedMapIndex(chX + mapOffsetX, chY + mapOffsetY);
                    VECTOR_BOUNDS(state->terrainMap, tind);
                    if (state->terrainMap[tind] != State::WATER && state->terrainMap[tind] != State::HIVE)
                    {
                        pathMap[ind - mapWidth] = stepNum + 1;
                    }
                    else 
                    {
                        pathMap[ind - mapWidth] = maxStepValue + 1;
                    }
                }
                chY = y + 1;
                if (chY < mapHeight && pathMap[ind + mapWidth] == maxStepValue)
                {
                    VECTOR_BOUNDS(pathMap, ind + mapWidth);

                    int tind = state->GetNormalizedMapIndex(chX + mapOffsetX, chY + mapOffsetY);
                    VECTOR_BOUNDS(state->terrainMap, tind);
                    if (state->terrainMap[tind] != State::WATER && state->terrainMap[tind] != State::HIVE)
                    {
                        pathMap[ind + mapWidth] = stepNum + 1;
                    }
                    else 
                    {
                        pathMap[ind + mapWidth] = maxStepValue + 1;
                    }
                }
            }
        }
    }
    
    return false;
}

bool WaveField::GenerateFakeField()
{
    LOG("Fake field generation", 0);
    for (int y = 0; y < mapHeight; y++)
    {
        int indY = y * mapWidth;
        for (int x = 0; x < mapWidth; x++) 
        {
            VECTOR_BOUNDS(pathMap, indY + x);
            pathMap[indY + x] = maxStepValue;
        }
    }
            
    VECTOR_BOUNDS(pathMap, unitX + unitY * mapWidth);
    pathMap[unitX + unitY * mapWidth] = 0;
    targetInd = targetX + targetY * mapWidth;
    step = 1;
    while (true) 
    {
        fieldFromX = max(unitX - step, 0);
        fieldFromY = max(unitY - step, 0);
        fieldToX = min(unitX + step, mapWidth - 1);
        fieldToY = min(unitY + step, mapHeight - 1);
        
        bool noSteps = true;
        int st = step - 1;
        for (int y = fieldFromY; y <= fieldToY; y++)
        {
            int indY = y * mapWidth;
            for (int x = fieldFromX; x <= fieldToX; x++) 
            {
                int ind = indY + x;
                VECTOR_BOUNDS(pathMap, ind);
                if (pathMap[ind] == st)
                {
                    noSteps = false;
                    int chX = x - 1;
                    int chY = y;
                    if (chX >= 0 && pathMap[ind - 1] == maxStepValue)
                    {
                        VECTOR_BOUNDS(pathMap, ind - 1);
                        pathMap[ind - 1] = step;
                    }
                    chX = x + 1;
                    if (chX < mapWidth && pathMap[ind + 1] == maxStepValue)
                    {
                        VECTOR_BOUNDS(pathMap, ind + 1);
                        pathMap[ind + 1] = step;
                    }
                    chX = x;
                    chY = y - 1;
                    if (chY >= 0 && pathMap[ind - mapWidth] == maxStepValue)
                    {
                        VECTOR_BOUNDS(pathMap, ind - mapWidth);
                        pathMap[ind - mapWidth] = step;
                    }
                    chY = y + 1;
                    if (chX < mapHeight && pathMap[ind + mapWidth] == maxStepValue)
                    {
                        VECTOR_BOUNDS(pathMap, ind + mapWidth);
                        pathMap[ind + mapWidth] = step;
                    }
                }
            }
        }
        
        
        step++;
        if (step >= stepsLimit || noSteps)
        {
            if (noSteps) 
            {
//                state->bug.Log("NO Steps");
            }
            else 
            {
//                state->bug.Log("Steps limit reached");
            }
            
            return false;
        }
    }
}

void WaveField::Log()
{
    LOG("WaveField print", 0);
    char txBuf[4 * 1024];
    for (int y = 0; y < mapHeight; y++)
    {
        int indY = y * mapWidth;
        txBuf[0] = 0;
        for (int x = 0; x < mapWidth; x++) 
        {
            VECTOR_BOUNDS(pathMap, indY + x);
            if (pathMap[indY + x] == maxStepValue) 
            {
                sprintf(txBuf, "%s _N_", txBuf);
            }
            else if (pathMap[indY + x] == maxStepValue + 1)
            {
                sprintf(txBuf, "%s  W ", txBuf);
            }
            else 
            {
                sprintf(txBuf, "%s %.3d", txBuf, pathMap[indY + x]);
            }
        }
        LOG(txBuf, 0);
    }
}

int WaveField::DistanceToPoint(int destX, int destY)
{
    int fx = destX;
    int fy = destY;
    
    int x = mapOffsetX + centerX;
    int y = mapOffsetY + centerY;

    if (fx - x > State::width/2)
    {
        fx -= State::width;
    }
    else if (fx - x < -State::width/2)
    {
        fx += State::width;
    }
    
    if (fy - y > State::height/2)
    {
        fy -= State::height;
    }
    else if (fy - y < -State::height/2)
    {
        fy += State::height;
    }
    
    fx = fx - mapOffsetX;
    fy = fy - mapOffsetY;
    
    int result = MAX_MAP_SIZE;
    
    if (fx >= 0 && fy >= 0 && fx < mapWidth && fy < mapHeight) 
    {
        result = pathMap[fx + fy * mapWidth];
    }
    
    return result;
}




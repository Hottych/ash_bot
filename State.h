#ifndef STATE_H_
#define STATE_H_

#include <iostream>
#include <stdio.h>
#include <cstdlib>
#include <cmath>
#include <string>
#include <vector>
#include <queue>
#include <stack>
#include <stdint.h>

#include "Timer.h"
#include "Bug.h"
#include "Square.h"
#include "Location.h"
#include "Hive.h"
#include "ControlPoint.h"
#include "Food.h"

/*
    constants
*/
const int TDIRECTIONS = 4;
const char CDIRECTIONS[4] = {'N', 'E', 'S', 'W'};

class Ant;
class Bot;
class Enemy;
class WarriorTeam;

struct WaveFieldMapCell
{
    bool isFullyValid;
    WaveField *field;
};

struct tRect
{
    int x1, y1, x2, y2;
 
    tRect(int _x1, int _y1, int _x2, int _y2) : x1(_x1), y1(_y1), x2(_x2), y2(_y2) { };
    
    bool isIntersect(tRect &r)
    {
        return (r.x2 >= x1 && r.x1 <= x2 && r.y2 >= y1 && r.y1 <= y2);
    }

//    tRect intersect(tRect &r)
//    {
//        return tRect(std::min(x1, r.x1), std::min(y1, r.y1), std::max(x2, r.x2), std::max(y2, r.y2));
//    }
    void intersect(tRect &r)
    {
        x1 = std::min(x1, r.x1);
        y1 = std::min(y1, r.y1);
        x2 = std::max(x2, r.x2);
        y2 = std::max(y2, r.y2);
    }
};

/*
    struct to store current state information
*/
class State
{
public:
    enum eCellTypes 
    {
        GROUND = 0,
        WATER,
        FOOD,
        
        ANT = 1000,
        HIVE = 10000
    };
    /*
        Variables
    */
    static int width;
    static int height;
//    static int controlAreaWidth;
//    static int controlAreaHeight;
    static int currentTurn;
    

    /*
        Functions
    */
    State();
    ~State();

    void Setup();

    void Refresh();//вызывается после получения новых данных, но перед ходом
    void Reset();//вызывается в конце каждого хода

    void MakeMove(Ant *ant, int direction);

    
    int GetNormalizedMapIndex(int x, int y);//отдает индекс с учетом того что координаты могут быть больше размеров карты или меньше нуля
    
    int DistanceSq(int x0, int y0, int x1, int y1);

    bool NeedToKillHive(Hive *h);
    void UpdateMyHive(int x, int y);
    void UpdateEnemyHive(int x, int y, int player);

    void UpdateEnemy(int x, int y, int player);

    void UpdateMyAnt(int x, int y);
    void RemoveMyAnt(Ant *ant);

    void UpdateFood(int x, int y);
    void RemoveFood(Food *fd);

    void UpdateWater(int x, int y);
    
    void AddWarriorTeam(WarriorTeam *aTeam);
    void RemoveWarriorTeam(WarriorTeam *aTeam);
    
    void SetAttackRadiusSq(double arg);
    
    int turnsCount;
    
    int numberOfPlayers;
    
    int attackRadiusSq;
    double spawnRadiusSq;
    int viewRadiusSq;
    
    double movedAttackRadius;//радиус атаки с учетом того что атакующие сдвинутся
    int movedAttackRadiusSq;//радиус атаки с учетом того что атакующие сдвинутся

    double attackRadius;
    int attackAreaSize_2;
    int attackAreaSize;
    int attackAreaExtendedSize;

    int **attackMask;
    int **attackMaskExtended;
    
    double spawnRadius;
    double viewRadius;
    
    double loadtime;
    double turntime;
    std::vector<double> scores;
    bool gameover;
    int64_t seed;
    
    // АК: маска видимости мураша
    int viewTemplateWidth, viewTemplateHeight;
    std::vector<int> viewTemplate;
    
    std::vector<int> terrainMap;//чисто террейн и типы объектов
    std::vector<PositionedObject *> objectsMap;//указатели на все неподвижные объекты(ульи, еда)
    std::vector<Ant *> myAntsMap;//наши мураши
    
    std::vector<Enemy *> enemiesMap;//наши мураши
    
//    std::vector<ControlPoint *> controlMap;//возможно не нужно
//    std::vector<ControlArea *> controlAreas;//расположенны упорядоченно
    std::vector<ControlPoint *> controlAreasMap;//расположенны в виде карты исключительно в целях ускорения доступа
                                                //очень аккуратно работайте с этой картой! Помните что она динамически изменяется каждый ход!
    
//    std::vector<int> enemyDensityMap; // карта напряженности
    
    std::vector<ControlPoint *>controlPoints;//поинты лежат почти упорядочено по положению, но затачиваться на это не стоит.
    
    std::vector<Hive*> myHives;
    int activeHivesCount;
    std::vector<Hive*> enemyHives;

    std::vector<WarriorTeam *> warriorTeams;
    
//    std::vector<int> attackMask;
//    int attackMaskSize_2;
    
    std::vector<Ant*> myAnts;

    std::vector<Enemy*> enemies;

    std::vector<Food*> food;

    void AddInvalidRect(tRect &r);
    std::vector<tRect> invalidRects;

    Bot *bot;
    
    Timer timer;
    Bug bug;
    
//    // stats    
//    std::vector<double> *move0EnemyProbability;
//    std::vector<double> *move1EnemyProbability;

private:    
//    std::vector<double> move0EnemyProbabilityStorage;
//    std::vector<double> move1EnemyProbabilityStorage;

    std::vector<WaveFieldMapCell> waveFieldsMap;
    
public:
    WaveFieldMapCell GetWaveFieldMapCell(int x, int y);

#ifdef DEBUG
    std::ofstream mainstream;
#endif

};



std::ostream& operator<<(std::ostream &os, const State &state);
std::istream& operator>>(std::istream &is, State &state);

#endif //STATE_H_

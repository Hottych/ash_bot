//
//  WarriorTeam.cpp
//  HottychBot
//
//  Created by Denis Fileev on 11/20/11.
//  Copyright (c) 2011 DAVA Consulting LLC. All rights reserved.
//

#include "Ant.h"
#include "Enemy.h"
#include "Bug.h"
#include "Utils.h"
#include "Path.h"
#include "WavePathFinder.h"
#include "Bot.h"
#include "State.h"
#include "ControlPoint.h"
#include <limits.h>
#include "Poi.h"

#include "WarriorTeam.h"

#ifdef DEBUG
//#define WARTEAM_DEBUG_FULL
#endif

#ifdef WARTEAM_DEBUG_FULL
//#define WARTEAM_PRINT_MAP
#endif

#ifdef UNIT_TEST_LOG_ENABLED
    #define WAR_LOG(msg, ...) LOG(msg, __VA_ARGS__)
#elif defined WARTEAM_DEBUG_FULL
    #define WAR_LOG(msg, ...) Bug::Log(msg, __VA_ARGS__)
#else
    #define WAR_LOG(msg, ...) 
#endif

#undef WAR_LOG
#define WAR_LOG(msg, ...) 

#define MAX_HISTORY_SIZE 10

static const double kMinStepProbability = 0.3;

using namespace std;

static double maxTurnTime = 0;

static int nextID = 1;

int compare_available_moves(const void *move0, const void *move1);

WarriorTeam::WarriorTeam(State *statePtr)
: stepsLeftToUpdateTarget(0)
, enemiesCountOnLastUpdate(MAX_MAP_SIZE)
{
    worldState = statePtr;
    
    if (nextID >= INT_MAX - 1) {
        nextID = 1;
    }
    teamID = nextID++;
    poi = NULL;

    LOG("WARTEAM_%d CONSTRUCTOR", teamID);
}

WarriorTeam::WarriorTeam(State *statePtr, WarriorTeam *_team0, WarriorTeam *_team1)
: stepsLeftToUpdateTarget(0)
, enemiesCountOnLastUpdate(MAX_MAP_SIZE)
{

    worldState = statePtr;
    teamID = nextID++;
    //    tempMemberCoordinates = NULL;
    LOG("WARTEAM_%d CONSTRUCTOR", teamID);
    
    vector<Ant *> all_members;
    all_members.insert(all_members.end(), _team0->members.begin(), _team0->members.end());
    all_members.insert(all_members.end(), _team1->members.begin(), _team1->members.end());
    
    for (vector<Ant *>::iterator it = all_members.begin(); it != all_members.end(); it++)        
    {
        (*it)->warriorTeam->RemoveAnt(*it);        
        AddAnt((*it));
    }
    
    vector<Enemy *> all_enemies;
    all_enemies.insert(all_enemies.end(), _team0->enemies.begin(), _team0->enemies.end());
    all_enemies.insert(all_enemies.end(), _team1->enemies.begin(), _team1->enemies.end());
    
    for (vector<Enemy *>::iterator it = all_enemies.begin(); it != all_enemies.end(); it++)
    {
        (*it)->warriorTeam->RemoveEnemy(*it);
        AddEnemy(*it);
    }
    
    worldState->RemoveWarriorTeam(_team0);
    worldState->RemoveWarriorTeam(_team1);

    poi = NULL;
    
//    LogState();
}

WarriorTeam::~WarriorTeam()
{
    LOG("WARTEAM_%d DESTRUCTOR", teamID);
    
    for (int i = members.size() - 1; i >= 0; i--)
    {
        VECTOR_BOUNDS(members, i)
        RemoveAnt(members[i]);
    }
    
    for (int i = enemies.size() - 1; i >= 0; i--)
    {
        VECTOR_BOUNDS(enemies, i)
        RemoveEnemy(enemies[i]);
    }
    enemies.clear();
    if (poi) 
    {
        delete poi;
        poi = NULL;
    }
}

void WarriorTeam::AddAnt(Ant *ant)
{
    LOG("WARTEAM_%d ADD ANT_%d", teamID, ant->antId);
    
    ANT_ASSERT(NULL == ant->warriorTeam);
    ANT_ASSERT(-1 == ant->indexInWarriorTeamMembersVector);
    
    ant->warriorTeam = this;
    ant->indexInWarriorTeamMembersVector = members.size();
    members.push_back(ant);
}

void WarriorTeam::RemoveAnt(Ant *ant)
{
    LOG("WARTEAM_%d REMOVE ANT_%d", teamID, ant->antId);
    
    if (ant->indexInWarriorTeamMembersVector < members.size() - 1)
    {
        VECTOR_BOUNDS(members, ant->indexInWarriorTeamMembersVector);
        VECTOR_BOUNDS(members, members.size() - 1);
        VECTOR_BOUNDS(members, members[ant->indexInWarriorTeamMembersVector]->indexInWarriorTeamMembersVector);
        ANT_ASSERT(members[ant->indexInWarriorTeamMembersVector]);
        ANT_ASSERT(members[members.size() - 1]);
        members[ant->indexInWarriorTeamMembersVector] = members[members.size() - 1];
        members[ant->indexInWarriorTeamMembersVector]->indexInWarriorTeamMembersVector = ant->indexInWarriorTeamMembersVector;
    }
    DecreaseVector(members);
    
    ant->indexInWarriorTeamMembersVector = -1;
    ant->warriorTeam = NULL;
}

void WarriorTeam::OnAntIsDead(Ant *ant)
{
    RemoveAnt(ant);
}

void WarriorTeam::AddEnemy(Enemy *aEnemy, int distance)
{
    LOG("WARTEAM_%d ADD ENEMY AT %d %d", teamID, aEnemy->x, aEnemy->y);
    
    ANT_ASSERT(NULL == aEnemy->warriorTeam);
    ANT_ASSERT(-1 == aEnemy->indexInWarriorTeamMembersVector);
    
    aEnemy->warriorTeam = this;
    aEnemy->indexInWarriorTeamMembersVector = enemies.size();
    enemies.push_back(aEnemy);
}

void WarriorTeam::RemoveEnemy(Enemy *aEnemy)
{
    LOG("WARTEAM_%d REMOVE ENEMY AT %d %d", teamID, aEnemy->x, aEnemy->y);
    
    if (aEnemy->indexInWarriorTeamMembersVector < enemies.size() - 1)
    {
        VECTOR_BOUNDS(enemies, aEnemy->indexInWarriorTeamMembersVector);
        VECTOR_BOUNDS(enemies, enemies.size() - 1);
        VECTOR_BOUNDS(enemies, enemies[aEnemy->indexInWarriorTeamMembersVector]->indexInWarriorTeamMembersVector);
        ANT_ASSERT(enemies[aEnemy->indexInWarriorTeamMembersVector]);
        ANT_ASSERT(enemies[enemies.size() - 1]);
        enemies[aEnemy->indexInWarriorTeamMembersVector] = enemies[enemies.size() - 1];
        enemies[aEnemy->indexInWarriorTeamMembersVector]->indexInWarriorTeamMembersVector = aEnemy->indexInWarriorTeamMembersVector;
    }
    DecreaseVector(enemies);
    
    aEnemy->indexInWarriorTeamMembersVector = -1;
    aEnemy->warriorTeam = NULL;    
}

void WarriorTeam::GetCenter(int *x, int *y)
{
    // FILTODO: найти геометрический центр
    (*x) = members[0]->x;
    (*y) = members[0]->y;
}

void WarriorTeam::LogState()
{
    LOG("WARTEAM_%d:", teamID);
    LOG("TEAM MEMBERS:", 0);
    for (vector<Ant *>::iterator it = members.begin(); it != members.end(); it++)
    {
        LOG(" ANT_%d at (%d, %d)", (*it)->antId, 
            (*it)->x, 
            (*it)->y);
        LOG("    path: %c, next path step direction: %c", 
            (*it)->currentPath ? '+' : '-',
            (*it)->currentPath ? '0' + (*it)->currentPath->GetCurrentStepDirection() : '-');
    }
    LOG("ENEMIES:", 0);
    for (vector<Enemy *>::iterator it = enemies.begin(); it != enemies.end(); it++)
    {
        LOG(" ENEMY with owner %d at (%d, %d)", (*it)->owner, (*it)->x, (*it)->y);
    }
}

void WarriorTeam::GetAchievableCellsQueue(std::vector<PositionedObject *> *objects,
                                          std::vector<int> *cellIsAchievable,
                                          int *queueSize,
                                          int *queue)
{
    WAR_LOG("WarriorTeam::GetAchievableCellsQueue start at %f", worldState->timer.getTime());

    vector<bool> cellIsVisited = vector<bool>(State::width * State::height);    
    
    int tailIndex = 0;
    for (vector<PositionedObject *>::iterator it = objects->begin(); it != objects->end(); it++) {
        
        for (int direction = 0; direction < Ant::MOVES_COUNT; direction++) {
            
            int newX = (*it)->x + MOVE_DIRECTIONS[direction][0];
            int newY = (*it)->y + MOVE_DIRECTIONS[direction][1];
            int newIndex = worldState->GetNormalizedMapIndex(newX, newY);
            
            if ((cellIsAchievable->at(newIndex) > 0) && (!cellIsVisited[newIndex])) 
            {
                cellIsVisited[newIndex] = true;
                queue[tailIndex * 2] = newX;
                queue[tailIndex * 2 + 1] = newY;
                tailIndex += 1;
            }
        }
    }
        

    *queueSize = tailIndex;

    WAR_LOG("WarriorTeam::GetAchievableCellsQueue end at %f", worldState->timer.getTime());
}

bool WarriorTeam::CalculateMinEnemiesAffected(int queueSize, 
                                              int *queue, 
                                              vector<int> *attackArea, 
                                              vector<int> *enemyAchievableCells, 
                                              vector<int> *resultEqual,
                                              vector<int> *resultWorse)
{     
    WAR_LOG("WarriorTeam::CalculateMinEnemiesAffected start at %f", worldState->timer.getTime());

    bool wereAnyChanges = false;
    
    
    for (int i = 0; i < queueSize; i++) {
        int x = queue[2 * i];
        int y = queue[2 * i + 1];
        
        int index = worldState->GetNormalizedMapIndex(x, y);
        
        int prevResultEqual = resultEqual->at(index);
        resultEqual->at(index) = 0;
        
        int prevResultWorse = resultWorse->at(index);
        resultWorse->at(index) = 0;

        int attackAreaX = x - worldState->attackAreaSize_2;
        int attackAreaY = y - worldState->attackAreaSize_2;
        
        for (int i = 0; i < worldState->attackAreaSize; i++) {
            for (int j = 0; j < worldState->attackAreaSize; j++) {
                if (worldState->attackMask[i][j]) {
                    int cellIndex = worldState->GetNormalizedMapIndex(attackAreaX + i, attackAreaY + j);
                    if (enemyAchievableCells->at(cellIndex)) {

                        if (attackArea->at(cellIndex) > calculatedEnemyAttackArea[index])
                        {
                            resultWorse->at(index) += 1;
                        }
                        else if (attackArea->at(cellIndex) == calculatedEnemyAttackArea[index])
                        {
                            resultEqual->at(index) += 1;
                        }
                    }                
                }
            }
        }
        
        wereAnyChanges = (wereAnyChanges || (resultEqual->at(index) != prevResultEqual) || (resultWorse->at(index) != prevResultWorse));
        
//        if (MAX_MAP_SIZE == result->at(index)) 
//        {
//            result->at(index) = 0;
//        }
    }
    
    WAR_LOG("WarriorTeam::CalculateMinEnemiesAffected end at %f", worldState->timer.getTime());
    return wereAnyChanges;
}

void WarriorTeam::CalculateMaxEnemiesAffected(int queueSize, 
                                              int *queue, 
                                              std::vector<int> *enemyAchievableCells, 
                                              std::vector<int> *result)
{
    WAR_LOG("WarriorTeam::CalculateMaxEnemiesAffected start at %f", worldState->timer.getTime());
    
    for (int i = 0; i < queueSize; i++) {
        int x = queue[2 * i];
        int y = queue[2 * i + 1];
        
        int index = worldState->GetNormalizedMapIndex(x, y);
        
        int attackAreaX = x - worldState->attackAreaSize_2;
        int attackAreaY = y - worldState->attackAreaSize_2;
        
        result->at(index) = 0;
        for (int i = 0; i < worldState->attackAreaSize; i++) {
            for (int j = 0; j < worldState->attackAreaSize; j++) {
                if (worldState->attackMask[i][j]) {
                    int cellIndex = worldState->GetNormalizedMapIndex(attackAreaX + i, attackAreaY + j);
                    if (enemyAchievableCells->at(cellIndex)) {
                        result->at(index) += 1;
                    }                
                }
            }
        }
    }
    WAR_LOG("WarriorTeam::CalculateMaxEnemiesAffected end at %f", worldState->timer.getTime());
}

void WarriorTeam::CalculateAttackArea(vector<PositionedObject *> *ants, 
                                      vector<bool> *isAlreadyMovedOrNull,
                                      vector<int> *moveDirectionsOrNull,
                                      vector<int> *result,
                                      vector<int> *cellIsAchievableByMembers,
                                      vector< vector<bool> > *cellIsAchievableByMember)
{    
    WAR_LOG("WarriorTeam::CalculateAttackArea start at %f", worldState->timer.getTime());
//    if (NULL != isAlreadyMovedOrNull)
//    {
//        LOG("is already moved:", 0);
//        for (int i = 0; i < members.size(); i++) {
//            LOG("%d ", isAlreadyMovedOrNull->at(i) ? 1 : 0);
//        }
//    }

    cellIsAchievableByMembers->assign(State::width * State::height, 0);
    
    for (int antIndex = 0; antIndex < ants->size(); antIndex++) {
        
        PositionedObject *ant = ants->at(antIndex);
        
        vector<int> tempAttackArea = vector<int>(State::width * State::height);
        
        for (int d = 0; d < Ant::MOVES_COUNT; d++) {
            
            int newX = ant->x + MOVE_DIRECTIONS[d][0];
            int newY = ant->y + MOVE_DIRECTIONS[d][1];
            
            cellIsAchievableByMember->at(antIndex)[worldState->GetNormalizedMapIndex(newX, newY)] = false;
        }
        
        bool isAlreadyMoved = (NULL == isAlreadyMovedOrNull) ? false : isAlreadyMovedOrNull->at(antIndex);
        int startDirection = isAlreadyMoved ? moveDirectionsOrNull->at(antIndex) : 0;
        int endDirection = isAlreadyMoved ? moveDirectionsOrNull->at(antIndex) : (Ant::MOVES_COUNT - 1);
        
        for (int d = startDirection; d <= endDirection; d++) {
            
            int newX = ant->x + MOVE_DIRECTIONS[d][0];
            int newY = ant->y + MOVE_DIRECTIONS[d][1];
            
            int index = worldState->GetNormalizedMapIndex(newX, newY);
            
            if (cellIsSteppable[index] || isAlreadyMoved) {
                
                //                UT_LOG("cell is achievable: %d %d", newX, newY);
                cellIsAchievableByMembers->at(index) += 1;
                cellIsAchievableByMember->at(antIndex)[index] = true;
                
                int attackAreaX = newX - worldState->attackAreaSize_2;
                int attackAreaY = newY - worldState->attackAreaSize_2;
                
                for (int i = 0; i < worldState->attackAreaSize; i++) {
                    for (int j = 0; j < worldState->attackAreaSize; j++) {                        
                        int cellIndex = worldState->GetNormalizedMapIndex(attackAreaX + i, attackAreaY + j);
                        tempAttackArea[cellIndex] = max(tempAttackArea[cellIndex], worldState->attackMask[i][j]);
                    }
                }                    
#ifdef WARTEAM_DEBUG_FULL
                //                UT_LOG("tempEnemyAttackArea:", 0);
#endif// WARTEAM_DEBUG_FULL
            }
        }
        int attackAreaX = ant->x - worldState->attackAreaSize_2 - 1;
        int attackAreaY = ant->y - worldState->attackAreaSize_2 - 1;
        
        for (int i = 0; i < worldState->attackAreaSize + 2; i++) {
            for (int j = 0; j < worldState->attackAreaSize + 2; j++) {                        
                int cellIndex = worldState->GetNormalizedMapIndex(attackAreaX + i, attackAreaY + j);
                result->at(cellIndex) += tempAttackArea[cellIndex];
            }
        }                    
    
//        WAR_LOG("ach by %d:", antIndex);
//        for (int j = 0; j < State::height; j++) {
//            char txBuf[1024 * 1024];
//            txBuf[0] = 0;
//            for (int i = 0; i < State::width; i++) {
//                int index = worldState->GetNormalizedMapIndex(i, j);
//                sprintf(txBuf, "%s %d", txBuf, cellIsAchievableByMember->at(antIndex)[index] ? 1 : 0);
//            }
//            UT_LOG(txBuf, 0);
//        }
//        UT_LOG("-----", 0);
}

    
    WAR_LOG("WarriorTeam::CalculateAttackArea end at %f", worldState->timer.getTime());
}

void WarriorTeam::NormalizeAttackArea(int targetQueueSize, 
                                      int *targetQueue,
                                      int attackersCount,
                                      vector< vector<bool> > *attackerStepPossibility,
                                      vector<int> *attackArea)
{
    WAR_LOG("WarriorTeam::NormalizeAttackArea start at %f", worldState->timer.getTime());
    for (int i = 0; i < targetQueueSize; i++) {
        int x = targetQueue[2 * i];
        int y = targetQueue[2 * i + 1];
        
        int index = worldState->GetNormalizedMapIndex(x, y);
        
        int attackAreaX = x - worldState->attackAreaSize_2;
        int attackAreaY = y - worldState->attackAreaSize_2;
        
        vector<bool> attackerCanAttack = vector<bool>(attackersCount);
        int attackingCellsCount = 0;
        
        for (int i = 0; i < worldState->attackAreaSize; i++) {
            for (int j = 0; j < worldState->attackAreaSize; j++) {
                if (worldState->attackMask[i][j]) {
                                        
                    int cellIndex = worldState->GetNormalizedMapIndex(attackAreaX + i, attackAreaY + j);
                    
                    bool shouldIncrementAttackingCellsCount = false;
                    
                    for (int a = 0; a < attackersCount; a++) {
                        if (attackerStepPossibility->at(a)[cellIndex]) {
//                            WAR_LOG("member[%d] (%d %d) can attack %d %d", a, attackAreaX + i, attackAreaY + j, x, y);
                            attackerCanAttack[a] = true;
                            shouldIncrementAttackingCellsCount = true;
                        }
                    }
                    
                    if (shouldIncrementAttackingCellsCount) {
                        attackingCellsCount += 1;
                    }
                }
            }
        }
        
        int realAttackersCount = 0;
        for (int i = 0; i < attackersCount; i++) {
            realAttackersCount += attackerCanAttack[i] ? 1 : 0;
        }

#ifdef UNIT_TEST_LOG_ENABLED
//        LOG("real attackers count for %d, %d: %d", x, y, realAttackersCount);
#endif
        realAttackersCount = min(realAttackersCount, attackingCellsCount);
        attackArea->at(index) = min(attackArea->at(index), realAttackersCount);
    }
    WAR_LOG("WarriorTeam::NormalizeAttackArea end at %f", worldState->timer.getTime());
}

static WarriorTeam *warriorTeam;

int compare_available_moves(const void *arg0, const void *arg1)
{
    PossibleMove *move0 = (PossibleMove *)arg0;
    PossibleMove *move1 = (PossibleMove *)arg1;
    
//    if (0 == move0->enemiesInWorsePosition
//        && 3 == move0->enemiesInEqualPosition
//        && 3 == move0->attackCoverage
//        && 3 == move1->enemiesInWorsePosition
//        && 1 == move1->enemiesInEqualPosition
//        && 4 == move1->attackCoverage)
//    {
//        LOG("erf", 0);
//    }
    
    int result = 0;
    
    if (move0->addedAttackSupport != move1->addedAttackSupport) 
    {
        result = move0->addedAttackSupport - move1->addedAttackSupport;
    } 
    else 
    {
        if ((0 == move0->attackAffection) && (0 == move1->attackAffection))
        {
        }
        else if ((0 == move0->attackAffection) && (0 != move1->attackAffection))
        {
            if (WarriorTeam::ATTACK_STRATEGY_ALWAYS_FLY == warriorTeam->attackStrategy)
            {
                result = (move1->attackCoverage == move1->enemiesInWorsePosition) ?  -1 : 1;
            }
            else if (WarriorTeam::ATTACK_STRATEGY_FIGHT_EQUAL_DEATH_IS_OK == warriorTeam->attackStrategy)
            {                
                result = (move1->enemiesInWorsePosition + move1->enemiesInEqualPosition == move1->attackCoverage) ? -1 : 1;
            }
            else if (WarriorTeam::ATTACK_STRATEGY_ALWAYS_FIGHT == warriorTeam->attackStrategy)
            {
                result = (move1->enemiesInWorsePosition + move1->enemiesInEqualPosition > 0) ? -1 : 1;
            }
            else 
            {
                ANT_ASSERT(false);
            }
        }
        else if ((0 != move0->attackAffection) && (0 == move1->attackAffection))
        {
            result = compare_available_moves(move1, move0);
        }
        else 
        {            
            if (WarriorTeam::ATTACK_STRATEGY_ALWAYS_FIGHT == warriorTeam->attackStrategy)
            {
                result = move0->enemiesInWorsePosition - move1->enemiesInWorsePosition;
                
                if (0 == result)
                {
                    result = move0->enemiesInEqualPosition - move1->enemiesInEqualPosition;
                }
                
                if (0 == result)
                {
                    result = move0->attackCoverage - move1->attackCoverage;
                }
            }
            else 
            {
                if ((move0->enemiesInWorsePosition == move0->attackCoverage)
                    && (move1->enemiesInWorsePosition == move1->attackCoverage))
                {
                    result = (move0->attackCoverage - move1->attackCoverage);
                }
                else if ((move0->enemiesInWorsePosition == move0->attackCoverage)
                         && (move1->enemiesInWorsePosition < move1->attackCoverage))
                {
                    result = 1;
                }
                else if ((move0->enemiesInWorsePosition < move0->attackCoverage)
                         && (move1->enemiesInWorsePosition == move1->attackCoverage))
                {
                    result = -1;
                }
                else 
                {
                    int enemiesBetterCountDelta = (move0->attackCoverage - move0->enemiesInWorsePosition - move0->enemiesInEqualPosition) 
                    - (move1->attackCoverage - move1->enemiesInWorsePosition - move1->enemiesInEqualPosition);
                    
                    if (0 == enemiesBetterCountDelta)
                    {
                        result = move0->enemiesInWorsePosition - move1->enemiesInWorsePosition;
                        
                        if (0 == result)
                        {
                            result = move1->enemiesInEqualPosition - move0->enemiesInEqualPosition;
                        }
                        
                        if (0 == result)
                        {
                            result = move1->attackCoverage - move1->attackCoverage;
                        }
                    }
                    else 
                    {
                        result = - enemiesBetterCountDelta;
                    }
                }
            }
        }
    }
        
    if (0 == result) {
        
        if (move0->cellIsAchievableByMembersCount != move1->cellIsAchievableByMembersCount)
        {
            result = - (move0->cellIsAchievableByMembersCount - move1->cellIsAchievableByMembersCount);
        }
//        else if (move0->distanceToTarget != move1->distanceToTarget)
//        {
//            result = - (move0->distanceToTarget - move1->distanceToTarget);
//        }
        else if (move0->isAlongPath != move1->isAlongPath) 
        {
            result = move0->isAlongPath ? 1 : -1;
        }
        else if ((Ant::MOVE_NONE == move0->moveDirection) || (Ant::MOVE_NONE == move1->moveDirection)) {
            result = (Ant::MOVE_NONE == move0->moveDirection) ? 1 : -1;
        }
    }

//    WAR_LOG("comparing: (%d, %d (%d, %d dist:%d), %d, %d, %d, %d) is %s (%d, %d (%d, %d dist:%d), %d, %d, %d, %d)", 
//            move0->antIndex, 
//            move0->moveDirection,
//            move0->newX,
//            move0->newY,
//            move0->distanceToTarget,    
//            move0->attackAffection,
//            move0->enemiesInWorsePosition,
//            move0->enemiesInEqualPosition,
//            move0->attackCoverage, 
//            (result > 0) ? "greater than" : ((0 == result) ? "equal to" : "worse than"),
//            move1->antIndex, 
//            move1->moveDirection,
//            move1->newX,
//            move1->newY,
//            move1->distanceToTarget,
//            move1->attackAffection,
//            move1->enemiesInWorsePosition,
//            move1->enemiesInEqualPosition,
//            move1->attackCoverage);

    return (-1 * result);
}

bool WarriorTeam::SummonRequiredAttackHelpers(PossibleMove *move,
                                              vector<bool> *isAlreadyMoved,
                                              vector<bool> *cellIsSteppable,
                                              vector<bool> *cellIsAchievableByMembers)
{
    LOG("Summon attack helpers for %d %d", move->newX, move->newY);
    
    bool success = false;
    
    
    
    
    
    LOG("Summon %s", success ? "succeed" : "failed");
    return success;
}

void WarriorTeam::FindBestMove(vector<bool> *isAlreadyMoved,
                               vector<int> *moveDirections)
{   
    //    bool moveShouldBeImproved = false;
    WAR_LOG("WarriorTeam::FindBestMove start at %f", worldState->timer.getTime());
        
    bool attackMapIsNormalized = false;
    while (!attackMapIsNormalized) {
        
        WAR_LOG("-----iteration----- at %f", worldState->timer.getTime());
        
        attackMapIsNormalized = true;
        
        calculatedMembersAttackArea.assign(State::width * State::height, 0);
        cellIsAchievableByMembers.assign(State::width * State::height, false);
                
//        LOG("is already moved:", 0);
//        for (int i = 0; i < members.size(); i++) {
//            LOG("%d ", isAlreadyMoved->at(i) ? 1 : 0);
//        }

        CalculateAttackArea(&memberObjects, 
                            isAlreadyMoved, 
                            moveDirections, 
                            &calculatedMembersAttackArea, 
                            &cellIsAchievableByMembers,
                            &cellIsAchievableByMember);
        
        GetAchievableCellsQueue(&memberObjects, &cellIsAchievableByMembers, &queueSize, queue);
        
        NormalizeAttackArea(queueSize, queue, enemyObjects.size(), &cellIsAchievableByEnemy, &calculatedEnemyAttackArea);
                
        maxEnemiesAffected.assign(State::width * State::height, 0);
        CalculateMaxEnemiesAffected(queueSize, queue, &cellIsAchievableByEnemies, &maxEnemiesAffected);
        
        bool minEnemiesAffectedMapIsStabilized = false;
        while (!minEnemiesAffectedMapIsStabilized)
        {
            minEnemiesAffectedMapIsStabilized = true;
            
            vector< vector<bool> > *cellIsAchievableByMemberRef = &cellIsAchievableByMember;
            
            cellIsWinningForMember.assign(members.size(), vector<bool>(State::width * State::height));
            if (ATTACK_STRATEGY_ALWAYS_FLY == attackStrategy)
            {                
                for (int q = 0; q < queueSize; q++) 
                {
                    int index = worldState->GetNormalizedMapIndex(queue[2 * q], queue[2 * q + 1]);
                    
                    for (int antIndex = 0; antIndex < members.size(); antIndex++) {
                        if (cellIsAchievableByMember[antIndex][index]) {
//                            WAR_LOG("cellIsWinningForMember[%d, %d][%d] = %d", 
//                                    queue[2 * q], queue[2 * q + 1], 
//                                    index, enemiesInWorsePositionCount[index] >= maxEnemiesAffected[index]);
                            cellIsWinningForMember[antIndex][index] = (enemiesInWorsePositionCount[index] >= maxEnemiesAffected[index]);
                        }
                    }
                }

                cellIsAchievableByMemberRef = &cellIsWinningForMember;
            }
            
            NormalizeAttackArea(enemyAchievableQueueSize, 
                                enemyAchievableQueue, 
                                members.size(),
                                cellIsAchievableByMemberRef, 
                                &calculatedMembersAttackArea);

#ifdef WARTEAM_DEBUG_FULL
//            LOG("cellIsAchievableByMembersRef", 0);
//            for (int j = 0; j < State::height; j++) {
//                char txBuf[1024 * 1024];
//                txBuf[0] = 0;
//                for (int i = 0; i < State::width; i++) {
//                    int index = worldState->GetNormalizedMapIndex(i, j);
//                    sprintf(txBuf, "%s %c/%c:%c", txBuf, 
//                            '0' + calculatedEnemyAttackArea[index],
//                            '0' + minEnemiesAffected[index],
//                            '0' + cellIsAchievableByMembersRef->at(index)                            
//                            );
//                }
//                LOG(txBuf, 0);
//            }
#endif// WARTEAM_DEBUG_FULL
            minEnemiesAffectedMapIsStabilized = !CalculateMinEnemiesAffected(queueSize, 
                                                                             queue, 
                                                                             &calculatedMembersAttackArea,
                                                                             &cellIsAchievableByEnemies,
                                                                             &enemiesInEqualPositionCount,
                                                                             &enemiesInWorsePositionCount);            
//#ifdef WARTEAM_PRINT_MAP
//            for (int j = 0; j < State::height; j++) {
//                char txBuf[1024 * 1024];
//                txBuf[0] = 0;
//                for (int i = 0; i < State::width; i++) {
//                    int index = worldState->GetNormalizedMapIndex(i, j);
//                    if (cellIsAchievableByMembers[index]) {
//                        sprintf(txBuf, "%s %c/%c", txBuf, 
//                                '0' + calculatedEnemyAttackArea[index], 
//                                '0' + minEnemiesAffected[index]);
//                    } else if (cellIsAchievableByEnemies[index]) {
//                        sprintf(txBuf, "%s  %c ", txBuf, '0' + calculatedMembersAttackArea[index]);
//                    } else {
//                        sprintf(txBuf, "%s -/-", txBuf);
//                    }
//                    //            sprintf(txBuf, "%s %5d", txBuf, minEnemiesAffected[index]);
//                }
//                LOG(txBuf, 0);
//            }
//#endif        
        }            
                
        WAR_LOG("WarriorTeam::FindBestMove stage 01 at %f", worldState->timer.getTime());
        if (ATTACK_STRATEGY_ALWAYS_FIGHT != attackStrategy)
        {
//            WAR_LOG("WarriorTeam::FindBestMove stage 01 queue size: %d", queueSize);

            for (int q = 0; q < queueSize; q++) {
                
//                WAR_LOG("WarriorTeam::FindBestMove stage 01_02", 0);

                int index = worldState->GetNormalizedMapIndex(queue[2 * q], queue[2 * q + 1]);
                
                if (!cellIsOccupiedByMember[index] && !movedMemberIsHere[index])
                {
                    if ((calculatedEnemyAttackArea[index] > 0) 
                        && (enemiesInWorsePositionCount[index] + enemiesInEqualPositionCount[index] == 0)) 
                    {                
                        WAR_LOG("0 cell is baaady baad: %d, %d with attack: %d minEnAff: %d", 
                               queue[2 * q], queue[2 * q + 1], 
                               calculatedEnemyAttackArea[index], enemiesInWorsePositionCount[index] + enemiesInEqualPositionCount[index]);
                        
                        cellIsSteppable[index] = false;
                        attackMapIsNormalized = false;
                        
                    } 
                    else if ((calculatedEnemyAttackArea[index] > 0) 
                             && (enemiesInWorsePositionCount[index] < maxEnemiesAffected[index]) 
                             && (ATTACK_STRATEGY_ALWAYS_FLY == attackStrategy))
                    {               
                        WAR_LOG("1 cell is baaady baad: %d, %d with attack: %d %d %d %d", 
                               queue[2 * q], queue[2 * q + 1], 
                               calculatedEnemyAttackArea[index], 
                                enemiesInWorsePositionCount[index],
                                enemiesInEqualPositionCount[index],
                                maxEnemiesAffected[index]);
                        cellIsSteppable[index] = false;
                        attackMapIsNormalized = false;
                        
                    } 
                    else if (!cellIsOccupiedByMember[index] && calculatedEnemyAttackArea[index] > 0) {
                        
                        bool isBetterOrEqual = false;
                        
                        for (int d = 0; d < 4; d++)
                        {
                            int x = queue[2 * q] + MOVE_DIRECTIONS[d][0];
                            int y = queue[2 * q + 1] + MOVE_DIRECTIONS[d][1];
                            
                            int antIndex = worldState->GetNormalizedMapIndex(x, y);
                            
                            if (cellIsOccupiedByMember[antIndex]) {
                                
                                if ((enemiesInWorsePositionCount[antIndex] + enemiesInEqualPositionCount[antIndex] == 0) 
                                    && (enemiesInWorsePositionCount[index] + enemiesInEqualPositionCount[index] >= 0))
                                {
                                    isBetterOrEqual = true;
                                    break;
                                } 
                                if ((enemiesInWorsePositionCount[antIndex] + enemiesInEqualPositionCount[antIndex] > 0) 
                                    && (enemiesInWorsePositionCount[index] + enemiesInEqualPositionCount[index] > 0))
                                {
                                    isBetterOrEqual = true;
                                    break;
                                }
                            }   
                        }
                        
                        if (!isBetterOrEqual)
                        {
                            WAR_LOG("2 cell is baaady baad: %d, %d (%d, %d, %d, %d)", 
                                   queue[2 * q], queue[2 * q + 1], 
                                   calculatedEnemyAttackArea[index], 
                                    enemiesInWorsePositionCount[index],
                                    enemiesInEqualPositionCount[index],
                                    calculatedMembersAttackArea[index]);
                            cellIsSteppable[index] = false;
                            attackMapIsNormalized = false;
                        }
                    }
                }
            }        
        }
        
//        WAR_LOG("WarriorTeam::FindBestMove stage 01 09", 0);
#ifdef WARTEAM_PRINT_MAP
//#ifdef UNIT_TEST_LOG_ENABLED    
        
        char *txBuf = (char *)malloc(1024 * 1024);
        
        for (int j = 0; j < State::height; j++) {
            
            txBuf[0] = 0;
            for (int i = 0; i < State::width; i++) {
                int index = worldState->GetNormalizedMapIndex(i, j);
                if (cellIsAchievableByMembers[index]) {
                    sprintf(txBuf, "%s %c/%c/%c/%c", txBuf, 
                            '0' + calculatedEnemyAttackArea[index], 
                            '0' + enemiesInWorsePositionCount[index], 
                            '0' + enemiesInEqualPositionCount[index], 
                            '0' + maxEnemiesAffected[index]);
                } else if (cellIsAchievableByEnemies[index]) {
                    sprintf(txBuf, "%s    %c   ", txBuf, '0' + calculatedMembersAttackArea[index]);
                } else {
                    sprintf(txBuf, "%s -/-/-/-", txBuf);
                }
                //            sprintf(txBuf, "%s %5d", txBuf, minEnemiesAffected[index]);
            }
            LOG(txBuf, 0);
        }
        
        free(txBuf);
        
//#endif //UNIT_TEST_LOG_ENABLED
        
#endif// WARTEAM_PRINT_MAP
//        WAR_LOG("WarriorTeam::FindBestMove stage 01 10", 0);
    }
    
    ////////////////////////////////////////////////////////////////////////////////
    
    WAR_LOG("WarriorTeam::FindBestMove stage 02 at %f", worldState->timer.getTime());

    attackHelpRequired.assign(State::width * State::height, 0);
    
    for (int q = 0; q < enemyAchievableQueueSize; q++)
    {
        int requiredAttackers = 0;
        int currentAttackers = 0;
        
        int attackAreaX = enemyAchievableQueue[2 * q] - worldState->attackAreaSize_2;
        int attackAreaY = enemyAchievableQueue[2 * q + 1] - worldState->attackAreaSize_2;
        
        for (int i = 0; i < worldState->attackAreaSize; i++) {
            for (int j = 0; j < worldState->attackAreaSize; j++) {
                if (worldState->attackMask[i][j]) {
                    
                    int cellIndex = worldState->GetNormalizedMapIndex(attackAreaX + i, attackAreaY + j);
                    
                    if (movedMemberIsHere[cellIndex]) {
                        requiredAttackers = max(requiredAttackers,
                                                min(calculatedMembersAttackArea[cellIndex], 
                                                    calculatedEnemyAttackArea[cellIndex] + 1
                                                    )
                                                );
                        currentAttackers += 1;
                    }
                }
            }
        }            
        
        if (requiredAttackers > currentAttackers) {
            for (int i = 0; i < worldState->attackAreaSize; i++) {
                for (int j = 0; j < worldState->attackAreaSize; j++) {
                    if (worldState->attackMask[i][j]) {
                        
                        int cellIndex = worldState->GetNormalizedMapIndex(attackAreaX + i, attackAreaY + j);
                        
                        if (cellIsAchievableByMembers[cellIndex])
                        {
                            bool isGoodAttacker = true;
                            
                            if (ATTACK_STRATEGY_ALWAYS_FLY == attackStrategy)
                            {
                                isGoodAttacker = (isGoodAttacker && (enemiesInWorsePositionCount[cellIndex] + enemiesInEqualPositionCount[cellIndex] >= calculatedEnemyAttackArea[cellIndex]));
                            }
                            else 
                            {
                                isGoodAttacker = (isGoodAttacker && (enemiesInWorsePositionCount[cellIndex] + enemiesInEqualPositionCount[cellIndex] > 0));
                            }
                            
                            if (isGoodAttacker)
                            {
                                attackHelpRequired[cellIndex] += 1;                            
                            }
                        }
                    }
                }
            }            
        }
    }
    
#ifdef WARTEAM_PRINT_MAP

//#ifdef UNIT_TEST_LOG_ENABLED    
    
    char *txBuf = (char *)malloc(1024 * 1024);
    
    LOG("calculatedEnemyAttackArea: at %f", worldState->timer.getTime());
    for (int j = 0; j < State::height; j++) {
        
        txBuf[0] = 0;
        for (int i = 0; i < State::width; i++) {
            int index = worldState->GetNormalizedMapIndex(i, j);
            if (cellIsAchievableByMembers[index]) {
                sprintf(txBuf, "%s %c/%c/%c/%c/%c", txBuf, 
                        '0' + calculatedEnemyAttackArea[index], 
                        '0' + enemiesInWorsePositionCount[index],
                        '0' + enemiesInEqualPositionCount[index], 
                        '0' + maxEnemiesAffected[index],
                        '0' + attackHelpRequired[index]);
            } else if (cellIsAchievableByEnemies[index]) {
                sprintf(txBuf, "%s     %c    ", txBuf, '0' + calculatedMembersAttackArea[index]);
            } else {
                sprintf(txBuf, "%s -/-/-/-/-", txBuf);
            }
            //            sprintf(txBuf, "%s %5d", txBuf, minEnemiesAffected[index]);
        }
        LOG(txBuf, 0);
    }
    
    free(txBuf);
    
//#endif //   UNIT_TEST_LOG_ENABLED 
    
#endif// WARTEAM_PRINT_MAP

    if (-1 != lastMove.antIndex)
    {
        bool isBadMove = false;
        
//        if ((lastMove.attackPotential != minEnemiesAffected[lastMove.newCellIndex]) 
//            || (lastMove.attackAffection != calculatedEnemyAttackArea[lastMove.newCellIndex])) 
//        {
//            isBadMove = true
//        }

        if ((lastMove.enemiesInWorsePosition + lastMove.enemiesInEqualPosition > lastMove.attackAffection) 
            && (enemiesInWorsePositionCount[lastMove.newCellIndex] + enemiesInEqualPositionCount[lastMove.newCellIndex] <= calculatedEnemyAttackArea[lastMove.newCellIndex])) 
        {
            isBadMove = true;
        } 
        else if ((lastMove.enemiesInWorsePosition + lastMove.enemiesInEqualPosition == lastMove.attackAffection) 
                 && (enemiesInWorsePositionCount[lastMove.newCellIndex] + enemiesInEqualPositionCount[lastMove.newCellIndex] < calculatedEnemyAttackArea[lastMove.newCellIndex])) 
        {
            isBadMove = true;
        }

        if (isBadMove)
        {
            WAR_LOG("This move is bad", 0);
            return;
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////////
    
    int availableMovesCount = 0;
    PossibleMove availableMoves[2000];
    
    WAR_LOG("WarriorTeam::FindBestMove stage 03", 0);

    for (int antIndex = 0; antIndex < members.size(); antIndex++) 
    {
        //        LOG("Look at ant with index %d", antIndex);
        
        Ant *ant = members.at(antIndex);
        
        if (!isAlreadyMoved->at(antIndex)) 
        {            
            int antAvailableStepsCount = 0;
            
            int alongPathDirection = ant->currentPath ? ant->currentPath->GetCurrentStepDirection() : -1;
            if (-1 != alongPathDirection)
            {
                int pathX = ant->x + MOVE_DIRECTIONS[alongPathDirection][0];
                int pathY = ant->y + MOVE_DIRECTIONS[alongPathDirection][1];
                int pathCellIndex = worldState->GetNormalizedMapIndex(pathX, pathY);
                
                if (cellIsOccupiedByMember[pathCellIndex])
                {
                    alongPathDirection = -1;
                }
            }
            
            for (int d = 0; d < Ant::MOVES_COUNT; d++)
            {
                int newX = ant->x + MOVE_DIRECTIONS[d][0];
                int newY = ant->y + MOVE_DIRECTIONS[d][1];
                int newCellIndex = worldState->GetNormalizedMapIndex(newX, newY);
                
                if (cellIsSteppable[newCellIndex]) {
                    
                    availableMoves[availableMovesCount].antIndex = antIndex;
                    availableMoves[availableMovesCount].moveDirection = d;
                    availableMoves[availableMovesCount].newX = newX;
                    availableMoves[availableMovesCount].newY = newY;
                    availableMoves[availableMovesCount].newCellIndex = newCellIndex;
                    availableMoves[availableMovesCount].addedAttackSupport = attackHelpRequired[newCellIndex];
                    availableMoves[availableMovesCount].attackAffection = calculatedEnemyAttackArea[newCellIndex];
                    availableMoves[availableMovesCount].enemiesInWorsePosition = enemiesInWorsePositionCount[newCellIndex];
                    availableMoves[availableMovesCount].enemiesInEqualPosition = enemiesInEqualPositionCount[newCellIndex];
                    availableMoves[availableMovesCount].attackCoverage = maxEnemiesAffected[newCellIndex];
                    availableMoves[availableMovesCount].isAlongPath = (alongPathDirection == d);
                    availableMoves[availableMovesCount].cellIsAchievableByMembersCount = cellIsAchievableByMembers[newCellIndex];
//                    availableMoves[availableMovesCount].distanceToTarget = targetField.DistanceToPoint(newX, newY);
                    //
                    availableMoves[availableMovesCount].members = &members;
                    
                    availableMovesCount += 1;
                    antAvailableStepsCount += 1;
                }
            }
            
            
            //            LOG("available steps for ant %d:", antIndex);
            //            for (int i = 0; i < availableMovesCount; i++) {
            //                LOG("%d", availableMoves[i * 5]);
            //            }
            
            if (0 == antAvailableStepsCount) 
            {
                WAR_LOG("There are no available moves for this ant", 0);
                return; // dead end, impossible move
            } 
//            else if (1 == antAvailableStepsCount)
//            {
//                WAR_LOG("There is only one available move for this ant", 0);
//
//                int prevDirection = moveDirections->at(availableMoves[availableMovesCount - 1].antIndex);
//                moveDirections->at(availableMoves[availableMovesCount - 1].antIndex) = availableMoves[availableMovesCount - 1].moveDirection;
//
//                isAlreadyMoved->at(availableMoves[availableMovesCount - 1].antIndex) = true;
//                cellIsSteppable[availableMoves[availableMovesCount - 1].newCellIndex] = false;
//                movedMemberIsHere[availableMoves[availableMovesCount - 1].newCellIndex] = true;
//                lastMove = availableMoves[availableMovesCount - 1];
//                
//                FindBestMove(isAlreadyMoved, moveDirections);
//                
//                movedMemberIsHere[availableMoves[availableMovesCount - 1].newCellIndex] = false;
//                cellIsSteppable[availableMoves[availableMovesCount - 1].newCellIndex] = true;
//                isAlreadyMoved->at(availableMoves[availableMovesCount - 1].antIndex) = false;                
//                moveDirections->at(availableMoves[availableMovesCount - 1].antIndex) = prevDirection;        
//
//                return;
//            }
        }
    }
    
    warriorTeam = this;
    qsort(availableMoves, availableMovesCount, sizeof(PossibleMove), compare_available_moves);
    
    WAR_LOG("Sorted available moves:", 0);
    for (int i = 0; i < availableMovesCount; i++) {
        WAR_LOG("%d, %d (%d, %d ach by: %d is along path: %d), %d, %d, %d, %d", 
                availableMoves[i].antIndex, 
                availableMoves[i].moveDirection,
                availableMoves[i].newX,
                availableMoves[i].newY,
                availableMoves[i].cellIsAchievableByMembersCount,
//                availableMoves[i].distanceToTarget,   
                availableMoves[i].isAlongPath,
                availableMoves[i].attackAffection,
                availableMoves[i].enemiesInWorsePosition,
                availableMoves[i].enemiesInEqualPosition,
                availableMoves[i].attackCoverage);
    }
    
    WAR_LOG("WarriorTeam::FindBestMove stage 04", 0);

    for (int i = 0; i < availableMovesCount; i++) {
        
        //        Ant *ant = members.at(availableMoves[i].antIndex);
        //        int index = worldState->GetNormalizedMapIndex(ant->x, ant->y);
        
        WAR_LOG("Best move is: ANT_%d, direction %d new pos: %d, %d", 
            availableMoves[i].antIndex, 
            availableMoves[i].moveDirection,
            availableMoves[i].newX,
            availableMoves[i].newY);
        
        //        int prevCell = worldState->GetNormalizedMapIndex(members[availableMoves[i].antIndex]->x, 
        //                                                         members[availableMoves[i].antIndex]->y);
        //        
        //        if ((minEnemiesAffected[prevCell] < calculatedEnemyAttackArea[prevCell]) && (availableMoves[i])) {
        //            LOG("This steps makes it worse, try to find another move");
        //            
        //        }

        int prevDirection = moveDirections->at(availableMoves[i].antIndex);
        moveDirections->at(availableMoves[i].antIndex) = availableMoves[i].moveDirection;
        isAlreadyMoved->at(availableMoves[i].antIndex) = true;
        cellIsSteppable[availableMoves[i].newCellIndex] = false;
        movedMemberIsHere[availableMoves[i].newCellIndex] = true;
        lastMove = availableMoves[i];
        
        FindBestMove(isAlreadyMoved, moveDirections);
        
        if (didFoundGoodMove) {
            return;
        }

        WAR_LOG("Not so best, returning", 0);
        movedMemberIsHere[availableMoves[i].newCellIndex] = false;
        cellIsSteppable[availableMoves[i].newCellIndex] = true;
        isAlreadyMoved->at(availableMoves[i].antIndex) = false;                
        moveDirections->at(availableMoves[i].antIndex) = prevDirection;        
    }
    
    for (int i = 0; i < members.size(); i++) {
        if (!isAlreadyMoved->at(i)) {
            return;
        }
    }

    LOG("Did Found Good Move!", 0);
    didFoundGoodMove = true;
    
    for (int i = 0; i < members.size(); i++) {
        bestMemberDirections[i] = moveDirections->at(i);
    }

    WAR_LOG("WarriorTeam::FindBestMove end", 0);
}

void WarriorTeam::AddCurrentEnemyPositionToHistory()
{
    // history
    if (enemyLastPositionsHistory.size() >= MAX_HISTORY_SIZE - 1)
    {
        list<PositionedObject *> firstHistoryItem = enemyLastPositionsHistory.front();
        
        for (list<PositionedObject *>::iterator it = firstHistoryItem.begin(); it != firstHistoryItem.end(); it++)
        {
            delete (*it);
        }
        enemyLastPositionsHistory.erase(enemyLastPositionsHistory.begin());
    }    
    
    list<PositionedObject *> historyItem;
    
    for (vector<Enemy *>::iterator it = enemies.begin(); it != enemies.end(); it++)
    {
        historyItem.push_back(new PositionedObject((*it)->x, (*it)->y, (*it)->mapIndex));
    }
    
    historyItem.sort(compare_position_objects);
    enemyLastPositionsHistory.push_back(historyItem);
    
    LOG("WARTEAM_%d ENEMY POSITION HISTORY:", teamID);
    
    char txBuf[100 * 1024];
    for (list< list<PositionedObject *> >::reverse_iterator rit = enemyLastPositionsHistory.rbegin(); rit != enemyLastPositionsHistory.rend(); rit++) {
        
        txBuf[0] = 0;
        
        for (list<PositionedObject *>::iterator it = (*rit).begin(); it != (*rit).end(); it++)
        {
            sprintf(txBuf, "%s %d", txBuf, (*it)->mapIndex);
        }
        
        LOG("%s", txBuf);
    }
}

static double maxFindHighlyPossibleEnemyPositionTime = 0;

list<PositionedObject *> * WarriorTeam::FindHighlyPossibleEnemyPosition()
{
    if (enemyLastPositionsHistory.size() < 5)
    {
        return NULL;
    }
    
    bool didFind = true;    
//    int repeatSteps = 2;
    
    list<PositionedObject *> *currentPositions = &(enemyLastPositionsHistory.back());

#ifdef WARTEAM_DEBUG_FULL
    char txBuf[100 * 1024];
#endif
    int i = 2;
    int comparisonsCount = 0;
    
    for (list< list<PositionedObject *> >::reverse_iterator rit = enemyLastPositionsHistory.rbegin(); rit != enemyLastPositionsHistory.rend(); rit++) {
        
        if (0 == i)
        {
            i = 2;
#ifdef WARTEAM_DEBUG_FULL            
            LOG("compare:", 0);
            
            txBuf[0] = 0;
            for (list<PositionedObject *>::iterator it = currentPositions->begin(); it != currentPositions->end(); it++)
            {
                sprintf(txBuf, "%s %d", txBuf, (*it)->mapIndex);
            }
            LOG("%s", txBuf);
            LOG("with:", 0);
            txBuf[0] = 0;
            for (list<PositionedObject *>::iterator it = (*rit).begin(); it != (*rit).end(); it++)
            {
                sprintf(txBuf, "%s %d", txBuf, (*it)->mapIndex);
            }
            LOG("%s", txBuf);
#endif            
            list<PositionedObject *>::iterator it0 = currentPositions->begin();
            list<PositionedObject *>::iterator it1 = (*rit).begin();
            
            int equalCount = 0;
            int diffCount = 0;
            
            while ((it0 != currentPositions->end()) || (it1 != (*rit).end())) {
                
                if (it0 == currentPositions->end())
                {
                    diffCount += 1;
                    it1++;
                    continue;
                }
                
                if (it1 == (*rit).end())
                {
                    diffCount += 1;
                    it0++;
                    continue;
                }
                
                int delta = (*it0)->mapIndex - (*it1)->mapIndex;
                if (0 == delta)
                {
                    equalCount += 1;
                    it0++;
                    it1++;
                }
                else
                {
                    diffCount += 1;
                    
                    if (delta > 0)
                    {
                        if (it0 != currentPositions->end())
                        {
                            it0++;
                        }
                        else 
                        {
                            it1++;
                        }
                    }
                    else 
                    {
                        if (it1 != (*rit).end())
                        {
                            it1++;
                        }
                        else
                        {
                            it0++;
                        }
                    }
                }
            }
            
            double ratio = (double)equalCount / (equalCount + diffCount);
#ifdef WARTEAM_DEBUG_FULL
            LOG("equal count: %d diff count: %d ratio: %f", equalCount, diffCount, ratio);
#endif
            
            if (ratio < 0.8)
            {                
                didFind = false;
                break;
            }
            
            comparisonsCount++;
            if (comparisonsCount > 2)
            {
                break;
            }
        }
        
        i -= 1;
    }        
    
    if (didFind)
    {
#ifdef WARTEAM_DEBUG_FULL
        LOG("охуэнске! пробуем угадать", 0);        
#endif
        list< list<PositionedObject *> >::reverse_iterator it = enemyLastPositionsHistory.rbegin();
        it++;
        list<PositionedObject *> *prevStepPosObjects = &(*it);
        return prevStepPosObjects;
    }
    
    return NULL;
}

void WarriorTeam::CalculateNearbyAnts()
{
    friendsNearby = 0;
    enemiesNearby = 0;
    
    vector<ControlPoint *> controlPointsToCheck;
    controlPointsToCheck.reserve(ControlPoint::GetTotalControlPointsCount());
    
    vector<bool> cpIsVisited = vector<bool>(ControlPoint::GetTotalControlPointsCount());    
    
    for (vector<Ant *>::iterator it = members.begin(); it != members.end(); it++)        
    {
        ControlPoint *cp0 = worldState->controlAreasMap[(*it)->mapIndex];
        controlPointsToCheck.push_back(cp0);
        
        for (vector<ControlPoint *>::iterator it1 = cp0->connectedPoints.begin(); it1 != cp0->connectedPoints.end(); it1++)
        {
            controlPointsToCheck.push_back(*it1);
        }
    }
    
    for (vector<ControlPoint*>::iterator it = controlPointsToCheck.begin(); it != controlPointsToCheck.end(); it++)
    {
        ControlPoint *cp = (*it);
        ANT_ASSERT(cp->curId < cpIsVisited.size());
        
        if (!cpIsVisited[cp->curId]) 
        {
            enemiesNearby += cp->enemies.size();
            friendsNearby += cp->placedAnts.size();
            
            cpIsVisited[cp->curId] = true;
        }
    }
}

void WarriorTeam::ProcessTurn()
{
    LOG("WARTEAM_%d PROCCESS TURN at %f", teamID, worldState->timer.getTime());

    Timer attackTimer;
    attackTimer.start();
    
//    LogState();
    
#ifndef UNIT_TEST_LOG_ENABLED
    
    CalculateNearbyAnts();
    
    LogState();

    int membersCount = members.size();
    
    int maxLeavableCount = members.size() - enemies.size();
    int alreadyLeaved = 0;
    
    for (int i = membersCount - 1; i >= 0; i--)
    {
        if ((0 == members[i]->attackableEnemies.size()) && (members[i]->noEnemiesForTurnsCount > 2))
        {
            LOG("WARTEAM_%d REMOVE UNUSED MEMBER: ANT_%d %d, %d", teamID, members[i]->antId, members[i]->x, members[i]->y);
            RemoveAnt(members[i]);
            
            alreadyLeaved += 1;
            if (alreadyLeaved >= maxLeavableCount)
            {
                break;
            }
        }
    }
/*    
    if (members.size() < enemies.size() + 1)
    {//todo: потюньте меня
        int size = 0;
        float fromI = 0.f;
        float toI = 0.f;
        ControlPoint *cpt = NULL;
        if (enemies.size() > 5)
        {
            size = 3;
            fromI = 0.2;
            toI = 0.4;
        }
        else if(enemies.size() > 3)
        {
            size = 2;
            fromI = 0.3;
            toI = 0.5;
        }
        if (size > 0 && members.size() > 0)
        {
            LOG("Poi for team %d needed", teamID);
            int ptX = enemies[0]->x;
            int ptY = enemies[0]->y;
            ptX /= 3;
            ptX *= 3;
            ptY /= 3;
            ptY *= 3;
            LOG("Poi Req pos %d, %d", ptX, ptY);
            VECTOR_BOUNDS(worldState->controlAreasMap, worldState->GetNormalizedMapIndex(ptX, ptY));
            cpt = worldState->controlAreasMap[worldState->GetNormalizedMapIndex(ptX, ptY)];

            if (!poi) 
            {
                LOG("Poi created in point %d with size %d and from interest %f", cpt->curId, size, fromI);
                poi = new Poi(cpt, size, fromI, toI);
            }
            else 
            {
                if (poi->center != cpt) 
                {
                    LOG("Poi repalced to point %d", cpt->curId);
                    poi->ReplaceField(cpt);
                }
                else if(poi->poiSteps != size || poi->valueFrom != fromI || poi->valueTo != toI)
                {
                    LOG("Poi vaues changed to size %d and from interest %f", cpt->curId, size, fromI);
                    poi->ChangeField(size, fromI, toI);
                }

            }

        }
    }
*/
    
    if (0 == members.size())
    {
        LOG("WARTEAM_%d: NO MORE MEMBERS LEFT", teamID);
        return;
    }
    
    LogState();


    attackStrategy = worldState->bot->GetAttackStrategyForWarriorTeam(this);
    
#endif// UNIT_TEST_LOG_ENABLED
    
    AddCurrentEnemyPositionToHistory();
    list<PositionedObject *> *highlyPossibleEnemyPosition = NULL;
    highlyPossibleEnemyPosition = FindHighlyPossibleEnemyPosition();

    LOG("friends nearby: %d enemies nearby: %d", friendsNearby, enemiesNearby);
    LOG("attack strategy: %d", attackStrategy);    

    cellIsSteppable = vector<bool>(State::width * State::height);
    for (int i = 0; i < State::width * State::height; i++) // FILTODO: optimize this
    { 
        if ((State::WATER != worldState->terrainMap[i]) 
            && (State::FOOD != worldState->terrainMap[i])
            && (!((NULL != worldState->myAntsMap[i]) && (this != worldState->myAntsMap[i]->warriorTeam)))) // FILTODO: HIVE?
        { 
            cellIsSteppable[i] = true;
        }
    }    
    
    if (NULL != highlyPossibleEnemyPosition)
    {
        enemyObjects.assign(highlyPossibleEnemyPosition->begin(), highlyPossibleEnemyPosition->end());
        isEnemyAlreadyMoved.assign(enemyObjects.size(), true);
        enemyDirections.assign(enemyObjects.size(), Ant::MOVE_NONE);
    }
    else 
    {
        enemyObjects.assign(enemies.begin(), enemies.end());    
    }
    
    calculatedEnemyAttackArea = vector<int>(State::width * State::height);
    cellIsAchievableByEnemies.assign(State::width * State::height, false);
    cellIsAchievableByEnemy.assign(enemyObjects.size(), vector<bool>(State::width * State::height));
    
    cellIsAchievableByMember.assign(members.size(), vector<bool>(State::width * State::height));

    CalculateAttackArea(&enemyObjects, 
                        (NULL == highlyPossibleEnemyPosition) ? NULL : &isEnemyAlreadyMoved, 
                        (NULL == highlyPossibleEnemyPosition) ? NULL : &enemyDirections, 
                        &calculatedEnemyAttackArea, 
                        &cellIsAchievableByEnemies,
                        &cellIsAchievableByEnemy);
    
    GetAchievableCellsQueue(&enemyObjects, &cellIsAchievableByEnemies, &enemyAchievableQueueSize, enemyAchievableQueue);
    
    memberObjects.assign(members.begin(), members.end());
    
    cellIsOccupiedByMember = vector<bool>(State::width * State::height);
    for (vector<Ant *>::iterator it = members.begin(); it != members.end(); it++) {
        cellIsOccupiedByMember[worldState->GetNormalizedMapIndex((*it)->x, (*it)->y)] = true;
    }
    
    vector<int> moveDirections;
    moveDirections.assign(members.size(), Ant::MOVE_NONE);
    for (int i = 0; i < members.size(); i++) {
        bestMemberDirections[i] = moveDirections[i];
    }
    
    vector<bool> isAlreadyMoved = vector<bool>(State::width * State::height);
    movedMemberIsHere = vector<bool>(State::width * State::height);

    enemiesInEqualPositionCount.assign(State::width * State::height, MAX_MAP_SIZE);
    enemiesInWorsePositionCount.assign(State::width * State::height, MAX_MAP_SIZE);

    didFoundGoodMove = false;
    lastMove.antIndex = -1;
    FindBestMove(&isAlreadyMoved, &moveDirections);
    
    for (int i = 0; i < members.size(); i++) {
        
        int index = worldState->GetNormalizedMapIndex(members[i]->x, members[i]->y);
        
        if (State::ANT == worldState->terrainMap[index])
        {
            worldState->terrainMap[index] = State::GROUND;
        }
        
        ANT_ASSERT(members[i] == worldState->myAntsMap[index]);
//        WAR_LOG("Remove ANT_%d from ants map at: %d, %d", members[i]->antId, members[i]->x, members[i]->y);
        worldState->myAntsMap[index] = NULL;
    }
    
    for (int i = 0; i < members.size(); i++) {

        if (bestMemberDirections[i] >=0 && bestMemberDirections[i] < 4) {
            LOG("ANT_%d should move %c", members[i]->antId, CDIRECTIONS[bestMemberDirections[i]]);
            
            int newX = members[i]->x + MOVE_DIRECTIONS[bestMemberDirections[i]][0];
            int newY = members[i]->y + MOVE_DIRECTIONS[bestMemberDirections[i]][1];
            int newIndex = worldState->GetNormalizedMapIndex(newX, newY);
            
            ANT_ASSERT(State::GROUND == worldState->terrainMap[newIndex]
                       || State::HIVE <= worldState->terrainMap[newIndex]);
            
#ifndef UNIT_TEST_LOG_ENABLED
            
            if (members[i]->currentPath)
            {
                if (bestMemberDirections[i] == members[i]->currentPath->GetCurrentStepDirection())
                {
                    members[i]->FollowPath();
                }
                else 
                {
                    members[i]->CancelCurrentTarget();
                }
            }
            
            if (!members[i]->isMoved)
            {
                members[i]->MoveAntToDirection(bestMemberDirections[i], true);
            }
            
#endif
        
        } else {
            LOG("ANT_%d should better stay where it is, at %d, %d", members[i]->antId, members[i]->x, members[i]->y);
            members[i]->isMoved = true;
        }
    }
    
    for (int i = 0; i < members.size(); i++) {
        
        int index = worldState->GetNormalizedMapIndex(members[i]->x, members[i]->y);
        
        if (State::GROUND == worldState->terrainMap[index])
        {
            worldState->terrainMap[index] = State::ANT;
        }
        
//        WAR_LOG("Insert ANT_%d to ants map at: %d, %d", members[i]->antId, members[i]->x, members[i]->y);
        if (NULL != worldState->myAntsMap[index])
        {
//            LOG("ants map cell is not empty at %d %d: ANT_%d is here", members[i]->x, members[i]->y, worldState->myAntsMap[index]->antId);
        }
        ANT_ASSERT(NULL == worldState->myAntsMap[index]);
        worldState->myAntsMap[index] = members[i];
    }
    
    if (NULL != highlyPossibleEnemyPosition)
    {
        for (list< list<PositionedObject *> >::iterator it = enemyLastPositionsHistory.begin(); it != enemyLastPositionsHistory.end(); it++) 
        {
            for (list<PositionedObject *>::iterator it1 = (*it).begin(); it1 != (*it).end(); it1++)
            {
                delete (*it1);
            }
        }
        enemyLastPositionsHistory.clear();
    }
        
    double t = attackTimer.getTime();
    maxTurnTime = max(maxTurnTime, t);
    LOG("WARTEAM_%d END PROCCESS TURN in %f ms (max is %f ms)", teamID, t, maxTurnTime);
}

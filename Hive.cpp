/*
 *  Hive.cpp
 *  HottychBot
 *
 *  Created by Alexey Prosin on 10/30/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#include "Hive.h"
#include "State.h"
#include "Ant.h"
#include "Utils.h"
#include "Poi.h"

using namespace std;

Hive::Hive(int _x, int _y, int _index, int _owner)
: PositionedObject(_x, _y, _index)
{
    owner = _owner;
    state = HIVE_ACTIVE;
    protectorsRequired = 0;
    connectedPoint = NULL;
    poi = NULL;
}

Hive::~Hive()
{
    if (poi) 
    {
        delete poi;
    }
}


void Hive::RegisterAnt(Ant *ant)
{
    protectors.push_back(ant);
}

void Hive::UnregisterAnt(Ant *ant)
{
    FastRemovePointerFromVector(ant, protectors);
}

void Hive::RegisterTerminators(Ant *ant)
{
    terminators.push_back(ant);
}

void Hive::UnregisterTerminators(Ant *ant)
{
    FastRemovePointerFromVector(ant, terminators);
}


Ant *Hive::GetProtector(int num)
{
    VECTOR_BOUNDS(protectors, num);
    ANT_ASSERT(protectors[num]);
    return protectors[num];
}


#include "Bot.h"
#include "Ant.h"
#include "Enemy.h"
#include "WarriorTeam.h"
#include "ControlPoint.h"
#include "Utils.h"
#include "list"
#include "Poi.h"

#ifdef STREAM_INPUT

//std::ifstream fin("/Users/fil/Projects/ash_bot/tools/mainstream.txt");
std::ifstream fin("/Users/Hottych/Documents/Ants/ash_bot/mainstream.txt");
#define cin fin

#endif


using namespace std;

static int scoutCount = 0;
static const int reqScoutCount = 5;
static int minProtectors = 1;



#define PROTECTORS_ALARM_DIST           2

#define HIVEGUARDS_NEED2HELP_RADIUS     3
#define HIVEGUARDS_NEED2HELP_FROM       0.2f
#define HIVEGUARDS_NEED2HELP_TO         0.4f

#define HIVEGUARDS_NEED2LEAVE_RADIUS    2
#define HIVEGUARDS_NEED2LEAVE_FROM      10.0f
#define HIVEGUARDS_NEED2LEAVE_TO        1.3f

//constructor
Bot::Bot()
{

};

void Bot::Setup()
{
    foodPriority = FOOD_IS_QUESTION_OF_LIFE;
    pathFinder = new WavePathFinder(&state);
    antsToRecheck = &tergetCheck1;
    recheckingAnts = &tergetCheck2;
    
    hivePoiRecheckCounter = 4;
    hiveToRecheck = 0;

}

void Bot::PlayGame()
{
    //reads the game parameters and sets up
    state.bot = this;
    
    cin >> state;
    state.Setup();
    state.bug << "State inited" << endl;
    Setup();
    state.bug << "time taken: " << state.timer.getTime() << "ms" << endl;
    state.bug << "Bot inited" << endl;
    EndTurn();

    //continues making moves while the game is not over
    while(cin >> state)
    {
//        LOG("Ant 1 is moved = %d in line: %d", state.myAnts[0]->isMoved, __LINE__);
        MakeMoves();
        EndTurn();
    }
};

static double maxWarriorTeamsCreatedTime = 0;

//makes the bots moves for the turn
void Bot::MakeMoves()
{
    state.bug << "TURN " << state.currentTurn << ":" << endl;
    if(state.currentTurn == 308)
    {
        LOG("Break!", 0);
    }
    
    state.Refresh();
//    LOG("Ant 1 is moved = %d in line: %d", state.myAnts[0]->isMoved, __LINE__);

    if(state.currentTurn == 1)
    {
        LOG("Build control areas for hives", 0);
        for (int i = 0; i < state.myHives.size(); i++) 
        {
            state.myHives[i]->connectedPoint->BuildControlArea();
        }
        ControlPoint::HivePulse(&state);
        for (int i = 0; i < state.myHives.size(); i++) 
        {
            state.myHives[i]->poi = new Poi(state.myHives[i]->connectedPoint, HIVEGUARDS_NEED2LEAVE_RADIUS, HIVEGUARDS_NEED2LEAVE_FROM, HIVEGUARDS_NEED2LEAVE_TO);
        }
//        LOG("Ant 1 is moved = %d in line: %d", state.myAnts[0]->isMoved, __LINE__);
        int asz = state.myAnts.size();
        for (int i = 0; i < asz; i++) 
        {
            state.myAnts[i]->currentPlace->UnregisterPlacedAnt(state.myAnts[i]);
            state.myAnts[i]->currentPlace = state.controlAreasMap[state.myAnts[i]->mapIndex];
            state.myAnts[i]->currentPlace->RegisterPlacedAnt(state.myAnts[i]);
        }
        ControlPoint::LogAreas(&state);
    }
//    LOG("Ant 1 is moved = %d in line: %d", state.myAnts[0]->isMoved, __LINE__);
    
    state.bug << "Hives: " << state.myHives.size() << endl;
    state.bug << "Visible food: " << state.food.size() << endl;
    state.bug << "Ants: " << state.myAnts.size() << endl;
    
    ProcessFoodPriority();
    
    if (state.myAnts.size() > 15)
    {
        minProtectors = 1;
    }
    if (state.myAnts.size() > 25)
    {
        minProtectors = 2;
    }
    if (state.myAnts.size() > 50)
    {
        minProtectors = 3;
    }
    
    int asz;
    if (state.myAnts.size() > 100 && reqScoutCount < scoutCount) 
    {//добавляем в дело скаутов
        asz = state.myAnts.size();
        Ant *candidate = NULL;
        int stepsD = 0;
        for (int i = 0; i < asz; i++)
        {   
            if (!state.myAnts[i]->isScout) 
            {
                int steps = state.myAnts[i]->currentPlace->stepsToHive;
                if (steps > stepsD) 
                {
                    stepsD = steps;
                    candidate = state.myAnts[i];
                }
            }
        }
        if (candidate)
        {
            candidate->isScout = true;
            scoutCount++;
        }
    }

    asz = state.myAnts.size();
    for (int i = 0; i < asz; i++)
    {        
        ANT_ASSERT(state.myAnts[i]);
        
        state.myAnts[i]->CheckForAttackableEnemies();
    }
    
    int wtsz = state.warriorTeams.size();
    for (int i = wtsz - 1; i >= 0; i--)
    {
        ANT_ASSERT(state.warriorTeams[i]);

        state.warriorTeams[i]->ProcessTurn();
        
        if (0 == state.warriorTeams[i]->members.size() || 0 == state.warriorTeams[i]->enemies.size())
        {
            state.RemoveWarriorTeam(state.warriorTeams[i]);
        }
    }

    UpdateProtectorsCount();
    
    for (int i = 0; i < state.myHives.size(); i++) 
    {
        ANT_ASSERT(state.myHives[i]);
        if (state.myHives[i]->GetProtectorsCount() > state.myHives[i]->protectorsRequired)
        {
            UseFreeProtectors(state.myHives[i]);
        }
    }
//    LOG("Ant 1 is moved = %d in line: %d", state.myAnts[0]->isMoved, __LINE__);
    
//    asz = antsOfType[Ant::ANT_FREE].size();
//    while (asz > 0) 
//    {//распределяем свободных мурашей
//        ANT_ASSERT(antsOfType[Ant::ANT_FREE][asz - 1]);
//        FindNewProffession(antsOfType[Ant::ANT_FREE][asz - 1]);
//        asz = antsOfType[Ant::ANT_FREE].size();
//    }
    
    LOG("Processing targeting", 0);
    recheckingAnts->clear();
    antsToRecheck->clear();
    asz = state.myAnts.size();
    for (int i = 0; i < asz; i++) 
    {
        ANT_ASSERT(state.myAnts[i]);
//        if (NULL == state.myAnts[i]->warriorTeam)
        {
            state.myAnts[i]->ProcessTargeting();
        }
    }

    while (!antsToRecheck->empty()) 
    {
        LOG("Rechecking %d ants", antsToRecheck->size());
        if (antsToRecheck == &tergetCheck1)
        {
            antsToRecheck = &tergetCheck2;
            recheckingAnts = &tergetCheck1;
        }
        else 
        {
            antsToRecheck = &tergetCheck1;
            recheckingAnts = &tergetCheck2;
        }
        asz = recheckingAnts->size();
        for (int i = 0; i < asz; i++) 
        {
            ANT_ASSERT((*recheckingAnts)[i]);
            VECTOR_BOUNDS((*recheckingAnts), i);
            (*recheckingAnts)[i]->ProcessTargeting();
        }
        recheckingAnts->clear();
    }
    
    
    
    LOG("Processing movement", 0);
    asz = state.myAnts.size();
    for (int i = 0; i < asz; i++) 
    {
        ANT_ASSERT(state.myAnts[i]);
        if (NULL == state.myAnts[i]->warriorTeam)
        {
            state.myAnts[i]->ProcessMovement();
        }
    }
    
    while (!ControlPoint::UpdateAreas())//ТОДО: проверять время уже затраченное на ход и относительно этого определять упдейтить ли зоны дальше
    {
    }
    hivePoiRecheckCounter--;
    if (hivePoiRecheckCounter <= 0 )
    {
        hivePoiRecheckCounter = 3;
        if (state.myHives[hiveToRecheck]->poi)
        {
            state.myHives[hiveToRecheck]->poi->RecheckField();
        }
        hiveToRecheck++;
        if (hiveToRecheck >= state.myHives.size()) 
        {
            hiveToRecheck = 0;
        }
    }
//    if (State::currentTurn % 10 == 0)
//    {
//        ControlPoint::LogAreas(&state);
//    }
//    if (State::currentTurn % 3 == 0)
//    {
//        ControlPoint::LogAreaTypes(&state);
//    }
//    ControlPoint::LogInfluence(&state);
    ControlPoint::LogInterest(&state);
    state.bug << "time taken: " << state.timer.getTime() << "ms" << endl << endl << endl << endl;
    
    // AK: draw links for all warteams
#ifdef DEBUG    
    
//    for (int x = 0; x < state.width; ++x)
//    {
//        for (int y = 0; y < state.height; ++y)
//        {
//            int i = state.GetNormalizedMapIndex(x, y);
//            char str [100];
//            sprintf(str, "stepsToHive%d", state.controlAreasMap[i]->stepsToHive);
//            cout << "i " << y << " " << x << " " << str << endl;
//        }
//    }
    
    wtsz = state.warriorTeams.size();
    for (int i = 0; i < wtsz; ++i)
    {
        ANT_ASSERT(state.warriorTeams[i]);
        
        int msz = state.warriorTeams[i]->members.size();
        for (int j = 1; j < msz; ++j)
        {
            Ant *a0 = state.warriorTeams[i]->members[0];
            Ant *a1 = state.warriorTeams[i]->members[j];
            
            cout << "v setLayer 0" << endl;
            cout << "v setLineWidth 1" << endl;
            cout << "v setLineColor 255 255 255 0.5" << endl;
            cout << "v line " << a0->y << " " << a0->x << " " << a1->y << " " << a1->x << endl;
        }

        int esz = state.warriorTeams[i]->enemies.size();
        for (int j = 0; j < esz; ++j)
        {
            Ant *a0 = state.warriorTeams[i]->members[0];
            Enemy *a1 = state.warriorTeams[i]->enemies[j];
            
            cout << "v setLayer 0" << endl;
            cout << "v setLineWidth 1" << endl;
            cout << "v setLineColor 255 0 0 0.5" << endl;
            cout << "v line " << a0->y << " " << a0->x << " " << a1->y << " " << a1->x << endl;
        }
    }
    
    asz = state.myAnts.size();
    for (int i = 0; i < asz; ++i)
    {
        Ant *a = state.myAnts[i];
        
        cout << "v setLayer 1" << endl;
        cout << "v setLineWidth 1" << endl;

        switch (a->type)
        {
            case Ant::ANT_FOOD_HUNTER:
                cout << "v setLineColor 0 255 0 1" << endl;
                cout << "v setFillColor 0 255 0 1" << endl;
                break;
                
            case Ant::ANT_PROTECTOR:
                cout << "v setLineColor 0 255 255 1" << endl;
                cout << "v setFillColor 0 255 255 1" << endl;
                break;
                
            case Ant::ANT_TERMINATOR:
                cout << "v setLineColor 255 255 0 1" << endl;
                cout << "v setFillColor 255 255 0 1" << endl;
                break;
                
            default:
                cout << "v setLineColor 0 255 0 0" << endl;
                cout << "v setFillColor 0 255 0 0" << endl;
                break;
        }
        
        cout << "v rect " << (a->y - 0.5) << " " << (a->x - 0.5) << " 1 1 false" << endl;

//        char str [256];
//
//        int ai = state.GetNormalizedMapIndex(a->x, a->y);
//        int stepsToHive = state.controlAreasMap[ai]->stepsToHive;
//
//        sprintf(str, "type=%d|warriorTeam=%d|strategy=%d|stepsToHive=%d",
//                a->type, a->warriorTeam ? a->warriorTeam->teamID : 0, a->warriorTeam ? a->warriorTeam->attackStrategy : 0, stepsToHive);
//        cout << "i " << a->y << " " << a->x << " " << str << endl;
    }
    
    cout << "v setLayer 0" << endl;
    cout << "v setFillColor 255 255 255 1" << endl;
    
    int hsz = state.myHives.size();
    for (int i = 0; i < hsz; ++i)
    {
        Hive *h = state.myHives[i];
        
        if (Hive::HIVE_DEAD == h->state)    continue;

        cout << "v star " << (h->y) << " " << (h->x) << " 0.8 1.5 5 true" << endl;
        
//        char str [100];
//        sprintf(str, "protectorsRequired=%d|protectorsCount=%d", h->protectorsRequired, h->GetProtectorsCount());
//        cout << "i " << h->y << " " << h->x << " " << str << endl;
    }

#endif
};

//finishes the turn
void Bot::EndTurn()
{
    cout << "go" << endl;

    if(state.currentTurn > 0)
        state.Reset();
    state.currentTurn++;
    LOG(" ", 0);
    LOG(" ", 0);
    LOG(" ", 0);
    LOG(" ", 0);
    LOG(" ", 0);
};

void Bot::UpdateProtectorsCount()
{
    int hsz = state.myHives.size();
    int esz = state.enemies.size();

    for (int h = 0; h < hsz; h++)
    {
        ANT_ASSERT(state.myHives[h]);
        
        if (Hive::HIVE_DEAD == state.myHives[h]->state)
        {
            state.myHives[h]->protectorsRequired = 0;
            continue;
        }

        // если когда-то уже был алярм, то всегда будет создаваться минумум один протектор
        // если вокруг есть враги, то будет создано E+1 протекторов
        int prc = state.myHives[h]->protectorsRequired ? minProtectors : 0;

        for (int e = 0; e < esz; e++) 
        {
            ANT_ASSERT(state.enemies[e]);
            
            int dst = state.enemies[e]->placedPoint->wayToPoints[state.myHives[h]->connectedPoint->curId].pointsLeft;
            
            if (dst < PROTECTORS_ALARM_DIST)
            {
                prc++;
            }
        }

        if (state.myHives[h]->poi) 
        {
            if (HIVEGUARDS_NEED2LEAVE_RADIUS == state.myHives[h]->poi->poiSteps && state.myHives[h]->protectorsRequired > minProtectors)
            {
                state.myHives[h]->poi->ChangeField(HIVEGUARDS_NEED2HELP_RADIUS, HIVEGUARDS_NEED2HELP_FROM, HIVEGUARDS_NEED2HELP_TO);
            }
            else if (HIVEGUARDS_NEED2HELP_RADIUS == state.myHives[h]->poi->poiSteps && state.myHives[h]->protectorsRequired <= minProtectors)
            {
                state.myHives[h]->poi->ChangeField(HIVEGUARDS_NEED2LEAVE_RADIUS, HIVEGUARDS_NEED2LEAVE_FROM, HIVEGUARDS_NEED2LEAVE_TO);
            }
        }
        
        if (prc >= state.myHives[h]->protectorsRequired)
        {
            state.myHives[h]->protectorsRequired = prc;
            state.myHives[h]->protCounter = 0;
        }
        else 
        {
            if (state.myHives[h]->protectorsRequired > prc)
            {
                state.myHives[h]->protCounter++;
                if (state.myHives[h]->protCounter >= minProtectors) 
                {
                    state.myHives[h]->protectorsRequired--;
                }
            }
        }

        
//        if (oldProtectors > 1 && state.myHives[h]->protectorsRequired == 0)
//        {
//            state.myHives[h]->poi->ChangeField(HIVEGUARDS_NEED2LEAVE_RADIUS, HIVEGUARDS_NEED2LEAVE_FROM, HIVEGUARDS_NEED2LEAVE_TO);
//        }
//        else if(oldProtectors == 0 && state.myHives[h]->protectorsRequired > 0)
//        {
//            state.myHives[h]->poi->ChangeField(HIVEGUARDS_NEED2HELP_RADIUS, HIVEGUARDS_NEED2HELP_FROM, HIVEGUARDS_NEED2HELP_TO);
//        }

//        if (state.myHives[h]->protectorsRequired)
//        {
//            LOG("hive %d protectorsRequired %d", h, state.myHives[h]->protectorsRequired);
//        }
    }
}

void Bot::UseFreeProtectors(Hive *hive)
{
//    state.bug << "set proffesions to the free protectors" << endl;
    LOG("Current number of hive %d, %d  protectors: %d", hive->x, hive->y, hive->GetProtectorsCount());
    while (hive->GetProtectorsCount() > hive->protectorsRequired)
    {//todo: calculate the most wanted ant
//        state.bug << "Get new freen protector" << endl;
        Ant *a = hive->GetProtector(hive->GetProtectorsCount() - 1);
        LOG("Removeint from protectors ant %d", a->antId);
        hive->UnregisterAnt(a);
        FindNewProffession(a);
    }
}

void Bot::FindNewProffession(Ant *ant)
{
    ant->OnTypeChanged(Ant::ANT_FOOD_HUNTER);
}



void Bot::OnAntTypeChanged(Ant *ant, int newType)
{
    FastRemovePointerFromVector(ant, antsOfType[ant->type]);
    antsOfType[newType].push_back(ant);
}

void Bot::OnAntIsDead(Ant *ant)
{
    if (ant->isScout)
    {
        scoutCount--;
    }
    FastRemovePointerFromVector(ant, antsOfType[ant->type]);
}

void Bot::OnAntIsNeedNewTarget(Ant *ant)
{
    antsToRecheck->push_back(ant);
}

void Bot::ProcessFoodPriority()
{
    foodPriority = FOOD_IS_QUESTION_OF_LIFE;
}

Hive *Bot::GetNearestHive(ControlPoint *forPoint)
{
    int sz = state.myHives.size();
    Hive *retHive = NULL;
    bool isAlive = false;
    int dst = ControlPoint::maxPointsDist;
    for (int i = 0; i < sz; i++)
    {//стараемся найти ближайший живой
        int pl = forPoint->wayToPoints[state.myHives[i]->connectedPoint->curId].pointsLeft;
        if (pl < dst)
        {
            retHive = state.myHives[i];
            dst = pl;
        }
    }
//    for (int i = 0; i < sz; i++)
//    {//стараемся найти ближайший живой
//        if (state.myHives[i]->state != Hive::HIVE_DEAD && !isAlive)
//        {
//            isAlive = true;
//            dst = ControlPoint::maxPointsDist;
//        }
//        if (isAlive) 
//        {
//            if (state.myHives[i]->state != Hive::HIVE_DEAD)
//            {
//                int pl = forPoint->wayToPoints[state.myHives[i]->connectedPoint->curId].pointsLeft;
//                if (pl < dst)
//                {
//                    retHive = state.myHives[i];
//                    dst = pl;
//                }
//            }
//        }
//        else 
//        {
//            int pl = forPoint->wayToPoints[state.myHives[i]->connectedPoint->curId].pointsLeft;
//            if (pl < dst)
//            {
//                retHive = state.myHives[i];
//                dst = pl;
//            }
//        }
//
//    }
    
    return retHive;
}

int Bot::GetAttackStrategyForWarriorTeam(WarriorTeam *aWarriorTeam)
{
    int result = WarriorTeam::ATTACK_STRATEGY_ALWAYS_FLY;

    int minHiveDist = 99999;
    int enemyHiveDist = 99999;
    int sz = aWarriorTeam->members.size();
    for (int i = 0; i < sz; i++) 
    {
        if (aWarriorTeam->members[i]->currentPlace->stepsToHive < minHiveDist) 
        {
            minHiveDist = aWarriorTeam->members[i]->currentPlace->stepsToHive;
        }
    }
    sz = aWarriorTeam->enemies.size();
    for (int i = 0; i < sz; i++) 
    {
        if (aWarriorTeam->enemies[i]->placedPoint->stepsToHive < enemyHiveDist) 
        {
            enemyHiveDist = aWarriorTeam->enemies[i]->placedPoint->stepsToHive;
        }
    }
    if (state.myAnts.size() > 100)
    {
        if(aWarriorTeam->members.size() > 2)
        {
            result = WarriorTeam::ATTACK_STRATEGY_FIGHT_EQUAL_DEATH_IS_OK;
        }

        if (aWarriorTeam->friendsNearby > aWarriorTeam->enemiesNearby + 2)
        {
            result = WarriorTeam::ATTACK_STRATEGY_FIGHT_EQUAL_DEATH_IS_OK;
        }

        if (aWarriorTeam->members.size() + aWarriorTeam->friendsNearby > aWarriorTeam->enemiesNearby + aWarriorTeam->enemies.size() + 2)
        {
            result = WarriorTeam::ATTACK_STRATEGY_FIGHT_EQUAL_DEATH_IS_OK;
        }
        if (minHiveDist <= 2) 
        {
            result = WarriorTeam::ATTACK_STRATEGY_FIGHT_EQUAL_DEATH_IS_OK;
        }
        return result;
    }

    if (aWarriorTeam->friendsNearby > 21)
    {
        result = WarriorTeam::ATTACK_STRATEGY_FIGHT_EQUAL_DEATH_IS_OK;
    }
    
    if (aWarriorTeam->friendsNearby > aWarriorTeam->enemiesNearby * 2 + 2)
    {
        result = WarriorTeam::ATTACK_STRATEGY_FIGHT_EQUAL_DEATH_IS_OK;
    }
    
    if (aWarriorTeam->members.size() > 3) 
    {
        if (aWarriorTeam->friendsNearby > aWarriorTeam->enemiesNearby + 2)
        {
            result = WarriorTeam::ATTACK_STRATEGY_FIGHT_EQUAL_DEATH_IS_OK;
        }
    }

    if (minHiveDist <= 1 && aWarriorTeam->members.size() > aWarriorTeam->enemies.size() + 1)
    {
        result = WarriorTeam::ATTACK_STRATEGY_FIGHT_EQUAL_DEATH_IS_OK;
    }
    if (minHiveDist <= 0) 
    {
        result = WarriorTeam::ATTACK_STRATEGY_FIGHT_EQUAL_DEATH_IS_OK;
    }
    if (enemyHiveDist <= 0) 
    {
        result = WarriorTeam::ATTACK_STRATEGY_FIGHT_EQUAL_DEATH_IS_OK;
    }
    
//    if (aWarriorTeam->friendsNearby > aWarriorTeam->enemiesNearby * 3 + 2)
//    {
//        result = WarriorTeam::ATTACK_STRATEGY_ALWAYS_FIGHT;
//    }
    
    return result;
}










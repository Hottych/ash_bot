/*
 *  Path.cpp
 *  HottychBot
 *
 *  Created by Alexey Prosin on 11/1/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#include "Path.h"
#include "State.h"
#include "Bug.h"

void Path::Prefetch(State *st)
{
    currentStep = steps.size();
    for (int i = 0; i < currentStep; i++) 
    {
        VECTOR_BOUNDS(steps, i);
        steps[i].x = (steps[i].x + State::width) % State::width;
        steps[i].y = (steps[i].y + State::height) % State::height;
        LOG("Path step %d, %d    dir: %d", steps[i].x, steps[i].y, steps[i].moveDir);
    }
    currentStep -= 2;
}


int Path::GetCurrentStepDirection()
{
    VECTOR_BOUNDS(steps, currentStep);
    return steps[currentStep].moveDir;
}

int Path::GetStepX(int stepToGet)
{
    VECTOR_BOUNDS(steps, stepToGet);
    return steps[stepToGet].x;
}
int Path::GetStepY(int stepToGet)
{
    VECTOR_BOUNDS(steps, stepToGet);
    return steps[stepToGet].y;
}

int Path::GetCurrentStepX()
{
    VECTOR_BOUNDS(steps, currentStep);
    return steps[currentStep].x;
}

int Path::GetCurrentStepY()
{
    VECTOR_BOUNDS(steps, currentStep);
    return steps[currentStep].y;
}

void Path::StepIsDone()
{
    currentStep--;
}

int Path::GetStepsLeft()
{
    return currentStep;
}

bool Path::IsFinished()
{
    return currentStep < 0;
}


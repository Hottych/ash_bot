/*
 *  WavePathFinder.cpp
 *  HottychBot
 *
 *  Created by Alexey Prosin on 11/5/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#include "WavePathFinder.h"
#include "State.h"
#include "Path.h"
#include "Bug.h"
#include "Ant.h"

using namespace std;

WavePathFinder::WavePathFinder(State *statePtr)
: PathFinder(statePtr)
{
    LOG("Path finder init", 0);
    field = new WaveField(state, State::width, State::height);
    field->stepsLimit = 100;
    LOG("Path finder init DONE", 0);
}


Path *WavePathFinder::GetPath(int fromX, int fromY, int toX, int toY)
{
    int offX = fromX - field->centerX;
    int offY = fromY - field->centerY;
    LOG("Map offset %d, %d", offX, offY);

    field->targetX = (toX - offX + field->mapWidth) % field->mapWidth;
    field->targetY = (toY - offY + field->mapHeight) % field->mapHeight;
    field->mapOffsetX = offX;
    field->mapOffsetY = offY;
    if (field->GenerateField()) 
    {
        LOG("Field is generated in %d steps", field->step);
        Path *path = new Path();
        Path::PathStep step;
        int stX = field->targetX;
        int stY = field->targetY;
        int stInd = stX + stY * field->mapWidth;
        VECTOR_BOUNDS(field->pathMap, stInd);
        int stVal = field->pathMap[stInd];
//        int moveDir = 0;
        step.x = stX + offX;
        step.y = stY + offY;
        path->steps.push_back(step);
        while (true) 
        {

            if (stVal == 0)
            {
                path->Prefetch(state);
                return path;
            }
            

            int newVal;
            if (stX > 0)
            {
                VECTOR_BOUNDS(field->pathMap, stInd - 1);
                newVal = field->pathMap[stInd - 1];
                if (newVal == stVal - 1) 
                {
//                    state->bug.Log("Find left");
                    stVal--;
                    stX--;
                    stInd--;
                    VECTOR_BOUNDS(path->steps, path->steps.size()-1);
                    path->steps[path->steps.size()-1].moveDir = Ant::MOVE_RIGHT;

                    step.x = stX + offX;
                    step.y = stY + offY;
//                    state->bug.Log("Path step %d, %d    dir: %d", step.x, step.y, step.moveDir);
                    path->steps.push_back(step);
                    continue;
                }
            }
            if (stX < field->mapWidth)
            {
                VECTOR_BOUNDS(field->pathMap, stInd + 1);
                newVal = field->pathMap[stInd + 1];
                if (newVal == stVal - 1) 
                {
//                    state->bug.Log("Find right");
                    stVal--;
                    stX++;
                    stInd++;
                    VECTOR_BOUNDS(path->steps, path->steps.size()-1);
                    path->steps[path->steps.size()-1].moveDir = Ant::MOVE_LEFT;

                    step.x = stX + offX;
                    step.y = stY + offY;
//                    state->bug.Log("Path step %d, %d    dir: %d", step.x, step.y, step.moveDir);
                    path->steps.push_back(step);
                    continue;
                }
            }
            if (stY > 0)
            {
                VECTOR_BOUNDS(field->pathMap, stInd - field->mapWidth);
                newVal = field->pathMap[stInd - field->mapWidth];
                if (newVal == stVal - 1) 
                {
//                    state->bug.Log("Find up");
                    stVal--;
                    stY--;
                    stInd -= field->mapWidth;
                    VECTOR_BOUNDS(path->steps, path->steps.size()-1);
                    path->steps[path->steps.size()-1].moveDir = Ant::MOVE_DOWN;

                    step.x = stX + offX;
                    step.y = stY + offY;
//                    state->bug.Log("Path step %d, %d    dir: %d", step.x, step.y, step.moveDir);
                    path->steps.push_back(step);
                    continue;
                }
            }
            if (stY < field->mapHeight)
            {
                VECTOR_BOUNDS(field->pathMap, stInd + field->mapWidth);
                newVal = field->pathMap[stInd + field->mapWidth];
                if (newVal == stVal - 1) 
                {
//                    state->bug.Log("Find down");
                    stVal--;
                    stY++;
                    stInd += field->mapWidth;
                    VECTOR_BOUNDS(path->steps, path->steps.size()-1);
                    path->steps[path->steps.size()-1].moveDir = Ant::MOVE_UP;

                    step.x = stX + offX;
                    step.y = stY + offY;
//                    state->bug.Log("Path step %d, %d    dir: %d", step.x, step.y, step.moveDir);
                    path->steps.push_back(step);
                    continue;
                }
            }
            
            SafeRelease(path);
            return NULL;
        }

    }
    
    return NULL;
}



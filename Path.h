/*
 *  Path.h
 *  HottychBot
 *
 *  Created by Alexey Prosin on 11/1/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef _PATH_H_
#define _PATH_H_

#include "RefCounter.h"
#include "vector"

class State;

class Path : public RefCounter
{
public:
    struct PathStep 
    {
        int x;
        int y;
        int moveDir;
    };
    
    Path() : RefCounter(){};
    
    void Prefetch(State *st);//подготавливаем путь к использования, нормализуем координаты и т.д.
    
    int GetCurrentStepDirection();
    int GetCurrentStepX();
    int GetCurrentStepY();

    int GetStepX(int stepToGet);//шаги считаются в обратную сторону 0 - конечная точка пути
    int GetStepY(int stepToGet);

    void StepIsDone();

    int GetStepsLeft(); 
    bool IsFinished();
    
    std::vector<PathStep> steps;
    int currentStep;
};

#endif
//
//  WarriorTeam.h
//  HottychBot
//
//  Created by Denis Fileev on 11/20/11.
//  Copyright (c) 2011 DAVA Consulting LLC. All rights reserved.
//

#ifndef _WARRIOR_TEAM_H_
#define _WARRIOR_TEAM_H_

#include <vector>
#include "Bot.h"

class Ant;
class Enemy;
class State;
class PositionedObject;
class Poi;

typedef struct 
{
    int antIndex;
    int moveDirection;
    int newX;
    int newY;
    int newCellIndex;
    int addedAttackSupport;
    int attackAffection;
    int enemiesInWorsePosition;
    int enemiesInEqualPosition;
    int attackCoverage;
    int cellIsAchievableByMembersCount;
//    int distanceToTarget;
    bool isAlongPath;
    //
    std::vector<Ant *> *members;
} 
PossibleMove;

// FILTODO: targeted team support
class WarriorTeam 
{
public:
    enum eAttackStrategies
    {
        ATTACK_STRATEGY_ALWAYS_FLY = 0,
//        ATTACK_STRATEGY_FIGHT_IF_CAN_PROBABLY_WIN,
        ATTACK_STRATEGY_FIGHT_EQUAL_DEATH_IS_OK,
        ATTACK_STRATEGY_ALWAYS_FIGHT,
        ATTACK_STRATEGIES_COUNT
    };
    
    WarriorTeam(State *statePtr);
    WarriorTeam(State *statePtr, WarriorTeam *_team0, WarriorTeam *_team1);
    ~WarriorTeam();
    
    void AddAnt(Ant *aAnt);
    void RemoveAnt(Ant *aAnt);
    void OnAntIsDead(Ant *ant);
    
    void AddEnemy(Enemy *aEnemy, int distance = 0);
    void RemoveEnemy(Enemy *aEnemy);
    
    int stepsLeftToUpdateTarget;
    int enemiesCountOnLastUpdate;
    
//    void GetArea();
    void FindMyBestMove(int firstMemberIndex = 0);
    
    bool CalculateMinEnemiesAffected(int myAchievableCellsQueueSize, 
                                     int *myAchievableCellsQueue, 
                                     std::vector<int> *attackArea, 
                                     std::vector<int> *enemyAchievableCells, 
                                     std::vector<int> *resultEqual,
                                     std::vector<int> *resultWorse);

    void CalculateMaxEnemiesAffected(int myAchievableCellsQueueSize, 
                                     int *myAchievableCellsQueue, 
                                     std::vector<int> *enemyAchievableCells, 
                                     std::vector<int> *result);

    void GetAchievableCellsQueue(std::vector<PositionedObject *> *objects,
                                 std::vector<int> *cellIsAchievable,
                                 int *queueSize,
                                 int *queue);
    
    void CalculateAttackArea(std::vector<PositionedObject *> *ants, 
                             std::vector<bool> *isAlreadyMoved,
                             std::vector<int> *moveDirections,
                             std::vector<int> *result,
                             std::vector<int> *cellIsAchievableByMembers,
                             std::vector< std::vector<bool> > *cellIsAchievableByMember);

    void NormalizeAttackArea(int targetQueueSize, 
                             int *targetQueue,
                             int attackersCount,
                             std::vector< std::vector<bool> > *attackerStepPossibility,
                             std::vector<int> *attackArea);
    
    void FindBestMove(std::vector<bool> *isAlreadyMoved,
                      std::vector<int> *moveDirections);
    
    bool SummonRequiredAttackHelpers(PossibleMove *move,
                                     std::vector<bool> *isAlreadyMoved,
                                     std::vector<bool> *cellIsSteppable,
                                     std::vector<bool> *cellIsAchievableByMembers);
    
    void CalculateNearbyAnts();
    void AddCurrentEnemyPositionToHistory();
    std::list<PositionedObject *> * FindHighlyPossibleEnemyPosition();

    void ProcessTurn();
    
    void LogState();
    
    void GetCenter(int *x, int *y);
    
    State *worldState;
    
    std::vector<Ant *> members;
    std::vector<Enemy *> enemies;
    std::vector<PositionedObject *> enemyObjects;
    std::vector<PositionedObject *> memberObjects;
    
    int bestMemberDirections[2000];
    
    int *tempMemberCoordinates;
    
    std::vector<bool> cellIsSteppable;
    
    std::vector<bool> cellIsOccupiedByMember;

    std::vector<bool> movedMemberIsHere;
    PossibleMove lastMove;
    
    std::vector<int> cellIsAchievableByEnemies;
    std::vector< std::vector<bool> > cellIsAchievableByEnemy;
    
    int enemyAchievableQueueSize;
    int enemyAchievableQueue[MAX_MAP_SIZE * 2];
    std::vector<int> calculatedEnemyAttackArea;
    
    bool didFoundGoodMove;
    
    int attackStrategy;
    
    int indexInWarriorTeamsVector;
    int teamID;
    
    bool moreMembersNeededCount;
    
    int enemiesNearby;
    int friendsNearby;
    
    std::vector< std::vector<bool> > cellIsAchievableByMember;
//    std::vector<int> cellIsAchievableByMembers;
    std::vector< std::vector<bool> > cellIsWinningForMember;
    
    //////
    std::vector<int> cellIsAchievableByMembers;
    
    std::vector<int> calculatedMembersAttackArea;
    
    std::vector<int> enemiesInEqualPositionCount;
    std::vector<int> enemiesInWorsePositionCount;
    
    std::vector<int> maxEnemiesAffected;
    std::vector<int> attackHelpRequired;
    
    int queue[MAX_MAP_SIZE * 2];
    int queueSize;
    
    Poi *poi;

    std::vector<bool> isEnemyAlreadyMoved;
    std::vector<int> enemyDirections;
    std::list< std::list<PositionedObject *> > enemyLastPositionsHistory;
};


#endif // _WARRIOR_TEAM_H_

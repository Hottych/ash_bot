/*
 *  Enemy.cpp
 *  HottychBot
 *
 *  Created by Alexey Prosin on 11/19/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#include "Enemy.h"

#include "State.h"
#include "Ant.h"
#include "Utils.h"

using namespace std;

Enemy::Enemy(int _x, int _y, int _index, int _owner)
: PositionedObject(_x, _y, _index)
{
    owner = _owner;
    placedPoint = NULL;
    warriorTeam = NULL;
    indexInWarriorTeamMembersVector = -1;
}

Enemy::~Enemy()
{
}

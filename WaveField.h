/*
 *  WaveField.h
 *  HottychBot
 *
 *  Created by Alexey Prosin on 11/5/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef _WAVE_FIELD_H_
#define _WAVE_FIELD_H_

#include "vector"

class State;
class WaveField
{
public:
    
    
    WaveField(State *statePtr, int width, int height);
    WaveField(const WaveField &field);
    
        //позиция юнита для которого считается поле всегда равна середине карты
        //не очень удобно, но избавляет от лишних рассчетов - поле распространяется равномерно
    bool GenerateField();
    bool ProcessFieldStep(int stepNum);//returns true when steps is finished

    bool GenerateFieldWithFullClearing();

    bool GenerateFakeField();
    
    int DistanceToPoint(int x, int y);
    
    void Log();
    
    
    
    const static int maxStepValue;

    std::vector<int> pathMap;
        //    std::vector<int> indexes1; есть идея как прооптимизировать, будет время - займусь
        //    std::vector<int> indexes2;
    
    State *state;
    
    int mapWidth;
    int mapHeight;
    
    int mapOffsetX;
    int mapOffsetY;
    
    int targetX;
    int targetY;
    int targetInd;
    
    int step;
    int stepsLimit;
    
    
    int fieldFromX;
    int fieldFromY;
    int fieldToX;
    int fieldToY;
    
    int centerX;
    int centerY;


    bool noSteps;
    
//private:
    int unitX;
    int unitY;
};

#endif
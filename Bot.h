#ifndef BOT_H_
#define BOT_H_

#include "State.h"
#include "WavePathFinder.h"
#include "vector"
#include "Ant.h"

#define MAX_MAP_SIZE (200 * 200)

/*
    This struct represents your bot in the game of Ants
*/
class Bot
{
public:
    enum eFoodPriority 
    {
        FOOD_IS_QUESTION_OF_LIFE = 0,   //все срочно ищем жрачку
        FOOD_IS_NEEDED_FOR_YOUR_HIVE,   //ищем жрачку в регулярном режиме
        FOOD_IS_OK,                     //еды хватит на добрую сотню ходов вперед - уделяем больше времени войне
        FOOD_IS_A_CRAP                  //Еда? Да зачем она нам! Ходы закончатся раньше чем запасы еды
    };
    

    Bot();
    
    void Setup();

    void PlayGame();    //plays a single game of Ants

    void MakeMoves();   //makes moves for a single turn
    void EndTurn();     //indicates to the engine that it has made its moves


    State state;
    WavePathFinder *pathFinder;
    int foodPriority;

    void UpdateProtectorsCount();
    void UseFreeProtectors(Hive *hive);
    void FindNewProffession(Ant *ant);
    
    void OnAntTypeChanged(Ant *ant, int newType);
    void OnAntIsDead(Ant *ant);
    void OnAntIsNeedNewTarget(Ant *ant);

    void ProcessFoodPriority();
    
    int hivePoiRecheckCounter;
    int hiveToRecheck;
    
    
    Hive *GetNearestHive(ControlPoint *forPoint);
    
    std::vector<Ant*> antsOfType[Ant::ANT_TYPES_COUNT];

    int GetAttackStrategyForWarriorTeam(WarriorTeam *aWarriorTeam);
    std::vector<Ant*> tergetCheck1;
    std::vector<Ant*> tergetCheck2;
    std::vector<Ant*> *recheckingAnts;
    std::vector<Ant*> *antsToRecheck;
};

#endif //BOT_H_

/*
 *  ControlPoint.h
 *  HottychBot
 *
 *  Created by Alexey Prosin on 10/31/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef _CONTROL_POINT_H_
#define _CONTROL_POINT_H_

#include <vector>
#include <list>

#include "PositionedObject.h"

class State;
class Hive;
class Food;
class Ant;
class WaveField;
class Hive;
class Enemy;

//class ControlPoint;
//class ControlArea
//{
//public:
//    static int GetNormalizedAreaIndex(int x, int y);
//    
//    void RegisterControlPoint(ControlPoint *cp);
//    void UnregisterControlPoint(ControlPoint *cp);
//
//    std::vector<ControlPoint *> points;
//};

class ControlPoint : public PositionedObject
{
    
public:
    enum eControlType 
    {
        OUTER_POINT = 0,        //внешние точки
        INNER_POINT,        //внутренняя территория контролируемая нами
        BLOCKED_POINT,      //хз че, может и не надо
        HIVE                //здесь находится наш муравейник
    };
    
    static const int maxPointsDist;

    struct NextStep
    {//описывает то, на какую точку нужно пойти далее
        NextStep()
        : nextPoint(NULL)
        , pointsLeft(maxPointsDist)
        {
            
        }
        ControlPoint *nextPoint;
        int pointsLeft;
    };
    
    static void GenerateControlPoints(State *state);
    static ControlPoint *AddMyHive(State *state, Hive *hive);
    static void HivePulse(State *_state);

    static void LogAreas(State *_state);
    static void LogInfluence(State *_state);
    static void LogInterest(State *_state);
    
    static void OnPointFirstlyVisited(ControlPoint *point);
    static void OnNewWaterInPointArea(ControlPoint *point);
    static void OnPointOnWater(ControlPoint *point);
    static void OnCantFindPathToPoint(ControlPoint *point);
    static bool UpdateAreas();//returns bool if areas update queue is finished
    static void UpdateWaterPoints();

    static int controlDist;
    static int halfControlDist;
    
    static int GetTotalControlPointsCount();
    
    
    ControlPoint(int xPos, int yPos, State *_state);
    void AddPoint(ControlPoint* newPoint);
    void AddConnection(ControlPoint* point);
    void RemoveConnection(ControlPoint* point);
    void ClearConnections();
    void RegisterFood(Food *fd);
    void UnregisterFood(Food *fd);

    void RegisterHive(Hive *hv);//пока только для вражеских муравейников. Потом может и свои добавим сюда же.
    void UnregisterHive(Hive *hv);

    void RegisterEnemy(Enemy *en);

    void FindNewPosition();

    void RegisterPlacedAnt(Ant *ant);//муравьи находящиеся в зоне контроля точки
    void UnregisterPlacedAnt(Ant *ant);
    void OnPlacedAntChangeType(Ant *ant, int newType);

    void RegisterTargetedAnt(Ant *ant);//муравьи направляющиеся в эту точку
    void UnregisterTargetedAnt(Ant *ant);


    
    void BuildControlArea();
    void CheckForDeadlock();//проверка точки на тупик
    
    void OldBuildControlArea();

    
    
    void PerformNewType(int newType);
    void RecheckType();
    
    void RecheckInfluence();
    

    

    std::vector<ControlPoint*> nearPoints;
    std::vector<ControlPoint*> connectedPoints;
    std::vector<Food*> food;
    std::vector<Hive*> hives;//пока только для вражеских муравейников. Потом может и свои добавим сюда же.
    std::vector<Enemy*> enemies;

    std::vector<Ant*> placedAnts;
    std::vector<Ant*> targetedAnts;
    
    State *state;
    
    WaveField *waveField;
    
    int type;
    bool isVisited;
    
    
//    int movePotential;
    int influence;

    bool poiTemp;
    float interest;

    int centerX;
    int centerY;
    
    int curId;
    
    int totalVisits;
    int directionCounter;
    
    
    bool isDeadlock;//бывают и тупиковые точки которые для нас ничего не значат в плане ценности
    
    std::vector<NextStep> wayToPoints;//хранит для конкретной точки к какой следующей точке необходимо двигаться чтобы добраться в требуемую
    int stepsToHive;//расстояние в контрол поинтах до ближайшего муравейника.

    static void FindWayFromPointToPoint(ControlPoint *fromPoint, ControlPoint *toPoint, std::list<ControlPoint*> &findedWay);
    
protected:
    void CalculateField();
    void CalculateControlArea();

//    void AddPotential(int add, int hpId);
    
    void RecheckWorkers();
    
    void CheckPointOnTheWay(int curStep, std::vector<ControlPoint*> *pointsToScan);
    
    void SetType(int newType);
    static void RecheckPointLater(ControlPoint *pointToRecheck);
    
    struct PointUpdate 
    {
        ControlPoint *point;
        int priority;
    };
    
    static std::list<PointUpdate> updatesQueue;
    static std::vector<ControlPoint*> pointsOnWater;
    static std::vector<ControlPoint*> recheckOnRestart;
    static bool isResettingAreas;

    static std::vector<int> pointsInTheWay;

    

    
    
};

#endif
#ifndef BUG_H_
#define BUG_H_

#include <fstream>

#ifndef DEBUG
//    #define DEBUG
#endif


#ifdef DEBUG

#define ANT_ASSERT(expr)\
if(!(expr))\
{\
    Bug::Log("ASSERT [ %s, line %d ]", __FILE__, __LINE__);\
    exit(0);\
}\


#define ANT_ASSERT_MSG(expr, msg)\
if(!(expr))\
{\
    Bug::Log("ASSERT [ %s, line %d ] %s", __FILE__, __LINE__, msg);\
    exit(0);\
}\

#define LOG(msg, ...) Bug::Log(msg, __VA_ARGS__)

#define VECTOR_BOUNDS(vectorToCheck, indexToCheck) ANT_ASSERT(indexToCheck >= 0); ANT_ASSERT(indexToCheck < vectorToCheck.size())

#define MAINSTREAM state.mainstream

#else
#define ANT_ASSERT(expr) 
#define ANT_ASSERT_MSG(expr, msg)
#define LOG(msg, ...) 
#define VECTOR_BOUNDS(vectorToCheck, indexToCheck)
#define MAINSTREAM state.bug
#endif

#ifdef UNIT_TEST_LOG_ENABLED

#define UT_LOG(msg, ...) printf(msg, __VA_ARGS__); printf("\n")

#undef LOG
#define LOG(msg, ...) printf(msg, __VA_ARGS__); printf("\n")

//#elif defined DEBUG
//
//#define UT_LOG(msg, ...) Bug::Log(msg, __VA_ARGS__)
//
#else

#define UT_LOG(msg, ...) 

#endif

/*
    struct for debugging - this is gross but can be used pretty much like an ofstream,
                           except the debug messages are stripped while compiling if
                           DEBUG is not defined.
    example:
        Bug bug;
        bug.open("./debug.txt");
        bug << state << endl;
        bug << "testing" << 2.0 << '%' << endl;
        bug.close();
*/
struct Bug
{
    static std::ofstream file;

    Bug()
    {
        
    };

//    Bug(const Bug &b)
//    {
//        
//    };
    
#ifdef DEBUG
    static void Log(const char* text, ...)
    {
        va_list vl;
        va_start(vl, text);
        char tmp[4096];
        vsprintf(tmp, text, vl);
//        strcat(tmp, "\n");
        file << tmp << std::endl;
    };
#endif
    
//    template <class T>
//    void Log(const T &t)
//    {
//        file << t;
//    };
//    
//    void LogEndl()
//    {
//        file << std::endl;
//    };
    
    //opens the specified file
    inline void open(const std::string &filename)
    {
        #ifdef DEBUG
            file.open(filename.c_str());
        file<<"Debugging started"<<std::endl;
        #endif
    };

    //closes the ofstream
    inline void close()
    {
        #ifdef DEBUG
        file<<"Debugging ended"<<std::endl;
            file.close();
        #endif
    };
};


//output function for endl
inline Bug& operator<<(Bug &bug, std::ostream& (*manipulator)(std::ostream&))
{
#ifdef DEBUG
    bug.file << manipulator;
#endif
    
    return bug;
};

//output function
template <class T>
inline Bug& operator<<(Bug &bug, const T &t)
{
#ifdef DEBUG
    bug.file << t;
#endif
    
    return bug;
};

#endif //BUG_H_

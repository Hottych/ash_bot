/*
 *  ControlPoint.cpp
 *  HottychBot
 *
 *  Created by Alexey Prosin on 10/31/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#include "ControlPoint.h"
#include "State.h"
#include "Hive.h"
#include <list>
#include "Utils.h"
#include "Ant.h"
#include "WaveField.h"
#include "Enemy.h"

using namespace std;

ControlPoint* PointFor(int x, int y, int w, int h, State *state);

int ControlPoint::controlDist = 6;
int ControlPoint::halfControlDist = 3;
std::list<ControlPoint::PointUpdate> ControlPoint::updatesQueue;
std::vector<ControlPoint*> ControlPoint::pointsOnWater;

std::vector<ControlPoint*> ControlPoint::recheckOnRestart;
bool ControlPoint::isResettingAreas = false;

std::vector<int> ControlPoint::pointsInTheWay;

static int cpId = 0;
static int totalControlPoints = 0;

static WaveField *baseField;

const int ControlPoint::maxPointsDist = 99999;


struct CellController 
{
    ControlPoint *controller;
    int value;
    int dist;
};

static vector<CellController> contField;


ControlPoint* PointFor(int x, int y, int w, int h, State *state)
{
    return state->controlPoints[((x + w) % w) + ((y + h) % h) * w];
}

int ControlPoint::GetTotalControlPointsCount()
{
    return totalControlPoints;
}

void ControlPoint::GenerateControlPoints(State *state)
{
    controlDist = (int)sqrt(state->viewRadiusSq/2);//ну гиппотенуза, катеты. Все поняли почему квадраты меньше чем радиус обзора?
//    if (controlDist > 6) 
//    {//если до еды ползти дольше трех ходов по прямой, то это печально. Стоит потюнить это значение
//        controlDist = 6;
//    }

//    controlDist = 5;
    
    LOG("Control distance: %d", controlDist);

    totalControlPoints = (state->width/controlDist + 1)*(state->height/controlDist + 1) + 20;

    int w = 0;
    int h = 0;
    for (int y = 0; y < state->height; y += controlDist) 
    {
        if (state->height - y < controlDist) 
        {
            y = y - controlDist + (state->height - (y - controlDist))/2;
        }
        w = 0;
        for (int x = 0; x < state->width; x += controlDist) 
        {
            if (state->width - x < controlDist) 
            {
                x = x - controlDist + (state->width - (x - controlDist))/2;
            }
            ControlPoint *cp = new ControlPoint(x, y, state);
            state->controlPoints.push_back(cp);
//            state->controlMap[cp->mapIndex] = cp;
//            state->controlAreasMap[cp->mapIndex]->RegisterControlPoint(cp);
            w++;
        }
        h++;
    }
    

    contField = vector<CellController>((controlDist*2 + 1) * (controlDist*2 + 1));
    pointsInTheWay = vector<int>(totalControlPoints);
    recheckOnRestart.reserve(200);

    baseField = new WaveField(state, controlDist*2 + 1, controlDist*2 + 1);//поля должны быть вплотную к соседним точкам
    baseField->GenerateFakeField();
//    baseField->Log();
    
    for (int y = 0; y < h; y++) 
    {
        for (int x = 0; x < w; x++) 
        {
            VECTOR_BOUNDS(state->controlPoints, x + y * w);
            ControlPoint *cp = state->controlPoints[x + y * w];
            cp->waveField = new WaveField(*baseField);
            cp->waveField->mapOffsetX = cp->x - cp->waveField->centerX;
            cp->waveField->mapOffsetY = cp->y - cp->waveField->centerY;
            cp->AddPoint(PointFor(x,   y-1, w, h, state));
            cp->AddPoint(PointFor(x,   y+1, w, h, state));
            cp->AddPoint(PointFor(x-1, y, w, h, state));
            cp->AddPoint(PointFor(x+1, y, w, h, state));
            cp->AddPoint(PointFor(x-1, y-1, w, h, state));
            cp->AddPoint(PointFor(x+1, y+1, w, h, state));
            cp->AddPoint(PointFor(x+1, y-1, w, h, state));
            cp->AddPoint(PointFor(x-1, y+1, w, h, state));
        }
    }

    int sz = state->controlPoints.size();
    for (int i = 0; i < sz; i++) 
    {
//        state->controlPoints[i]->OldBuildControlArea();
        VECTOR_BOUNDS(state->controlPoints, i);
        state->controlPoints[i]->CalculateControlArea();
    }
    
}

ControlPoint *ControlPoint::AddMyHive(State *state, Hive *hive)
{
    LOG("Adding new hive at %d, %d", hive->x, hive->y);
    
    ANT_ASSERT(state->controlAreasMap[hive->mapIndex]);
    VECTOR_BOUNDS(state->controlAreasMap, hive->mapIndex);
    
    ControlPoint *hivePoint = NULL;
    std::list<ControlPoint *> nearList;
    if (hive->DistanceSq(state->controlAreasMap[hive->mapIndex]) < 3)
    {
        LOG("Low distance point", 0);
        hivePoint = state->controlAreasMap[hive->mapIndex];
        LOG("Hive point is exists %d, %d", hivePoint->x, hivePoint->y);
    }
    else 
    {
        LOG("Searching for nearest points", 0);
        ControlPoint *lastCp = NULL;
        for (int py = hive->y - controlDist; py < hive->y + controlDist; py++) 
        {
            for (int px = hive->x - controlDist; px < hive->x + controlDist; px++) 
            {
                VECTOR_BOUNDS(state->controlAreasMap, state->GetNormalizedMapIndex(px, py));
                ControlPoint *conPoint = state->controlAreasMap[state->GetNormalizedMapIndex(px, py)];
                ANT_ASSERT(conPoint);
                if (conPoint != lastCp) 
                {
                    lastCp = conPoint;
                    std::list<ControlPoint *>::iterator it;
                    for (it = nearList.begin(); it != nearList.end(); it++)
                    {
                        if (*it == conPoint)
                        {
                            conPoint = NULL;
                            break;
                        }
                    }
                    if (conPoint) 
                    {
                        int dst = hive->DistanceSq(conPoint);
                        LOG("Point distance to hive, %d", dst);
                        for (it = nearList.begin(); it != nearList.end(); it++)
                        {
                            int dst2 = hive->DistanceSq(*it);
                            if (dst <= dst2)
                            {
                                nearList.insert(it, conPoint);
                                conPoint = NULL;
                                break;
                            }
                        }
                        if (conPoint) 
                        {
                            nearList.push_back(conPoint);
                        }
                    }
                }
            }
        }
        
        LOG("Nearest control points collected %d", nearList.size());
    }
    
    
    
    
    if (hivePoint) 
    {
            //        state->controlMap[hivePoint->mapIndex] = NULL;
            //        state->controlAreasMap[hivePoint->mapIndex]->UnregisterControlPoint(hivePoint);
        hivePoint->SetPosition(hive->x, hive->y);
        delete hivePoint->waveField;
        hivePoint->waveField = new WaveField(*baseField);
        hivePoint->waveField->mapOffsetX = hivePoint->x - hivePoint->waveField->centerX;
        hivePoint->waveField->mapOffsetY = hivePoint->y - hivePoint->waveField->centerY;
    }
    else 
    {
        hivePoint = new ControlPoint(hive->x, hive->y, state);
        hivePoint->waveField = new WaveField(*baseField);
        hivePoint->waveField->mapOffsetX = hivePoint->x - hivePoint->waveField->centerX;
        hivePoint->waveField->mapOffsetY = hivePoint->y - hivePoint->waveField->centerY;
        state->controlPoints.push_back(hivePoint);
        
        LOG("New point created %d, %d", hivePoint->x, hivePoint->y);
        std::list<ControlPoint *>::iterator it;
        int i = 0;
        for (it = nearList.begin(); it != nearList.end(); it++)
        {
            hivePoint->AddPoint(*it);
            i++;
        }
//        hivePoint->BuildControlArea(); заказть пересчет для новой точки, причем пересчет должен быть в начале хода
    }
    hive->connectedPoint = hivePoint;
    hivePoint->SetType(ControlPoint::HIVE);
        //    state->controlMap[hivePoint->mapIndex] = hivePoint;
        //    state->controlAreasMap[hivePoint->mapIndex]->RegisterControlPoint(hivePoint);
    
    return hivePoint;
}


void ControlPoint::LogAreas(State *_state)
{
#ifdef DEBUG
    LOG("Control Areas print", 0);
    char txBuf[200 * 200 * 10];
    for (int y = 0; y < State::height; y++)
    {
        int indY = y * State::width;
        txBuf[0] = 0;
        for (int x = 0; x < State::width; x++) 
        {
            if (_state->terrainMap[indY + x] != State::WATER)
            {
                sprintf(txBuf, "%s %.3d", txBuf, _state->controlAreasMap[indY + x]->curId);
            }
            else 
            {
                sprintf(txBuf, "%s _W_", txBuf);
            }

        }
        LOG(txBuf, 0);
    }
#endif
}

void ControlPoint::LogInfluence(State *_state)
{
#ifdef DEBUG
    LOG("Area influence print", 0);
    char txBuf[200 * 200 * 10];
    for (int y = 0; y < State::height; y++)
    {
        int indY = y * State::width;
        txBuf[0] = 0;
        for (int x = 0; x < State::width; x++) 
        {
            if (_state->terrainMap[indY + x] != State::WATER)
            {
                if (_state->myAntsMap[indY + x])
                {
                    sprintf(txBuf, "%s  A ", txBuf);
                    
                }
                else
                {
                    sprintf(txBuf, "%s %.3d", txBuf, _state->controlAreasMap[indY + x]->influence);
                }
            }
            else 
            {
                sprintf(txBuf, "%s -W-", txBuf);
            }
            
        }
        LOG(txBuf, 0);
    }
#endif
}

void ControlPoint::LogInterest(State *_state)
{
#ifdef DEBUG
    LOG("Area interest print", 0);
    char txBuf[200 * 200 * 10];
    for (int y = 0; y < State::height; y++)
    {
        int indY = y * State::width;
        txBuf[0] = 0;
        for (int x = 0; x < State::width; x++) 
        {
            if (_state->terrainMap[indY + x] != State::WATER)
            {
                sprintf(txBuf, "%s %.2f", txBuf, _state->controlAreasMap[indY + x]->interest/10.f);
            }
            else 
            {
                sprintf(txBuf, "%s -WW-", txBuf);
            }
            
        }
        LOG(txBuf, 0);
    }
#endif
}

void ControlPoint::OnPointFirstlyVisited(ControlPoint *point)
{
    PointUpdate pu;
    pu.point = point;
    pu.priority = 1;
    updatesQueue.push_front(pu);
}

void ControlPoint::OnCantFindPathToPoint(ControlPoint *point)
{
    PointUpdate pu;
    pu.point = point;
    pu.priority = 0;
    updatesQueue.push_front(pu);
}

void ControlPoint::OnNewWaterInPointArea(ControlPoint *point)
{
    for (list<PointUpdate>::iterator it = updatesQueue.begin(); it != updatesQueue.end(); it++)
    {
        if (point == it->point)
        {
            return;
        }
    }
    PointUpdate pu;
    pu.point = point;
    pu.priority = 3;
    updatesQueue.push_front(pu);
}

void ControlPoint::OnPointOnWater(ControlPoint *point)
{
    LOG("Point %d is on water", point->curId);
    pointsOnWater.push_back(point);
}


bool ControlPoint::UpdateAreas()
{
    if (updatesQueue.empty()) 
    {
        return true;
    }
    updatesQueue.begin()->point->BuildControlArea();
    updatesQueue.erase(updatesQueue.begin());
    return false;
}

void ControlPoint::UpdateWaterPoints()
{
    for (int i = 0; i < pointsOnWater.size(); i++) 
    {
        VECTOR_BOUNDS(pointsOnWater, i);
        pointsOnWater[i]->FindNewPosition();
    }
    pointsOnWater.clear();
}


void ControlPoint::FindWayFromPointToPoint(ControlPoint *fromPoint, ControlPoint *toPoint, std::list<ControlPoint*> &findedWay)
{
    LOG("Find way form point %d to point %d", fromPoint->curId, toPoint->curId);
        //generatre field
    for (int i = 0; i < totalControlPoints; i++)
    {
        pointsInTheWay[i] = maxPointsDist;
    }

    vector<ControlPoint*> points1;//todo: optimize this
    vector<ControlPoint*> points2;
    points1.reserve(100);
    points2.reserve(100);

    vector<ControlPoint*> *inPoints = &points2;
    vector<ControlPoint*> *pointsToScan = &points1;

    inPoints->push_back(toPoint);
    
    LOG("Generating field", 0);
    int step = 0;
    while (true) 
    {
        bool isFinished = false;
        int sz = inPoints->size();
        LOG("Points to check %d", sz);
        for (int i = 0; i < sz; i++) 
        {
            if((*inPoints)[i] == fromPoint)
            {
                isFinished = true;
                break;
            }
            (*inPoints)[i]->CheckPointOnTheWay(step, pointsToScan);
        }
        if (isFinished) 
        {
            break;
        }
        
        step++;
        if (inPoints == &points2) 
        {
            inPoints = &points1;
            points2.clear();
            pointsToScan = &points2;
        }
        else 
        {
            inPoints = &points2;
            points1.clear();
            pointsToScan = &points1;
        }

    }
    LOG("Field is generated in %d steps", step);

    
        //find way on the field
    ControlPoint *lastPoint = fromPoint;
    while (true) 
    {
        int sz = lastPoint->connectedPoints.size();
        int d = maxPointsDist;
        int dist = 0;
        ControlPoint *newPoint = NULL;
        for (int i = 1; i < sz; i++) 
        {
            if (pointsInTheWay[lastPoint->connectedPoints[i]->curId] < d)
            {
                d = pointsInTheWay[lastPoint->connectedPoints[i]->curId];
                newPoint = lastPoint->connectedPoints[i];
                dist = lastPoint->DistanceSq(newPoint);
            }
            else if (pointsInTheWay[lastPoint->connectedPoints[i]->curId] == d)
            {
                int nd = lastPoint->DistanceSq(lastPoint->connectedPoints[i]);
                if (nd < dist) 
                {
                    newPoint = lastPoint->connectedPoints[i];
                    dist = nd;
                }
                
            }

        }
        ANT_ASSERT(newPoint);
        LOG("Way point %d", newPoint->curId);
        findedWay.push_back(newPoint);
        if (newPoint == toPoint) 
        {
            break;
        }
        lastPoint = newPoint;
    }

    LOG("Way is done", 0);
}

void ControlPoint::RecheckPointLater(ControlPoint *pointToRecheck)
{
    int sz = recheckOnRestart.size();
    for (int i = 0; i < sz; i++) 
    {
        if (recheckOnRestart[i] == pointToRecheck) 
        {
            return;
        }
    }
    recheckOnRestart.push_back(pointToRecheck);
}

void ControlPoint::HivePulse(State *_state)
{
    int sz = _state->controlPoints.size();
    int hsz = _state->myHives.size();
    for (int i = 0; i < sz; i++) 
    {
        _state->controlPoints[i]->stepsToHive = maxPointsDist;
        for (int h = 0; h < hsz; h++) 
        {
            _state->controlPoints[i]->wayToPoints[_state->myHives[h]->connectedPoint->curId].pointsLeft = maxPointsDist;
            _state->controlPoints[i]->wayToPoints[_state->myHives[h]->connectedPoint->curId].nextPoint = NULL;
        }
    }
    for (int h = 0; h < hsz; h++) 
    {
        int hpId = _state->myHives[h]->connectedPoint->curId;
        _state->myHives[h]->connectedPoint->wayToPoints[hpId].pointsLeft = 0;
        _state->myHives[h]->connectedPoint->wayToPoints[hpId].nextPoint = _state->myHives[h]->connectedPoint;

        vector<ControlPoint*> points1;//todo: optimize this
        vector<ControlPoint*> points2;
        points1.reserve(500);
        points2.reserve(500);
        
        vector<ControlPoint*> *inPoints = &points2;
        vector<ControlPoint*> *pointsToScan = &points1;
        
        inPoints->push_back(_state->myHives[h]->connectedPoint);
        _state->myHives[h]->connectedPoint->stepsToHive = 0;
        
        LOG("Generating pulse field for hive %d", h);
        int step = 1;
        while (true) 
        {
            bool isFinished = false;
            int sz = inPoints->size();
            LOG("Points to check %d", sz);
            for (int i = 0; i < sz; i++) 
            {
                int csz = (*inPoints)[i]->connectedPoints.size();
                for (int n = 1; n < csz; n++) 
                {
                    if ((*inPoints)[i]->connectedPoints[n]->wayToPoints[hpId].pointsLeft == maxPointsDist)
                    {
                        (*inPoints)[i]->connectedPoints[n]->wayToPoints[hpId].pointsLeft = step;
                        if (step < (*inPoints)[i]->connectedPoints[n]->stepsToHive) 
                        {
                            (*inPoints)[i]->connectedPoints[n]->stepsToHive = step;
                        }
                        (*inPoints)[i]->connectedPoints[n]->wayToPoints[hpId].nextPoint = (*inPoints)[i];
                        pointsToScan->push_back((*inPoints)[i]->connectedPoints[n]);
                    }
                }
            }
            if (pointsToScan->empty()) 
            {
                break;
            }
            
            step++;
            if (inPoints == &points2) 
            {
                inPoints = &points1;
                points2.clear();
                pointsToScan = &points2;
            }
            else 
            {
                inPoints = &points2;
                points1.clear();
                pointsToScan = &points1;
            }
            
        }
        LOG("Field is generated in %d steps", step);
    }
}

//void ControlPoint::AddPotential(int add, int hpId)
//{
//    movePotential += add;
//    int sz = connectedPoints.size();
//    int minDist = wayToPoints[hpId].pointsLeft - 1;
//    for (int i = 1; i < sz; i++) 
//    {
//        if (connectedPoints[i]->wayToPoints[hpId].pointsLeft <= minDist) 
//        {//todo: возможно нужно считать сразу для всех что меньше
//            minDist = connectedPoints[i]->wayToPoints[hpId].pointsLeft;
//        }
//    }
//    for (int i = 1; i < sz; i++) 
//    {
//        if (connectedPoints[i]->wayToPoints[hpId].pointsLeft == minDist) 
//        {//todo: возможно нужно считать сразу
//            connectedPoints[i]->AddPotential(add, hpId);
//        }
//    }
//}


















ControlPoint::ControlPoint(int xPos, int yPos, State *_state)
: PositionedObject(xPos, yPos)
{
    state = _state;
    type = OUTER_POINT;
    centerX = x;
    centerY = y;
    curId = cpId;
    totalVisits = 0;
    directionCounter = 0;

    stepsToHive = maxPointsDist;
    
//    movePotential = 1;
    influence = 1;
    interest = 1.0f;
    
    wayToPoints = vector<NextStep>(totalControlPoints);
    VECTOR_BOUNDS(wayToPoints, curId);
    wayToPoints[curId].pointsLeft = 0;
    wayToPoints[curId].nextPoint = this;
    
    isDeadlock = false;
    isVisited = false;
    cpId++;
}

void ControlPoint::AddPoint(ControlPoint* newPoint)
{
    for (int i = 0; i < nearPoints.size(); i++) 
    {
        VECTOR_BOUNDS(nearPoints, i);
        if (nearPoints[i] == newPoint)
        {
            return;
        }
    }
    nearPoints.push_back(newPoint);
//    connectedPoints.push_back(newPoint);
    newPoint->nearPoints.push_back(this);
//    newPoint->connectedPoints.push_back(this);

}

void ControlPoint::AddConnection(ControlPoint* point)
{
    for (int i = 0; i < connectedPoints.size(); i++) 
    {
        VECTOR_BOUNDS(connectedPoints, i);
        if (connectedPoints[i] == point)
        {
            return;
        }
    }
    connectedPoints.push_back(point);
    point->connectedPoints.push_back(this);
}

void ControlPoint::RemoveConnection(ControlPoint* point)
{
    FastRemovePointerFromVector(point, connectedPoints);
    FastRemovePointerFromVector(this, point->connectedPoints);
}

void ControlPoint::ClearConnections()
{
    while (!connectedPoints.empty()) 
    {
        RemoveConnection(connectedPoints[0]);
    }
}


void ControlPoint::FindNewPosition()
{//TODO: искать точку не просто ближнюю к центру, а ту в которой она сможет покрывать большую площадь

    int dst = controlDist / 3;
    
    int minVal = 999999;
    
    int newXd = 0;
    int newYd = 0;
    
    for (int py = -dst; py <= dst; py++) 
    {
        for (int px = -dst; px <= dst; px++) 
        {
            VECTOR_BOUNDS(state->terrainMap, state->GetNormalizedMapIndex(centerX + px, centerY + py));
            if (state->terrainMap[state->GetNormalizedMapIndex(centerX + px, centerY + py)] != State::WATER)
            {
                int ind = baseField->centerX + px + (baseField->centerY + py) * baseField->mapWidth;
                VECTOR_BOUNDS(baseField->pathMap, ind);
                if (baseField->pathMap[ind] < minVal)
                {
                    minVal = baseField->pathMap[ind];
                    newXd = px;
                    newYd = py;
                }
            }
        }
    }
    
    if (minVal < 999999) 
    {
        SetPosition(centerX + newXd, centerY + newYd);
        waveField->unitX = waveField->centerX + newXd;
        waveField->unitY = waveField->centerY + newYd;
        LOG("New point %d   %d, %d    new position: %d, %d", curId, centerX, centerY, x, y);
    }
    else 
    {
        SetPosition(centerX, centerY);
        waveField->unitX = waveField->centerX;
        waveField->unitY = waveField->centerY;
        ClearConnections();
        LOG("New point %d   %d, %d    is fully UNDER WATER", curId, x, y);
    }


    
    OnPointFirstlyVisited(this);//пересчитаем зоны контроля
}



void ControlPoint::CalculateField()
{
    LOG("CalculateField for id%d,    %d, %d", curId, centerX, centerY);
    waveField->GenerateFieldWithFullClearing();
}

void ControlPoint::CalculateControlArea()
{
    LOG("CalculateArea for id %d,    %d, %d", curId, centerX, centerY);
    int dw = controlDist * 2 + 1;
    int sz = dw * dw;
    
    ClearConnections();
    connectedPoints.push_back(this);//упросчаем просчеты по точке и присоедененным точкам
    
    for (int i = 0; i < sz; i++) 
    {
        VECTOR_BOUNDS(contField, i);
        VECTOR_BOUNDS(waveField->pathMap, i);
        contField[i].controller = this;
        contField[i].value = waveField->pathMap[i];
    }
    
//    for (int py = 0; py < dw; py++) 
//    {
//        int yInd = py * dw;
//        for (int px = 0; px < dw; px++) 
//        {
//            contField[px + yInd].controller = this;
//            contField[px + yInd].value = waveField->pathMap[px + yInd];
//            contField[px + yInd].dist = (px - waveField->unitX) * (px - waveField->unitX) + (py - waveField->unitY) * (py - waveField->unitY);
//        }
//    }

    sz = nearPoints.size();
    for (int i = 0; i < sz; i++) 
    {
        VECTOR_BOUNDS(nearPoints, i);
        ControlPoint *cp = nearPoints[i];
        bool isConnected = false;
//        LOG("working with id %d,    %d, %d", cp->curId, cp->centerX, cp->centerY);
        int fromX;
        int toX;
        int fromY;
        int toY;
        int offX;
        int offY;
        
        int cx = cp->centerX;
        int cy = cp->centerY;
        if (cx - centerX > controlDist*2) 
        {
            cx -= State::width;
        }
        else if(cx - centerX < -controlDist*2)
        {
            cx += State::width;
        }
        if (cy - centerY > controlDist*2) 
        {
            cy -= State::height;
        }
        else if(cy - centerY < -controlDist*2)
        {
            cy += State::height;
        }
        
        offX = cx - centerX;
        if (offX < 0) 
        {
            fromX = -offX;
            toX = dw;
        }
        else if(offX > 0)
        {
            fromX = 0;
            toX = dw - offX;
        }
        else 
        {
            fromX = 0;
            toX = dw;
        }
        
        
        offY = cy - centerY;
        if (offY < 0) 
        {
            fromY = -offY;
            toY = dw;
        }
        else if(offY > 0)
        {
            fromY = 0;
            toY = dw - offY;
        }
        else 
        {
            fromY = 0;
            toY = dw;
        }
        
//        LOG("offset  %d, %d", offX, offY);
//        LOG("from  %d, %d", fromX, fromY);
//        LOG("to  %d, %d", toX, toY);
//        char txBuf[4 * 1024];

//        LOG("Intersection:", 0);
//        for (int py = 0; py < dw; py++) 
//        {
//            txBuf[0] = 0;
//            for (int px = 0; px < dw; px++) 
//            {
//                if (px - offX >= fromX && px - offX < toX
//                    && py - offY >= fromY && py - offY < toY)
//                {
//                    sprintf(txBuf, "%s N", txBuf);
//                }
//                else
//                {
//                    sprintf(txBuf, "%s O", txBuf);
//                }
//
//            }
//            LOG(txBuf, 0);
//        }
                
        for (int py = fromY; py < toY; py++) 
        {
            for (int px = fromX; px < toX; px++) 
            {
                VECTOR_BOUNDS(cp->waveField->pathMap, px + py * cp->waveField->mapWidth);
                int vl = cp->waveField->pathMap[px + py * cp->waveField->mapWidth];
                int ind = (px + offX) + (py + offY) * dw;
                if (vl < WaveField::maxStepValue)
                {
                    VECTOR_BOUNDS(waveField->pathMap, ind);
                    if (waveField->pathMap[ind] < (controlDist*2-2) && vl < (controlDist*2-2))
                    {
                        isConnected = true;
                    }
                    VECTOR_BOUNDS(contField, ind);
                    if (vl < contField[ind].value)
                    {
                        contField[ind].value = vl;
                        contField[ind].controller = cp;
                    }
                }
            }
        }
        if (isConnected) 
        {
            AddConnection(cp);

            LOG("point id %d,    and point id %d  ARE STILL CONNECTED", curId, cp->curId);
        }

//        LOG("New field:", 0);
//        for (int py = 0; py < dw; py++) 
//        {
//            txBuf[0] = 0;
//            for (int px = 0; px < dw; px++) 
//            {
//                sprintf(txBuf, "%s %.3d", txBuf, contField[px + py * dw].controller->curId);
//            }
//            LOG(txBuf, 0);
//        }
    }
    
    
    
    for (int py = 0; py < dw; py++) 
    {
        for (int px = 0; px < dw; px++) 
        {
            int ind = state->GetNormalizedMapIndex(px + waveField->mapOffsetX, py + waveField->mapOffsetY);
            VECTOR_BOUNDS(state->controlAreasMap, ind);
            VECTOR_BOUNDS(contField, px + py * dw);
            ANT_ASSERT(contField[px + py * dw].controller);
            state->controlAreasMap[ind] = contField[px + py * dw].controller;
        }
    }
    
}


void ControlPoint::BuildControlArea()
{
    LOG("Calcualtes fields", 0);
    CalculateField();//TODO: передалать на UpdateField для уменьшения количества просчетов
//    waveField->Log();
    int sz = nearPoints.size();
    for (int i = 0; i < sz; i++) 
    {
        VECTOR_BOUNDS(nearPoints, i);
        nearPoints[i]->CalculateField();//ТОДО: аналогично
//        nearPoints[i]->waveField->Log();
    }
    LOG("Calculates areas", 0);
    CalculateControlArea();

    sz = nearPoints.size();
    for (int i = 0; i < sz; i++) 
    {
        VECTOR_BOUNDS(nearPoints, i);
        nearPoints[i]->CheckForDeadlock();
        nearPoints[i]->RecheckInfluence();
                                        //        nearPoints[i]->waveField->Log();
    }
//    LOG("Calculates areas DONE", 0);
}

void ControlPoint::CheckForDeadlock()
{//проверяем является ли точка тупиком - из нее можно пойти только в одну сторону
    if (connectedPoints.size() <= 3)
    {
        if (connectedPoints.size() <= 2) 
        {
            isDeadlock = true;
            totalVisits += 5;
            return;
        }
        else 
        {
                //connectedPoint[0] всегда текущая точка
            if ((connectedPoints[1]->x >= 0 && connectedPoints[2]->x >= 0)
                || (connectedPoints[1]->x <= 0 && connectedPoints[2]->x <= 0))
            {
                if ((connectedPoints[1]->y >= 0 && connectedPoints[2]->y >= 0)
                    || (connectedPoints[1]->y <= 0 && connectedPoints[2]->y <= 0))
                {//обе оставшиеся точки соединения находятся в одной плоскости
                    isDeadlock = true;
                    totalVisits += 5;
                }
            }
        }
    }
    isDeadlock = false;
    
}


void ControlPoint::RegisterFood(Food *fd)
{
    food.push_back(fd);
    fd->placedPoint = this;
}

void ControlPoint::UnregisterFood(Food *fd)
{
    fd->placedPoint = NULL;
    FastRemovePointerFromVector(fd, food);
}

void ControlPoint::RegisterHive(Hive *hv)
{
    LOG("%d Register hive", curId);
    hives.push_back(hv);
    hv->connectedPoint = this;
}

void ControlPoint::UnregisterHive(Hive *hv)
{
    LOG("%d Unregister hive", curId);
    hv->connectedPoint = NULL;
    FastRemovePointerFromVector(hv, hives);
}


void ControlPoint::RegisterPlacedAnt(Ant *ant)
{
    totalVisits++;
    if (isDeadlock) 
    {//в тупики мы ходим реже
        totalVisits += 2;
    }
    ant->currentPlaceVectorIndex = placedAnts.size();
    placedAnts.push_back(ant);
    if (!isVisited) 
    {
        isVisited = true;
        OnPointFirstlyVisited(this);
    }

    int sz = connectedPoints.size();
    for (int i = 0; i < sz; i++) 
    {
        connectedPoints[i]->influence += 2;
    }
    
    
//    if(ant->type == Ant::ANT_WORKER)
//    {
//        int sz = connectedPoints.size();
//        for (int i = 0; i < sz; i++) 
//        {
//            connectedPoints[i]->isWorkerControlled = true;
//        }
//    }
}

void ControlPoint::UnregisterPlacedAnt(Ant *ant)
{
    if (ant->currentPlaceVectorIndex < placedAnts.size() - 1)
    {
        VECTOR_BOUNDS(placedAnts, ant->currentPlaceVectorIndex);
        VECTOR_BOUNDS(placedAnts, placedAnts.size() - 1);
        VECTOR_BOUNDS(placedAnts, placedAnts[ant->currentPlaceVectorIndex]->currentPlaceVectorIndex);
        ANT_ASSERT(placedAnts[ant->currentPlaceVectorIndex]);
        ANT_ASSERT(placedAnts[placedAnts.size() - 1]);
        placedAnts[ant->currentPlaceVectorIndex] = placedAnts[placedAnts.size() - 1];
        placedAnts[ant->currentPlaceVectorIndex]->currentPlaceVectorIndex = ant->currentPlaceVectorIndex;
    }
    DecreaseVector(placedAnts);
    int sz = connectedPoints.size();
    for (int i = 0; i < sz; i++) 
    {
        connectedPoints[i]->influence -= 2;
    }
//    if(ant->type == Ant::ANT_WORKER)
//    {
//        RecheckWorkers();
//    }
}

void ControlPoint::OnPlacedAntChangeType(Ant *ant, int newType)
{
//    if (ant->type == Ant::ANT_WORKER && newType != Ant::ANT_WORKER)
//    {
//        RecheckWorkers();
//    }
//    else if (ant->type != Ant::ANT_WORKER && newType == Ant::ANT_WORKER)
//    {
//        int sz = connectedPoints.size();
//        for (int i = 0; i < sz; i++) 
//        {
//            connectedPoints[i]->isWorkerControlled = true;
//        }
//    }
//
}


void ControlPoint::RegisterTargetedAnt(Ant *ant)
{
    totalVisits++;
    if (isDeadlock) 
    {//в тупики мы ходим реже
        totalVisits += 2;
    }
    ant->currentTargetVectorIndex = targetedAnts.size();
    targetedAnts.push_back(ant);
    int sz = connectedPoints.size();
    for (int i = 0; i < sz; i++) 
    {
        connectedPoints[i]->influence += 1;
    }
}

void ControlPoint::UnregisterTargetedAnt(Ant *ant)
{
    LOG("Unregistring from %d", curId);
    totalVisits--;
    if (ant->currentTargetVectorIndex < targetedAnts.size() - 1)
    {
        ANT_ASSERT(targetedAnts[ant->currentTargetVectorIndex]);
        ANT_ASSERT(targetedAnts[targetedAnts.size() - 1]);
        VECTOR_BOUNDS(targetedAnts, targetedAnts[ant->currentTargetVectorIndex]->currentTargetVectorIndex);
        VECTOR_BOUNDS(targetedAnts, ant->currentTargetVectorIndex);
        VECTOR_BOUNDS(targetedAnts, targetedAnts.size() - 1);
        targetedAnts[ant->currentTargetVectorIndex] = targetedAnts[targetedAnts.size() - 1];
        targetedAnts[ant->currentTargetVectorIndex]->currentTargetVectorIndex = ant->currentTargetVectorIndex;
    }
    DecreaseVector(targetedAnts);

    int sz = connectedPoints.size();
    for (int i = 0; i < sz; i++) 
    {
        connectedPoints[i]->influence -= 1;
    }
}

void ControlPoint::RecheckInfluence()
{
    influence = 1;
    int sz = connectedPoints.size();
    for (int i = 0; i < sz; i++) 
    {
        influence += connectedPoints[i]->placedAnts.size() * 2;
        influence += connectedPoints[i]->targetedAnts.size();
    }
}



void ControlPoint::RegisterEnemy(Enemy *en)
{
    LOG("regisster enemy in %d", curId);
    enemies.push_back(en);
    en->placedPoint = this;
}


void ControlPoint::SetType(int newType)
{
    if (newType == type)
    {
        return;
    }
#ifdef DEBUG
    switch (newType) 
    {
        case HIVE:
            LOG("Point %d new type is HIVE", curId);
            break;
        case INNER_POINT:
            LOG("Point %d new type is INNER_POINT", curId);
            break;
        case OUTER_POINT:
            LOG("Point %d new type is OUTER_POINT", curId);
            break;
    }
#endif
    type = newType;
}

void ControlPoint::CheckPointOnTheWay(int curStep, std::vector<ControlPoint*> *pointsToScan)
{
//    LOG("Checking point %d", curId);
    pointsInTheWay[curId] = curStep;
    int sz = connectedPoints.size();
    for (int i = 1; i < sz; i++) 
    {
        VECTOR_BOUNDS(pointsInTheWay, connectedPoints[i]->curId);
        if (pointsInTheWay[connectedPoints[i]->curId] == maxPointsDist)
        {
            pointsInTheWay[connectedPoints[i]->curId] = maxPointsDist + 1;
            pointsToScan->push_back(connectedPoints[i]);
        }
    }
}




//int ControlArea::GetNormalizedAreaIndex(int x, int y)
//{
//    x = ((x + ControlPoint::halfControlDist + State::width) % State::width) / ControlPoint::controlDist;
//    y = ((y + ControlPoint::halfControlDist + State::height) % State::height) / ControlPoint::controlDist;
//    return x + y * State::controlAreaWidth;
//}
//
//void ControlArea::RegisterControlPoint(ControlPoint *cp)
//{
//    points.push_back(cp);
//}
//
//void ControlArea::UnregisterControlPoint(ControlPoint *cp)
//{
//    FastRemovePointerFromVector(cp, points);
//}






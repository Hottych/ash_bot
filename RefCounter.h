/*
 *  RefCounter.h
 *  HottychBot
 *
 *  Created by Alexey Prosin on 11/1/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef _REF_COUNTER_H_
#define _REF_COUNTER_H_

class	RefCounter
{
public:
	
	RefCounter()
    : referenceCount(1)
	{
	}
    
	virtual ~RefCounter()
	{
	}
    
	virtual void Retain()
	{
		referenceCount++;
	}
	
	virtual int Release()
	{
		--referenceCount;
		int refCounter = referenceCount;
		if (!refCounter)
		{
			delete this;
		}
		return refCounter;
	}
    
	int GetRetainCount() const
	{
		return referenceCount;
	}
protected:
	
	RefCounter(const RefCounter &rfc)
	{
	}
	RefCounter & operator = (const RefCounter & rfc)
	{
		return *this;
	}
	
	int referenceCount;
};

template<class C>
void SafeRelease(C * &c) 
{ 
	if (c) 
	{
		c->Release();
		c = 0;
	}
}

template<class C>
C * SafeRetain(C * c) 
{ 
	if (c) 
	{
		c->Retain();
	}
	return c;
}


#endif

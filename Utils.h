/*
 *  Utils.h
 *  HottychBot
 *
 *  Created by Alexey Prosin on 11/7/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef _UTILS_H_
#define _UTILS_H_

#include "vector"
#include "Bug.h"

template<class C>
void DecreaseVector(std::vector<C*> &v)
{
    ANT_ASSERT(v.size() > 0);
    v.resize(v.size() - 1);
}
    
template<class C>
void FastRemovePointerFromVector(C *c, std::vector<C*> &v)
{
    int sz = v.size();
    for (int i = 0; i < sz; i++) 
    {
        if (v[i] == c)
        {
            if (i < sz - 1)
            {
                VECTOR_BOUNDS(v, i);
                VECTOR_BOUNDS(v, sz - 1);
                v[i] = v[sz - 1];
            }
            v.resize(sz - 1);
            return;
        }
    }
}

#endif
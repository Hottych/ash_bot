/*
 *  PositionedObject.h
 *  HottychBot
 *
 *  Created by Alexey Prosin on 10/31/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef _POSITIONED_OBJECT_H_
#define _POSITIONED_OBJECT_H_

/*
 struct for representing locations in the grid.
 */
class PositionedObject
{
public:
    
    PositionedObject()
    {
        x = y = mapIndex = -1;
    };
    
    virtual ~PositionedObject() // виртуальный деструктор, чтобы работал оператор typeid
    {
    }
    
    PositionedObject(int _x, int _y, int _index);
    PositionedObject(int _x, int _y);
    
    void SetPosition(int _x, int _y);
    
    int DistanceSq(PositionedObject *toObject);
    
    int x;
    int y;
    int mapIndex;
    
};

bool compare_position_objects(PositionedObject *obj0, PositionedObject *obj1);

#endif
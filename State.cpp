#include "State.h"
#include "Ant.h"
#include "Utils.h"
#include "Enemy.h"
#include "WarriorTeam.h"
#include "Poi.h"
#include <stdlib.h>
#include <string.h>

using namespace std;

int State::width = 0;
int State::height = 0;

//int State::controlAreaWidth = 0;
//int State::controlAreaHeight = 0;
int State::currentTurn = 0;

std::ofstream Bug::file;


//constructor
State::State()
{
    gameover = 0;
    currentTurn = 0;
    attackMask = NULL;
    bug.open("./debug.txt");
    
#ifdef DEBUG
    mainstream.open("./mainstream.txt");
#endif
};

//deconstructor
State::~State()
{
    bug.close();
#ifdef DEBUG
    mainstream.close();
#endif
};

//sets the state up
void State::Setup()
{
    terrainMap = vector<int>(height * width);
    objectsMap = vector<PositionedObject*>(height * width);
    myAntsMap = vector<Ant *>(height * width);
    enemiesMap = vector<Enemy *>(height * width);
//    controlMap = vector<ControlPoint*>(height * width);
    controlAreasMap = vector<ControlPoint*>(height * width);
//    enemyDensityMap = vector<int>(height * width);
    
//    move0EnemyProbabilityStorage.assign(State::width * State::height, 1);
//    move0EnemyProbability = &move0EnemyProbabilityStorage;
//    
//    move1EnemyProbabilityStorage.assign(State::width * State::height, 1);
//    move1EnemyProbability = &move1EnemyProbabilityStorage;
    
    waveFieldsMap = vector<WaveFieldMapCell>(height * width);

    myAnts.reserve(2000);
    warriorTeams.reserve(2000);
    
    invalidRects.reserve(100);
    
    ControlPoint::GenerateControlPoints(this);
    
    // АК: setup view template with viewRadius
    {
        viewTemplateWidth = viewTemplateHeight = ((int)viewRadius - 1) * 2 + 1;
        //LOG("viewTemplateWidth %d, viewTemplateHeight %d", viewTemplateWidth, viewTemplateHeight);
        viewTemplate = vector<int>(viewTemplateHeight * viewTemplateWidth);

        for (int y = 0; y < viewTemplateHeight; ++y)
        {
            for (int x = 0; x < viewTemplateWidth; ++x)
            {
                int i = x + y * viewTemplateWidth;

                int dx = abs(x - viewTemplateWidth / 2);
                int dy = abs(y - viewTemplateHeight / 2);

                viewTemplate[i] = (dx*dx + dy*dy <= viewRadiusSq);//? State::GROUND : State::UNKNOWN;
            }
        }
    }
    
    activeHivesCount = 0;
};

WaveFieldMapCell State::GetWaveFieldMapCell(int x, int y)
{
    x = (x + width) % width;
    y = (y + height) % height;
    int index = x + y * width;
    
    if (NULL == waveFieldsMap[index].field)
    {
        waveFieldsMap[index].field = new WaveField(this, (int)viewRadius * 2 + 1, (int)viewRadius * 2 + 1);

        waveFieldsMap[index].field->mapOffsetX = x - (int)viewRadius;
        waveFieldsMap[index].field->mapOffsetY = y - (int)viewRadius;
        
        waveFieldsMap[index].field->GenerateFieldWithFullClearing();
        waveFieldsMap[index].isFullyValid = true;
    }
    else if (!waveFieldsMap[index].isFullyValid)
    {
        waveFieldsMap[index].field->GenerateFieldWithFullClearing();
        waveFieldsMap[index].isFullyValid = true;
    }


    return waveFieldsMap[index];
}

void State::UpdateMyHive(int x, int y)
{
    int ind = x + y * width;
    VECTOR_BOUNDS(objectsMap, ind);
    if (!objectsMap[ind])
    {
        ++activeHivesCount;
        Hive *h = new Hive(x, y, ind, 0);
        objectsMap[ind] = h;
        myHives.push_back(h);
        ControlPoint::AddMyHive(this, h);
    }
    else 
    {
        Hive *h = (Hive *)objectsMap[ind];
        h->state = Hive::HIVE_ACTIVE;
    }

}

void State::UpdateEnemyHive(int x, int y, int player)
{
    int ind = x + y * width;
    VECTOR_BOUNDS(objectsMap, ind);
    if (!objectsMap[ind])
    {
        LOG("Added enemy hive %d, %d   player: %d", x, y, player);
        Hive *h = new Hive(x, y, ind, player);
        objectsMap[ind] = h;
        enemyHives.push_back(h);
        controlAreasMap[ind]->RegisterHive(h);
        h->poi = new Poi(controlAreasMap[ind], 4, 0.1, 0.4);
    }
    else 
    {
        Hive *h = (Hive *)objectsMap[ind];
        h->state = Hive::HIVE_ACTIVE;
        if (controlAreasMap[ind] != h->connectedPoint)
        {
            h->connectedPoint->UnregisterHive(h);
            controlAreasMap[ind]->RegisterHive(h);
        }
    }
}

void State::UpdateMyAnt(int x, int y)
{
    int ind = x + y * width;
    LOG("Update ant %d, %d   index: %d", x, y, ind);
    VECTOR_BOUNDS(myAntsMap, ind);
    if (!myAntsMap[ind])
    {
        Ant *ant = new Ant(this, x, y, ind);
        myAntsMap[ind] = ant;
        ant->storeVectorIndex = myAnts.size();
        myAnts.push_back(ant);
        VECTOR_BOUNDS(objectsMap, ind);
        ANT_ASSERT(objectsMap[ind]);
        ((Hive *)objectsMap[ind])->RegisterAnt(ant);
        ant->myHive = (Hive *)objectsMap[ind];
        ANT_ASSERT(controlAreasMap[ind]);
        VECTOR_BOUNDS(controlAreasMap, ind);
        ant->currentPlace = controlAreasMap[ind];
        ant->currentPlace->RegisterPlacedAnt(ant);
//        LOG("Ant 1 is moved = %d in line: %d", myAnts[0]->isMoved, __LINE__);
    }
    
    myAntsMap[ind]->Refresh(currentTurn);
}

void State::UpdateFood(int x, int y)
{
    LOG("Update food %d, %d", x, y);
    int ind = x + y * width;
    VECTOR_BOUNDS(terrainMap, ind);
    if (terrainMap[ind] != FOOD)
    {
        Food *fd = new Food(x, y, ind, this);
        fd->lastActiveTurn = currentTurn;
        fd->storeVectorIndex = food.size();
        VECTOR_BOUNDS(objectsMap, ind);
        objectsMap[ind] = fd;
        food.push_back(fd);
        VECTOR_BOUNDS(controlAreasMap, ind);
        controlAreasMap[ind]->RegisterFood(fd);
        terrainMap[ind] = FOOD;
    }
    else 
    {
        VECTOR_BOUNDS(objectsMap, ind);
        ((Food *)objectsMap[ind])->lastActiveTurn = currentTurn;
    }
}

void State::UpdateEnemy(int x, int y, int player)
{//todo: генерировать точки опасности в точках появления и скопления врагов
    int ind = x + y * width;
    Enemy *en = new Enemy(x, y, ind, player);
    enemies.push_back(en);
//    terrainMap[ind] = ANT + player; походу это на терейне не надо
    
    enemiesMap[ind] = en;

    VECTOR_BOUNDS(controlAreasMap, ind);
    controlAreasMap[ind]->RegisterEnemy(en);
}

void State::AddInvalidRect(tRect &r)
{
    for (int i = 0; i < invalidRects.size(); ++i)
    {
        if (invalidRects[i].isIntersect(r))
        {
            invalidRects[i].intersect(r);
            return;
        }
    }
    
    invalidRects.push_back(r);
}

void State::UpdateWater(int x, int y)
{
    LOG("Update water %d, %d", x, y);
    int tr = (int)viewRadius;
    tRect r(x - tr, y - tr, x + tr, y + tr);
    AddInvalidRect(r);
    
    //LOG("water square: %d, %d | invalidRects.size(): %d", x, y, invalidRects.size());

    int ind = x + y * width;
    VECTOR_BOUNDS(controlAreasMap, ind);
    ANT_ASSERT(controlAreasMap[ind]);
    VECTOR_BOUNDS(terrainMap, ind);
    terrainMap[ind] = State::WATER;
    if (controlAreasMap[ind]->mapIndex == ind) 
    {
        ControlPoint::OnPointOnWater(controlAreasMap[ind]);
    }
    else if(controlAreasMap[ind]->isVisited)
    {
        ControlPoint::OnNewWaterInPointArea(controlAreasMap[ind]);
    }
}

void State::AddWarriorTeam(WarriorTeam *aTeam)
{
    aTeam->worldState = this;
    aTeam->indexInWarriorTeamsVector = warriorTeams.size();
    warriorTeams.push_back(aTeam);
}

void State::RemoveWarriorTeam(WarriorTeam *aTeam)
{
    if (aTeam->indexInWarriorTeamsVector < warriorTeams.size() - 1)
    {
        VECTOR_BOUNDS(warriorTeams, aTeam->indexInWarriorTeamsVector);
        VECTOR_BOUNDS(warriorTeams, warriorTeams.size() - 1);
        VECTOR_BOUNDS(warriorTeams, warriorTeams[aTeam->indexInWarriorTeamsVector]->indexInWarriorTeamsVector);
        ANT_ASSERT(warriorTeams[aTeam->indexInWarriorTeamsVector]);
        ANT_ASSERT(warriorTeams[warriorTeams.size() - 1]);
        warriorTeams[aTeam->indexInWarriorTeamsVector] = warriorTeams[warriorTeams.size() - 1];
        warriorTeams[aTeam->indexInWarriorTeamsVector]->indexInWarriorTeamsVector = aTeam->indexInWarriorTeamsVector;
    }
    DecreaseVector(warriorTeams);
    
    delete aTeam;
}

bool State::NeedToKillHive(Hive *h)
{
    if (Hive::HIVE_UNKNOWN != h->state) return false;
    
    for (int y = 0; y < viewTemplateHeight; ++y)
    {
        for (int x = 0; x < viewTemplateWidth; ++x)
        {
            int ti = x + y * viewTemplateWidth;
            VECTOR_BOUNDS(viewTemplate, ti);
            
            int xx = h->x + x - viewTemplateWidth / 2;
            int yy = h->y + y - viewTemplateHeight / 2;
            
            int ai = GetNormalizedMapIndex(xx, yy);
            VECTOR_BOUNDS(myAntsMap, ai);
            
            if (viewTemplate[ti] && myAntsMap[ai])  return true;
        }
    }
    
    return false;
}

void State::Refresh()
{
    LOG("Refreshing", 0);
    
    // invalidate wave fields
    for (int i = 0; i < invalidRects.size(); ++i)
    {
        tRect &r = invalidRects[i];
        for (int y = r.y1; y <= r.y2; ++y)
        {
            for (int x = r.x1; x <= r.x2; ++x)
            {
                int mi = GetNormalizedMapIndex(x, y);
                waveFieldsMap[mi].isFullyValid = false;
            }
        }
    }
    
    // kill my hives
    int sz = myHives.size();
    for (int i = 0; i < sz; i++) 
    {
        ANT_ASSERT(myHives[i]);
        
        if (NeedToKillHive(myHives[i]))
        {
            --activeHivesCount;
            myHives[i]->state = Hive::HIVE_DEAD;
            if (myHives[i]->poi)
            {
                delete myHives[i]->poi;
                myHives[i]->poi = NULL;
            }
            myHives[i]->protectorsRequired = 0;
            
            LOG("Our hive is dead!", 0);
        }
    }

    // kill enemy hives
    sz = enemyHives.size();
    for (int i = 0; i < sz; i++) 
    {
        ANT_ASSERT(enemyHives[i]);
        if (NeedToKillHive(enemyHives[i]))
        {
            LOG("Removing enemy hive", 0);
            Hive *h = enemyHives[i];
            h->state = Hive::HIVE_DEAD;
            while (!h->terminators.empty()) 
            {
                h->terminators[0]->OnTargetHiveIsGone(h);
            }
            h->connectedPoint->UnregisterHive(h);
            FastRemovePointerFromVector(h, enemyHives);
            objectsMap[h->mapIndex] = NULL;
            LOG("Deleting enemy hive", 0);
            delete h;
            i--;
            LOG("Enemy hive is dead!", 0);
        }
    }

    sz = food.size();
    for (int i = 0; i < sz; i++) 
    {//removing inactive food points
        ANT_ASSERT(food[i]);
        if (food[i]->lastActiveTurn < currentTurn)//можно потюнить, чтобы память о еде еще на пару ходов оставалась
        {
            RemoveFood(food[i]);
            i--;
            sz = food.size();
        }
    }
    sz = myAnts.size();
    for (int i = 0; i < sz; i++) 
    {//removing inactive ants
        ANT_ASSERT(myAnts[i]);
        if (myAnts[i]->lastActiveTurn < currentTurn)//можно потюнить, чтобы память о еде еще на пару ходов оставалась
        {
            RemoveMyAnt(myAnts[i]);
            i--;
            sz = myAnts.size();
        }
    }
    
    ControlPoint::UpdateWaterPoints();
    
//    vector<double> *tmpPtr = move0EnemyProbability;
//    move0EnemyProbability = move1EnemyProbability;
//    move1EnemyProbability = tmpPtr;
//    
//    for (int i = 0; i < width * height; i++) 
//    {
//        (*move0EnemyProbability)[i] = ((*move0EnemyProbability)[i] + (enemiesMap[i] ? 1 : 0)) / 2;
//    }
    
    LOG("Refreshing DONE", 0);
}

void State::RemoveFood(Food *fd)
{//тодо: сильно задумался над деструктором для еды
    LOG("Remove Food %d, %d   index: %d", fd->x, fd->y, fd->mapIndex);
    if (fd->harvester) 
    {
        fd->harvester->OnFoodIsGone(fd);
    }
    if (fd->storeVectorIndex < food.size() - 1)
    {
        ANT_ASSERT(food[food.size() - 1]);
        ANT_ASSERT(food[fd->storeVectorIndex]);
        VECTOR_BOUNDS(food, fd->storeVectorIndex);
        VECTOR_BOUNDS(food, food[fd->storeVectorIndex]->storeVectorIndex);
        food[fd->storeVectorIndex] = food[food.size() - 1];
        food[fd->storeVectorIndex]->storeVectorIndex = fd->storeVectorIndex;
    }
    DecreaseVector(food);
    
    fd->placedPoint->UnregisterFood(fd);
    
    ANT_ASSERT(objectsMap[fd->mapIndex]);
    VECTOR_BOUNDS(objectsMap, fd->mapIndex);
    objectsMap[fd->mapIndex] = NULL;
    VECTOR_BOUNDS(terrainMap, fd->mapIndex);
    if (terrainMap[fd->mapIndex] == FOOD)
    {
        terrainMap[fd->mapIndex] = GROUND;
    }
    delete fd->field;
    delete fd;
}

void State::RemoveMyAnt(Ant *ant)
{
    LOG("Remove My Ant %d, %d   index: %d", ant->x, ant->y, ant->mapIndex);
    ant->OnAntIsDead();
    
    if (ant->storeVectorIndex < myAnts.size() - 1)
    {
        ANT_ASSERT(myAnts[ant->storeVectorIndex]);
        ANT_ASSERT(myAnts[myAnts.size() - 1]);
        VECTOR_BOUNDS(myAnts, ant->storeVectorIndex);
        VECTOR_BOUNDS(myAnts, myAnts[ant->storeVectorIndex]->storeVectorIndex);
        myAnts[ant->storeVectorIndex] = myAnts[myAnts.size() - 1];
        myAnts[ant->storeVectorIndex]->storeVectorIndex = ant->storeVectorIndex;
    }
    DecreaseVector(myAnts);
    ANT_ASSERT(myAntsMap[ant->mapIndex]);
    VECTOR_BOUNDS(myAntsMap, ant->mapIndex);
    myAntsMap[ant->mapIndex] = NULL;
    VECTOR_BOUNDS(terrainMap, ant->mapIndex);
    if (terrainMap[ant->mapIndex] == ANT)
    {
        terrainMap[ant->mapIndex] = GROUND;
    }
    
    delete ant;
}

//resets all non-water squares to land and clears the bots ant vector
void State::Reset()
{
    LOG("Resetting", 0);
    
    int hs = myHives.size();
    for (int i = 0; i < hs; i++) 
    {
        VECTOR_BOUNDS(myHives, i);
        if (Hive::HIVE_DEAD != myHives[i]->state)
        {
            myHives[i]->state = Hive::HIVE_UNKNOWN;
        }
    }
    hs = enemyHives.size();
    for (int i = 0; i < hs; i++) 
    {
        VECTOR_BOUNDS(enemyHives, i);
        if (Hive::HIVE_DEAD != enemyHives[i]->state)
        {
            enemyHives[i]->state = Hive::HIVE_UNKNOWN;
        }
    }

    hs = warriorTeams.size();
    for (int i = 0; i < hs; i++) 
    {
        VECTOR_BOUNDS(warriorTeams, i);
        warriorTeams[i]->enemies.clear();
    }
    
    hs = enemies.size();
    for (int i = 0; i < hs; i++) 
    {
        VECTOR_BOUNDS(enemies, i);
        delete enemies[i];
    }
    hs = controlPoints.size();
    for (int i = 0; i < hs; i++) 
    {
        VECTOR_BOUNDS(controlPoints, i);
        controlPoints[i]->enemies.clear();
    }
    enemiesMap.clear();
    enemies.clear();
    
    ControlPoint::HivePulse(this);
    
    invalidRects.clear();
    
    LOG("Resetting done", 0);

    
        //    myAnts.clear();
//    enemyAnts.clear();
//    myHills.clear();
//    enemyHills.clear();
//    food.clear();
//    for(int row=0; row<width; row++)
//        for(int col=0; col<height; col++)
//            if(!grid[row][col].isWater)
//                grid[row][col].reset();
};

//outputs move information to the engine
void State::MakeMove(Ant *ant, int direction)
{
    ANT_ASSERT(direction >= 0 && direction < 4);
    cout << "o " << ant->y << " " << ant->x << " " << CDIRECTIONS[direction] << endl;

#ifdef DEBUG
    cout << "v setLayer 0" << endl;
    cout << "v setLineWidth 1" << endl;
    cout << "v setLineColor 255 255 255 1" << endl;
    
    const int MOVE_DIRECTIONS[Ant::MOVES_COUNT][2] = { {0, -1}, {1, 0}, {0, 1}, {-1, 0}, {0, 0} };
    float x1 = ant->x + MOVE_DIRECTIONS[direction][0];
    float y1 = ant->y + MOVE_DIRECTIONS[direction][1];
    float x2 = x1 + MOVE_DIRECTIONS[direction][0];
    float y2 = y1 + MOVE_DIRECTIONS[direction][1];

    cout << "v line " << y1 << " " << x1 << " " << y2 << " " << x2 << endl;
#endif
    
//    Location nLoc = getLocation(loc, direction);
//    grid[nLoc.row][nLoc.col].ant = grid[loc.row][loc.col].ant;
//    grid[loc.row][loc.col].ant = -1;
};

//returns the euclidean distance between two locations with the edges wrapped
//double State::distance(const Location &loc1, const Location &loc2)
//{
//// /*   
//    int d1 = abs(loc1.x-loc2.x),
//        d2 = abs(loc1.y-loc2.y),
//        dr = min(d1, width-d1),
//        dc = min(d2, height-d2);
//    return sqrt(dr*dr + dc*dc);
////   */  
//    return 0;
//};

//отдает индекс с учетом того что координаты могут быть больше размеров карты или меньше нуля
int State::GetNormalizedMapIndex(int x, int y)
{
    return ((x + width) % width) + ((y + height) % height) * width;
}

int State::DistanceSq(int x0, int y0, int x1, int y1)
{
    int dx = abs(x0 - x1);
    int dy = abs(y0 - y1);
    dx = min(dx, width - dx);
    dy = min(dy, height - dy);
    return dx * dx + dy * dy;
}

void State::SetAttackRadiusSq(double arg)
{
    attackRadiusSq = arg;
    attackRadius = sqrt(arg);
    
    movedAttackRadius = attackRadius + 2;
    movedAttackRadiusSq = (int)(movedAttackRadius * movedAttackRadius);
    LOG("Attack radius sq = %d", attackRadiusSq);
    LOG("attck radius = %f", attackRadius);
    LOG("Moved attack radius sq = %d", movedAttackRadiusSq);
    
    attackAreaSize_2 = (int)floor(attackRadius);
    attackAreaSize = attackAreaSize_2 * 2 + 1;
    attackAreaExtendedSize = attackAreaSize + 2;
    
    attackMask = (int **)malloc(attackAreaSize * sizeof(int *));
    for (int i = 0; i < attackAreaSize; i++) {
        attackMask[i] = (int *)malloc(attackAreaSize * sizeof(int));
    }
    
    attackMaskExtended = (int **)malloc(attackAreaExtendedSize * sizeof(int *));
    for (int i = 0; i < attackAreaExtendedSize; i++) {
        attackMaskExtended[i] = (int *)malloc(attackAreaExtendedSize * sizeof(int));
        memset(attackMaskExtended[i], 0, attackAreaExtendedSize * sizeof(int));
    }
    
    for (int j = 0; j < attackAreaSize; j++) {
        for (int i = 0; i < attackAreaSize; i++) {
            attackMask[i][j] = (DistanceSq(i, j, attackAreaSize_2, attackAreaSize_2) <= attackRadiusSq) ? 1 : 0;
        }
    }
    
    for (int i = 0; i < attackAreaExtendedSize; i++) {
        for (int j = 0; j < attackAreaExtendedSize; j++) {
            for (int d = 0; d < Ant::MOVES_COUNT; d++) {
                if (DistanceSq(i, 
                               j, 
                               attackAreaSize_2 + 1 + MOVE_DIRECTIONS[d][0], 
                               attackAreaSize_2 + 1 + MOVE_DIRECTIONS[d][1]) <= attackRadiusSq) 
                {
                    attackMaskExtended[i][j] = 1;
                    break;
                }
            }
        }
    }
    
    LOG("attack map:", 0);
    for (int j = 0; j < attackAreaSize; j++) {
        char txBuf[4 * 1024];
        txBuf[0] = 0;
        for (int i = 0; i < attackAreaSize; i++) {
            sprintf(txBuf, "%s %d", txBuf, attackMask[i][j]);
        }
        LOG(txBuf, 0);
    }
    LOG("-----\n", 0);

//    LOG("extended attack map:", 0);
//    for (int j = 0; j < attackAreaExtendedSize; j++) {
//        char txBuf[4 * 1024];
//        txBuf[0] = 0;
//        for (int i = 0; i < attackAreaExtendedSize; i++) {
//            sprintf(txBuf, "%s %d", txBuf, attackMaskExtended[i][j]);
//        }
//        LOG(txBuf, 0);
//    }
//    LOG("-----\n", 0);
}

/*
    This is the output function for a state. It will add a char map
    representation of the state to the output stream passed to it.

    For example, you might call "cout << state << endl;"
*/
ostream& operator<<(ostream &os, const State &state)
{
    int i = 0;
    for(int y = 0; y < state.height; y++)
    {
        for(int x = 0; x < state.width; x++)
        {
            int cn = state.terrainMap[i];
            switch (cn)
            {
                case State::GROUND:
                    os << '.';
                    break;
                case State::WATER:
                    os << '%';
                    break;
                case State::FOOD:
                    os << '*';
                    break;
                default:
                    if (cn >= State::HIVE) 
                    {
                        os << (char)('A' + (cn - State::HIVE));
                    }
                    else if (cn >= State::ANT)
                    {
                        os << (char)('a' + (cn - State::ANT));
                    }

                    break;
            }
            i++;
        }
        os << endl;
    }

    return os;
};

//input function
istream& operator>>(istream &is, State &state)
{
    int x, y, player;
    string inputType, junk;

    //finds out which turn it is
    while(is >> inputType)
    {
        MAINSTREAM << inputType << std::endl;
        
        if(inputType == "end")
        {
            state.gameover = 1;
            break;
        }
        else if(inputType == "turn")
        {
            is >> state.currentTurn;
            MAINSTREAM << state.currentTurn << std::endl;
            break;
        }
        else //unknown line
            getline(is, junk);
    }

    if(state.currentTurn == 0)
    {
        //reads game parameters
        while(is >> inputType)
        {
            MAINSTREAM << inputType << std::endl;
            
            if(inputType == "loadtime")
            {
                is >> state.loadtime;
                MAINSTREAM << state.loadtime << std::endl;
            }
            else if(inputType == "turntime")
            {
                is >> state.turntime;
                MAINSTREAM << state.turntime << std::endl;
            }
            else if(inputType == "rows")
            {
                is >> state.height;
                MAINSTREAM << state.height << std::endl;
            }
            else if(inputType == "cols")
            {
                is >> state.width;
                MAINSTREAM << state.width << std::endl;
            }
            else if(inputType == "turns")
            {
                is >> state.turnsCount;
                MAINSTREAM << state.turnsCount << std::endl;
            }
            else if(inputType == "player_seed")
            {
                is >> state.seed;
                MAINSTREAM << state.seed << std::endl;
                
                srand((int)state.seed);
            }
            else if(inputType == "viewradius2")
            {
                double tmp;
                is >> tmp;
                MAINSTREAM << tmp << std::endl;
                
                state.viewRadiusSq = tmp; // FILTODO: discuss this
                state.viewRadius = sqrt(tmp);
                LOG("View radius sq = %d", state.viewRadiusSq);
                LOG("View radius = %f", state.viewRadius);
            }
            else if(inputType == "attackradius2")
            {
                double tmp;
                is >> tmp;
                MAINSTREAM << tmp << std::endl;
                
                state.SetAttackRadiusSq(tmp);
            }
            else if(inputType == "spawnradius2")
            {
                is >> state.spawnRadiusSq;
                MAINSTREAM << state.spawnRadiusSq << std::endl;
                
                state.spawnRadius = sqrt(state.spawnRadiusSq);
            }
            else if(inputType == "ready") //end of parameter input
            {
                state.timer.start();
                break;
            }
            else    //unknown line
                getline(is, junk);
        }
    }
    else
    {
        //reads information about the current turn
        while(is >> inputType)
        {
            MAINSTREAM << inputType << std::endl;
            
            if(inputType == "w") //water square
            {
                is >> y >> x;
                MAINSTREAM << y << std::endl << x << std::endl;
                
                state.UpdateWater(x, y);
            }
            else if(inputType == "f") //food square
            {
                is >> y >> x;
                MAINSTREAM << y << std::endl << x << std::endl;
                
                state.UpdateFood(x, y);
            }
            else if(inputType == "a") //live ant square
            {
                is >> y >> x >> player;
                MAINSTREAM << y << std::endl << x << std::endl << player << std::endl;
                
                if (player == 0) 
                {
                    state.UpdateMyAnt(x, y);
                }
                else 
                {
                    state.UpdateEnemy(x, y, player);
                }

                    //TODO: add ants to array
//                if(player == 0)
//                    state.myAnts.push_back(Location(row, col));
//                else
//                    state.enemyAnts.push_back(Location(row, col));
            }
            else if(inputType == "d") //dead ant square
            {
                is >> y >> x >> player;
                MAINSTREAM << y << std::endl << x << std::endl << player << std::endl;
                
                    //TODO: use this if you need this
//                state.grid[row][col].deadAnts.push_back(player);
            }
            else if(inputType == "h")
            {
                is >> y >> x >> player;
                MAINSTREAM << y << std::endl << x << std::endl << player << std::endl;

                if(player == 0)
                {
                    state.UpdateMyHive(x, y);
                }
                else
                {
                    state.UpdateEnemyHive(x, y, player);
                }
                state.terrainMap[x + y * state.width] = State::HIVE + player;

                    //TODO: add enemy hives to array
//                state.grid[row][col].hillPlayer = player;
//                if(player == 0)
//                    state.myHills.push_back(Location(row, col));
//                else
//                    state.enemyHills.push_back(Location(row, col));

            }
            else if(inputType == "players") //player information
            {
                is >> state.numberOfPlayers;
                MAINSTREAM << state.numberOfPlayers << std::endl;
            }
            else if(inputType == "scores") //score information
            {
                state.scores = vector<double>(state.numberOfPlayers, 0.0);
                for(int p = 0; p < state.numberOfPlayers; p++)
                {
                    is >> state.scores[p];
                    MAINSTREAM << state.scores[p] << std::endl;
                }
            }
            else if(inputType == "go") //end of turn input
            {
                if(state.gameover)
                {
                    is.setstate(std::ios::failbit);
                }
                else
                {
                    state.timer.start();
                }
                break;
            }
            else //unknown line
                getline(is, junk);
        }
    }
    
    return is;
};

/*
 *  Food.h
 *  HottychBot
 *
 *  Created by Alexey Prosin on 11/7/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef _FOOD_H_
#define _FOOD_H_

#include "PositionedObject.h"
#include "vector"

class Ant;
class ControlPoint;
class State;
class WaveField;
class Food : public PositionedObject
{
public:
    
//    enum eHiveState
//    {
//        HIVE_ACTIVE = 0,
//        HIVE_UNKNOWN,
//        HIVE_DEAD
//    };
    
    Food(int _x, int _y, int _index, State *state);
    
    int lastActiveTurn;
    int storeVectorIndex;
    Ant *harvester;
    ControlPoint *placedPoint;
    WaveField *field;
};

#endif //_FOOD_H_

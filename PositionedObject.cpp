/*
 *  PositionedObject.cpp
 *  HottychBot
 *
 *  Created by Alexey Prosin on 10/31/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#include "PositionedObject.h"
#include "State.h"
using namespace std;


PositionedObject::PositionedObject(int _x, int _y, int _index)
{
    x = _x;
    y = _y;
    mapIndex = _index;
}

PositionedObject::PositionedObject(int _x, int _y)
{
    SetPosition(_x, _y);
}

void PositionedObject::SetPosition(int _x, int _y)
{
    x = _x;
    y = _y;
    mapIndex = x + y * State::width;
}

int PositionedObject::DistanceSq(PositionedObject *toObject)
{
    int dx = abs(x - toObject->x);
    int dy = abs(y - toObject->y);
    dx = min(dx, State::width - dx);
    dy = min(dy, State::height - dy);
    return dx*dx + dy*dy;
}

bool compare_position_objects(PositionedObject *obj0, PositionedObject *obj1)
{
    return (obj1->mapIndex < obj0->mapIndex);
}
/*
 *  Enemy.h
 *  HottychBot
 *
 *  Created by Alexey Prosin on 11/19/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

/*
 *  Hive.h
 *  HottychBot
 *
 *  Created by Alexey Prosin on 10/30/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef _ENEMY_H_
#define _ENEMY_H_

#include "PositionedObject.h"
#include "vector"

class Ant;
class ControlPoint;
class WarriorTeam;

class Enemy : public PositionedObject
{
public:
    
    Enemy()
    : PositionedObject()
    {
        owner = 0;
        warriorTeam = NULL;
        indexInWarriorTeamMembersVector = -1;
    };
    
    Enemy(int _x, int _y, int _index, int _owner);
    ~Enemy();
    
    
    int owner;
    
    WarriorTeam *warriorTeam;
    int indexInWarriorTeamMembersVector;
    
    ControlPoint *placedPoint;
protected:
};

#endif //_ENEMY_H_
